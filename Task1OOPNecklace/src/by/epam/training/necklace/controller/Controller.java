package by.epam.training.necklace.controller;

import by.epam.training.necklace.model.builder.GemstoneNecklaceBuilder;
import by.epam.training.necklace.model.repository.Necklace;
import by.epam.training.necklace.model.repository.Requests;
import by.epam.training.necklace.model.util.Parser;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.util.List;
import java.util.Optional;

@Log4j2
public class Controller {
    public static void main(String[] args) {
        GemstoneNecklaceBuilder builder = new GemstoneNecklaceBuilder(Necklace.getInstance());
        Optional<List<String[]>> someData = Parser.process(new File("data/file.txt"));
        builder.createNecklace(someData);
        Necklace.getInstance().sort(Requests.COST);
        log.debug(Necklace.getInstance().getAll());

    }
}
