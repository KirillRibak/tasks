package by.epam.training.necklace.model.builder;

import by.epam.training.necklace.model.exception.GemstoneException;
import by.epam.training.necklace.model.factory.AmberFactory;
import by.epam.training.necklace.model.factory.DiamondFactory;
import by.epam.training.necklace.model.factory.PearlFactory;
import by.epam.training.necklace.model.factory.RubyFactory;
import by.epam.training.necklace.model.repository.Necklace;
import by.epam.training.necklace.model.stones.details.StoneName;
import by.epam.training.necklace.model.util.Parser;
import by.epam.training.necklace.model.validation.Validation;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * That class is responsible for correct creation the Necklace.
 */
@Log4j2
public class GemstoneNecklaceBuilder extends NecklaceBuilder {

    /**
     * Index of param in string.
     */
    private final int nameIndex = 0;
    /**
     * Index of param in string.
     */
    private final int weightIndex = 1;
    /**
     * Index of param in string.
     */
    private final int costIndex = 2;
    /**
     * Index of param in string.
     */
    private final int originIndex = 3;
    /**
     * Index of param in string.
     */
    private final int groupsIndex = 4;
    /**
     * Index of param in string.
     */
    private final int transparencyIndex = 5;
    /**
     * Index of param in string.
     */
    private final int differencesIndex = 6;

    /**
     * The field contains the number of stones to be checked.
     */
    private int counter;
    /**
     * The field contains all names of stones from file.
     */
    private List<String> names = new ArrayList<>();
    /**
     * The field contains all weights of stones from file.
     */
    private List<Double> weights = new ArrayList<>();
    /**
     * The field contains all prices of stones from file.
     */
    private List<Double> costs = new ArrayList<>();
    /**
     * The field contains all origin of stones from file.
     */
    private List<String> origin = new ArrayList<>();
    /**
     * The field contains all groups of stones from file.
     */
    private List<String> groups = new ArrayList<>();
    /**
     * The field contains all transparency of stones from file.
     */
    private List<String> transparency = new ArrayList<>();
    /**
     * The field contains all differences of stones from file.
     */
    private List<String> differences = new ArrayList<>();
    /**
     * The factory for creation the amber.
     */
    private AmberFactory amberFactory;
    /**
     * The factory for creation the pearl.
     */
    private PearlFactory pearlFactory;
    /**
     * The factory for creation the diamond.
     */
    private DiamondFactory diamondFactory;
    /**
     * The factory for creation the ruby.
     */
    private RubyFactory rubyFactory;

    /**
     * Constructor.
     */
    public GemstoneNecklaceBuilder(Necklace necklace) {
        super(necklace);
        this.amberFactory = new AmberFactory();
        this.pearlFactory = new PearlFactory();
        this.diamondFactory = new DiamondFactory();
        this.rubyFactory = new RubyFactory();
        this.counter = 0;
    }

    /**
     * Method discards changes in string fields,
     * each describe some param the stone.
     *
     * @param params - The field contains string params.
     */
    private void backSting(final List<String> params) {
        if (params.size() > counter) {
            params.remove(params.size() - 1);
        }
    }

    /**
     * Method discards changes in double fields,
     * each describe some param the stone.
     *
     * @param params - The field contains double params.
     */
    private void backDouble(final List<Double> params) {
        if (params.size() > counter) {
            params.remove(params.size() - 1);
        }
    }

    /**
     * Takes a file and call the parser.
     * Break up strings to substrings, each describe a stone.
     *
     * @param data - breaking up file to array string.
     */
    private void process(final Optional<List<String[]>> data) {
        if (data.isPresent()) {
            for (String[] stone : data.get()) {
                try {
                    weights.add(Double.valueOf(stone[weightIndex]));
                    costs.add(Double.valueOf(stone[costIndex]));
                    names.add(stone[nameIndex]);
                    origin.add(stone[originIndex]);
                    groups.add(stone[groupsIndex]);
                    transparency.add(stone[transparencyIndex]);
                    differences.add(stone[differencesIndex]);
                    counter++;
                } catch (NumberFormatException e) {
                    if (weights.size() > names.size()) {
                        weights.remove(weights.size() - 1);
                    }
                    log.error("Incorrect data: " + stone.toString()
                            + "don't describe a gemstone.");
                    continue;
                } catch (IndexOutOfBoundsException e) {
                    backDouble(weights);
                    backDouble(costs);
                    backSting(names);
                    backSting(origin);
                    backSting(groups);
                    backSting(transparency);
                    log.error("String: " + stone.toString()
                            + "don't describe a gemstone.");
                }
            }
        } else {
            log.error("Program can't the read file.");
        }

    }

    /**
     * Create the necklace.
     *
     * @param data - breaking up file to array string.
     */
    @Override
    public void createNecklace(final Optional<List<String[]>> data) {
        process(data);
        for (int i = 0; i < counter; i++) {
            try {
                if (Validation.validate(names.get(i))) {
                    if ("ruby".compareToIgnoreCase(names.get(i)) == 0) {
                        necklace.addStone(rubyFactory.create(names.get(i),
                                weights.get(i), costs.get(i), origin.get(i),
                                groups.get(i), transparency.get(i),
                                differences.get(i)));
                    } else if ("pearl".compareToIgnoreCase(names.get(i)) == 0) {
                        necklace.addStone((pearlFactory.create(names.get(i),
                                weights.get(i), costs.get(i), origin.get(i),
                                groups.get(i), transparency.get(i),
                                differences.get(i))));
                    } else if ("diamond".compareToIgnoreCase(names.get(i)) == 0){
                        necklace.addStone((
                                diamondFactory.create(
                                names.get(i), weights.get(i),
                                costs.get(i), origin.get(i), groups.get(i),
                                transparency.get(i), differences.get(i))));
                    } else {
                        necklace.addStone((amberFactory.create(
                                names.get(i), weights.get(i),
                                costs.get(i), origin.get(i),
                                groups.get(i), transparency.get(i),
                                differences.get(i))));
                    }
                }
            } catch (GemstoneException ex) {
                log.error(names.get(i) + " - is not gem name.");
            }
        }
    }
}
