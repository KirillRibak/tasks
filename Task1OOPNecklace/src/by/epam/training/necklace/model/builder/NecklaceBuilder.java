package by.epam.training.necklace.model.builder;

import by.epam.training.necklace.model.repository.Necklace;

import java.io.File;
import java.util.List;
import java.util.Optional;

/**
 * Abstract class describe the model
 */
public abstract class NecklaceBuilder {
    protected Necklace necklace;
    public abstract void createNecklace(Optional<List<String[]>> data);

    public NecklaceBuilder(Necklace necklace) {
        this.necklace = necklace;
    }
}
