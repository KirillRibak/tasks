package by.epam.training.necklace.model.exception;

public class GemstoneException extends Exception {
    private String name;

    public GemstoneException(String message, String name) {
        super(message);
        this.name = name;
    }
}
