package by.epam.training.necklace.model.factory;

import by.epam.training.necklace.model.factory.abstraction.StoneFactory;
import by.epam.training.necklace.model.stones.Amber;
import by.epam.training.necklace.model.stones.abstraction.Gemstone;
import by.epam.training.necklace.model.stones.details.Size;
import lombok.extern.log4j.Log4j2;

import java.util.List;
import java.util.Optional;

@Log4j2
public class AmberFactory extends StoneFactory {

    private final List<String> SIZES_SRING = Size.getSizesLikeString();

    private final List<Size> SIZES = Size.getSizes();

    private Size size;

    public Optional<Gemstone> create(String name, double weight, double cost,
                                     String origin, String group, String transp, String diff) {
        if (defaultCheck(weight, cost, origin, group, transp)) {
            if (!SIZES_SRING.contains(diff.toUpperCase())) {
                log.debug("size check false");
                return Optional.empty();
            } else {
                log.debug("size is normal");
                int index = SIZES_SRING.indexOf(diff.toUpperCase());
                size = SIZES.get(index);
            }
            return Optional.of(new Amber(name, weight, cost, origin, rank, transparency, size));
        }
        return Optional.empty();
    }
}
