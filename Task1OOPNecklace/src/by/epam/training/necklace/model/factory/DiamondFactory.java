package by.epam.training.necklace.model.factory;

import by.epam.training.necklace.model.factory.abstraction.StoneFactory;
import by.epam.training.necklace.model.stones.Diamond;
import by.epam.training.necklace.model.stones.abstraction.Gemstone;
import by.epam.training.necklace.model.stones.details.Colors;

import java.util.List;
import java.util.Optional;

public class DiamondFactory extends StoneFactory {
    private final List<String> COLORS_STRING = Colors.getColorsLikeString();

    private final List<Colors> COLORS = Colors.getColors();

    private Colors color;

    public Optional<Gemstone> create(String name, double weight, double cost,
                                     String origin, String group, String transp, String diff) {
        if (defaultCheck(weight, cost, origin, group, transp)) {
            if (!COLORS_STRING.contains(diff.toUpperCase())) {
                return Optional.empty();
            } else {
                int index = COLORS_STRING.indexOf(diff.toUpperCase());
                color = COLORS.get(index);
            }
            return Optional.of(new Diamond(name, weight, cost, origin, rank, transparency, color));
        }
        return Optional.empty();
    }
}
