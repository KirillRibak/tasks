package by.epam.training.necklace.model.factory;

import by.epam.training.necklace.model.factory.abstraction.StoneFactory;
import by.epam.training.necklace.model.stones.Pearl;
import by.epam.training.necklace.model.stones.abstraction.Gemstone;
import lombok.extern.log4j.Log4j2;

import java.util.Optional;

@Log4j2
public class PearlFactory extends StoneFactory {


    public Optional<Gemstone> create(String name, double weight, double cost,
                                     String origin, String group, String transp, String diff) {
        if (defaultCheck(weight, cost, origin, group, transp)) {
            try {
                int age = Integer.parseInt(diff);
                if (age > 0) {
                    return Optional.of(new Pearl(name, weight, cost,
                            origin, rank, transparency, age));
                }
            } catch (NumberFormatException e) {
                log.error(diff + " - is not age.");
            }
        }
        return Optional.empty();
    }
}
