package by.epam.training.necklace.model.factory;

import by.epam.training.necklace.model.factory.abstraction.StoneFactory;
import by.epam.training.necklace.model.stones.Ruby;
import by.epam.training.necklace.model.stones.abstraction.Gemstone;
import by.epam.training.necklace.model.stones.details.RubyGroup;

import java.util.List;
import java.util.Optional;

public class RubyFactory extends StoneFactory {

    private final List<String> RUBY_GROUP_STRING = RubyGroup.getRubyGroupsLikeString();

    private final List<RubyGroup> RUBY_GROUP = RubyGroup.getRubyGroups();

    private RubyGroup rubyGroup;

    public Optional<Gemstone> create(String name, double weight, double cost,
                                     String origin, String group, String transp, String diff) {
        if (defaultCheck(weight, cost, origin, group, transp)) {
            if (!RUBY_GROUP_STRING.contains(diff.toUpperCase())) {
                return Optional.empty();
            } else {
                int index = RUBY_GROUP_STRING.indexOf(diff.toUpperCase());
                rubyGroup = RUBY_GROUP.get(index);
            }
            return Optional.of(new Ruby(name, weight, cost, origin, rank, transparency, rubyGroup));
        }
        return Optional.empty();
    }
}
