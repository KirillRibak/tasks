package by.epam.training.necklace.model.factory;

import by.epam.training.necklace.model.repository.Requests;
import by.epam.training.necklace.model.repository.search.Search;
import by.epam.training.necklace.model.repository.search.SearchPriceInRange;
import by.epam.training.necklace.model.repository.search.SerchByName;

public class SearchFactory {
    public Search chooseRequest(Requests requests) {
        switch (requests) {
            case SEARCH_BY_PRICE_RANGE:
                return new SearchPriceInRange();
            case SEARCH_BY_NAME:
                return new SerchByName();
            default:
                throw new EnumConstantNotPresentException(requests.getDeclaringClass(), requests.name());
        }
    }
}
