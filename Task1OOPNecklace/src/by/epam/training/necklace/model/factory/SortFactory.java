package by.epam.training.necklace.model.factory;

import by.epam.training.necklace.model.repository.Requests;
import by.epam.training.necklace.model.repository.sort.SortByCost;
import by.epam.training.necklace.model.repository.sort.SortByTransparency;
import by.epam.training.necklace.model.repository.sort.SortByWeight;
import by.epam.training.necklace.model.stones.abstraction.Gemstone;

import java.util.Comparator;

public class SortFactory {
    public Comparator<Gemstone> chooseRequest(Requests requests) {
        switch (requests) {
            case WEIGHT:
                return new SortByWeight();
            case TRANSPARENCY:
                return new SortByTransparency();
            case COST:
                return new SortByCost();
            default:
                throw new EnumConstantNotPresentException(requests.getDeclaringClass(), requests.name());
        }
    }
}
