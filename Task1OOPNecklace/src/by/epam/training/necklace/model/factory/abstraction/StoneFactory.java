package by.epam.training.necklace.model.factory.abstraction;

import by.epam.training.necklace.model.stones.abstraction.Gemstone;
import by.epam.training.necklace.model.stones.details.GemstoneRank;
import by.epam.training.necklace.model.stones.details.Origin;
import by.epam.training.necklace.model.stones.details.Transparency;
import lombok.extern.log4j.Log4j2;

import java.util.List;
import java.util.Optional;

@Log4j2
public abstract class StoneFactory {

    protected final List<String> RANKS_STRING = GemstoneRank.getRanksLikeString();
    protected final List<String> TRANSPARENSIES_STRING = Transparency.getTransparenciesLiskeString();
    protected final List<String> ORIGIN_STRING = Origin.getOriginLikeString();

    protected final List<GemstoneRank> RANKS = GemstoneRank.getRanks();
    protected final List<Transparency> TRANSPARENSIES = Transparency.getTransparencies();

    protected GemstoneRank rank;
    protected Transparency transparency;

    public abstract Optional<Gemstone> create(String name, double weight, double cost,
                                              String origin, String group, String transp, String diff);

    protected boolean defaultCheck(double weight, double cost,
                                   String origin, String group, String transp) {
        if (weight <= 0) {
            log.debug("weight check false");
            return false;
        }
        if (cost < 0) {
            log.debug("cost check false");
            return false;
        }
        if (!ORIGIN_STRING.contains(origin.toUpperCase())) {
            log.debug("origin check false");
            return false;
        }
        if (!RANKS_STRING.contains(group.toUpperCase())) {
            log.debug("rank check false");
            return false;
        } else {
            int index = RANKS_STRING.indexOf(group.toUpperCase());
            rank = RANKS.get(index);
        }
        if (!TRANSPARENSIES_STRING.contains(transp.toUpperCase())) {
            log.debug("transparency check false");
            return false;
        } else {
            int index = TRANSPARENSIES_STRING.indexOf(transp.toUpperCase());
            transparency = TRANSPARENSIES.get(index);
        }
        return true;
    }


}
