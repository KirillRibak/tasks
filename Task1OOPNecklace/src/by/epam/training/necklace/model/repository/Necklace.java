package by.epam.training.necklace.model.repository;

import by.epam.training.necklace.model.factory.SearchFactory;
import by.epam.training.necklace.model.factory.SortFactory;
import by.epam.training.necklace.model.repository.search.Search;
import by.epam.training.necklace.model.stones.abstraction.Gemstone;
import by.epam.training.necklace.model.stones.abstraction.Stone;
import lombok.extern.log4j.Log4j2;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Repository class.
 * The model of real necklace.
 */
@Log4j2
public class Necklace {
    private static Necklace instance;

    private Necklace() {
    }

    public static   Necklace getInstance() {
        if (instance == null) {
            instance = new Necklace();
        }
        return instance;
    }

    /**
     * The stone id.
     */
    private  Integer id = 0;
    /**
     * Container.
     */

    private  Map<Integer, Gemstone> necklace = new LinkedHashMap<>() ;
    /**
     * Necklace price.
     */
    private  double cost;
    /**
     * Necklace weight.
     */
    private  double weight;

    /**
     * Add to necklace new stone.
     */
    public  void addStone(Optional<Gemstone> stone) {
        if (stone.isPresent()) {
            log.debug("stone exists");
            Gemstone gemstone = stone.get();
            necklace.put(id, gemstone);
            id++;
        }
    }

    public List<Gemstone> getAll(){
        List<Gemstone> list = new ArrayList<Gemstone>(necklace.values());
        return list;
    }

    public  void deleteById(int stoneId) {
        boolean flag = false;
        for(Integer key: necklace.keySet()){
            if(key.compareTo(stoneId)==0){
                flag = true;
            }
        }
        if (flag) {
            necklace.remove(stoneId);
        } else {
            log.error("Element with id " + stoneId + " is not exist.");
        }
    }

    public  Optional<Gemstone> getStone(int stoneId) {
        boolean flag = false;
        for(Integer key: necklace.keySet()){
            if(key.compareTo(stoneId)==0){
                flag = true;
            }
        }
        if (flag) {
            return Optional.of(necklace.get(stoneId));
        } else {
            log.error("Element with id " + stoneId + " is not exist.");
            return Optional.empty();
        }
    }

    public  double getCost() {
        for (Stone stone : necklace.values()) {
            cost += stone.getCost();
        }
        return cost;
    }

    public  double getWeight() {
        for (Stone stone : necklace.values()) {
            weight += stone.getWeight();
        }
        return weight;
    }

    public  Map<Integer, Gemstone> sort(Requests requests) {
        SortFactory sortFactory = new SortFactory();
        necklace = necklace.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(sortFactory.chooseRequest(requests)))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2,
                        LinkedHashMap::new));
        return necklace;
    }

    public  Map<Integer, Gemstone> sort() {
        necklace = necklace.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2,
                        LinkedHashMap::new));
        return necklace;
    }

    public  List<Gemstone> find(Requests requests, int leftBorder, int rightBorder) {
        List<Gemstone> gemsWithCondition = new ArrayList<>();
        SearchFactory searchFactory = new SearchFactory();
        Search searcher = searchFactory.chooseRequest(requests);
        for (Map.Entry<Integer, Gemstone> stone : necklace.entrySet()) {
            if (searcher.find(stone.getValue(), leftBorder, rightBorder)) {
                gemsWithCondition.add(stone.getValue());
            }
        }
        return gemsWithCondition;
    }

    public  List<Gemstone> find(Requests requests, String param) {
        List<Gemstone> gemsWithCondition = new ArrayList<>();
        SearchFactory searchFactory = new SearchFactory();
        Search searcher = searchFactory.chooseRequest(requests);
        for (Map.Entry<Integer, Gemstone> stone : necklace.entrySet()) {
            if (searcher.find(stone.getValue(), param)) {
                gemsWithCondition.add(stone.getValue());
            }
        }
        return gemsWithCondition;
    }

    public  int size() {
        return necklace.size();
    }

    public  boolean isEmpty(){
        if(necklace.size()==0){
            return true;
        }
        return false;
    }

    public  void removeAll(){
        id=0;
        necklace.clear();
    }
    @Override
    public String toString() {
        StringBuilder stones = new StringBuilder();
        for (Map.Entry stone : necklace.entrySet()) {
            stones.append("id-" + stone.getKey() + "\n" + stone.getValue().toString() + "\n");
        }
        return "Necklace{" +
                ", necklace exist:\n" + stones.toString() +
                ", cost=" + getCost() +
                ", weight=" + getWeight() +
                '}';
    }
}
