package by.epam.training.necklace.model.repository;

public enum Requests {
    WEIGHT, TRANSPARENCY, COST, SEARCH_BY_PRICE_RANGE, SEARCH_BY_NAME
}

