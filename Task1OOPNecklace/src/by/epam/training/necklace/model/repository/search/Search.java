package by.epam.training.necklace.model.repository.search;

import by.epam.training.necklace.model.stones.abstraction.Gemstone;

public interface Search {
    default boolean find(Gemstone gem, Object obj) {
        return false;
    }

    default boolean find(Gemstone gem, int leftBorder, int rightBorder) {
        return false;
    }
}
