package by.epam.training.necklace.model.repository.search;

import by.epam.training.necklace.model.stones.abstraction.Gemstone;

public class SearchPriceInRange implements Search {
    @Override
    public boolean find(Gemstone gem, int leftBorder, int rightBorder) {
        if (gem.getCost() > leftBorder && gem.getCost() < rightBorder) {
            return true;
        }
        return false;
    }
}

