package by.epam.training.necklace.model.repository.search;

import by.epam.training.necklace.model.stones.abstraction.Gemstone;

public class SerchByName implements Search {
    @Override
    public boolean find(Gemstone gem, Object obj) {
        String name = (String) obj;
        if (gem.getName().compareToIgnoreCase(name) == 0) {
            return true;
        }
        return false;
    }
}
