package by.epam.training.necklace.model.repository.sort;

import by.epam.training.necklace.model.stones.abstraction.Gemstone;

import java.util.Comparator;

public class SortByCost implements Comparator<Gemstone> {
    @Override
    public int compare(Gemstone o1, Gemstone o2) {
        if (o1.getCost() > o2.getCost()) {
            return 1;
        }
        if (o1.getCost() < o2.getCost()) {
            return -1;
        } else {
            return 0;
        }
    }
}
