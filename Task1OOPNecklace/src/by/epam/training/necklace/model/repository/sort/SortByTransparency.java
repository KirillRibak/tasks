package by.epam.training.necklace.model.repository.sort;

import by.epam.training.necklace.model.stones.abstraction.Gemstone;
import by.epam.training.necklace.model.stones.details.Transparency;

import java.util.Comparator;

public class SortByTransparency implements Comparator<Gemstone> {
    @Override
    public int compare(Gemstone o1, Gemstone o2) {
        if (o1.getTransparency() == Transparency.CLEAR && o2.getTransparency() == Transparency.CLEAR) {
            return 0;
        }
        if (o1.getTransparency() == Transparency.CLEAR && (o2.getTransparency() == Transparency.SEMI
                || o2.getTransparency() == Transparency.NON)) {
            return 1;
        }
        if (o1.getTransparency() == Transparency.SEMI && (o2.getTransparency() == Transparency.SEMI)) {
            return 0;
        }
        if (o1.getTransparency() == Transparency.SEMI && (o2.getTransparency() == Transparency.NON)) {
            return 1;
        }
        if (o1.getTransparency() == Transparency.SEMI && (o2.getTransparency() == Transparency.CLEAR)) {
            return -1;
        }
        if (o1.getTransparency() == Transparency.NON && (o2.getTransparency() == Transparency.NON)) {
            return 0;
        } else {
            return -1;
        }
    }
}
