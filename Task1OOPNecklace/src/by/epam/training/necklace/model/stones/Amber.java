package by.epam.training.necklace.model.stones;

import by.epam.training.necklace.model.stones.abstraction.Gemstone;
import by.epam.training.necklace.model.stones.details.GemstoneRank;
import by.epam.training.necklace.model.stones.details.Size;
import by.epam.training.necklace.model.stones.details.Transparency;
import lombok.EqualsAndHashCode;

/**
 * Amber is not a gem, but used like decoration.
 */
@EqualsAndHashCode
public class Amber extends Gemstone {
    /**
     * There are a lot of amber colors.
     */
    private Size size;

    /**
     * Constructor
     *
     * @param weight - weight
     * @param cost   - price
     * @param origin
     * @param size   - color
     */
    public Amber(String name, double weight, double cost, String origin, GemstoneRank rank,
                 Transparency transparency, Size size) {
        super(name, weight, cost, origin, rank, transparency);
        this.size = size;
    }

    /**
     * @return the size of amber.
     */
    public Size getSize() {
        return size;
    }

    /**
     * @param size - set the size of amber.
     */
    public void setSize(Size size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "Amber{" + super.toString() +
                "size=" + size +
                '}';
    }
}
