package by.epam.training.necklace.model.stones;

import by.epam.training.necklace.model.stones.abstraction.Gemstone;
import by.epam.training.necklace.model.stones.details.Colors;
import by.epam.training.necklace.model.stones.details.GemstoneRank;
import by.epam.training.necklace.model.stones.details.Transparency;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode
@ToString
public class Diamond extends Gemstone {
    private Colors color;

    public Diamond(String name, double weight, double cost, String origin, GemstoneRank rank, Transparency transparency, Colors color) {
        super(name, weight, cost, origin, rank, transparency);
        this.color = color;
    }

    public Colors getColor() {
        return color;
    }

    public void setColor(Colors color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Diamond{" + super.toString() +
                "color=" + color +
                '}';
    }
}
