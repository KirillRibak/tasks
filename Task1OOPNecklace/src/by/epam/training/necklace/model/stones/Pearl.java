package by.epam.training.necklace.model.stones;

import by.epam.training.necklace.model.stones.abstraction.Gemstone;
import by.epam.training.necklace.model.stones.details.GemstoneRank;
import by.epam.training.necklace.model.stones.details.Transparency;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class Pearl extends Gemstone {
    private int age;

    public Pearl(String name, double weight, double cost,
                 String origin, GemstoneRank rank,
                 Transparency transparency, int age) {
        super(name, weight, cost, origin, rank, transparency);
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Pearl{" + super.toString() +
                "age=" + age +
                '}';
    }
}
