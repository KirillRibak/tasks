package by.epam.training.necklace.model.stones;

import by.epam.training.necklace.model.stones.abstraction.Gemstone;
import by.epam.training.necklace.model.stones.details.GemstoneRank;
import by.epam.training.necklace.model.stones.details.RubyGroup;
import by.epam.training.necklace.model.stones.details.Transparency;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class Ruby extends Gemstone {
    private RubyGroup group;

    public Ruby(String name, double weight, double cost,
                String origin, GemstoneRank rank,
                Transparency transparency, RubyGroup group) {
        super(name, weight, cost, origin, rank, transparency);
        this.group = group;
    }

    public RubyGroup getGroup() {
        return group;
    }

    public void setGroup(RubyGroup group) {
        this.group = group;
    }

    @Override
    public String toString() {
        return "Ruby{" + super.toString() +
                "group=" + group +
                '}';
    }
}
