package by.epam.training.necklace.model.stones.abstraction;

import by.epam.training.necklace.model.stones.details.GemstoneRank;
import by.epam.training.necklace.model.stones.details.Transparency;

/**
 * Gemstone abstraction.
 */
public abstract class Gemstone extends Stone {
    /**
     * Gems there are ranks.
     * The higher the rank the higher the price.
     */
    private GemstoneRank rank;
    /**
     * The transparency influence the price.
     */
    private Transparency transparency;

    /**
     * Constructor.
     *
     * @param name         - the name of stone.
     * @param weight       - the weight of stone.
     * @param cost         - cost the stone.
     * @param origin       - parameter shows natural stone or not.
     * @param rank         - the rank of gemstone.
     * @param transparency - the transparency of gemstone.
     */
    public Gemstone(final String name, final double weight, final double cost,
                    final String origin, final GemstoneRank rank,
                    final Transparency transparency) {
        super(name, weight, cost, origin);
        this.rank = rank;
        this.transparency = transparency;
    }

    public GemstoneRank getRank() {
        return rank;
    }

    public void setRank(GemstoneRank rank) {
        this.rank = rank;
    }

    public Transparency getTransparency() {
        return transparency;
    }

    public void setTransparency(Transparency transparency) {
        this.transparency = transparency;
    }

    @Override
    public String toString() {
        return super.toString() +
                "rank=" + rank +
                ", transparency=" + transparency +
                '}';
    }
}
