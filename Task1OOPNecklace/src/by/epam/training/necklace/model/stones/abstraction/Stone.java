package by.epam.training.necklace.model.stones.abstraction;

/**
 * Stone abstraction.
 */
public abstract class Stone {
    private String name;
    private double weight;
    private double cost;
    private String origin;

    /**
     * Constructor.
     *
     * @param name   - the name of stone.
     * @param weight - the weight of stone.
     * @param cost   - cost the stone.
     * @param origin - parameter shows natural stone or not.
     */
    public Stone(String name, double weight, double cost, String origin) {
        this.name = name;
        this.weight = weight;
        this.cost = cost;
        this.origin = origin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String isNatural() {
        return origin;
    }

    public void setNatural(String natural) {
        origin = natural;
    }

    @Override
    public String toString() {
        return "name='" + name + '\'' +
                ", weight=" + weight +
                ", cost=" + cost +
                ", origin='" + origin + '\'' +
                '}';
    }
}
