package by.epam.training.necklace.model.stones.details;

import java.util.Arrays;
import java.util.List;

public enum Colors {
    YELLOW, GREEN, BLUE, ORANGE, PINK;

    private static List<Colors> colors = Arrays.asList(Colors.values());
    private static List<String> colorsLikeString;

    public static List<Colors> getColors() {
        return colors;
    }

    public static List<String> getColorsLikeString() {
        String clearData = colors.toString().replaceAll("[^A-Za-zА-Яа-я0-9-,]", "");
        String[] arr = clearData.split(",");
        colorsLikeString = Arrays.asList(arr);
        return colorsLikeString;
    }
}
