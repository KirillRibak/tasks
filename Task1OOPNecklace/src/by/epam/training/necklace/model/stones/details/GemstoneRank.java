package by.epam.training.necklace.model.stones.details;

import java.util.Arrays;
import java.util.List;

public enum GemstoneRank {
    FIRST, SECOND, DECORATION;

    private static List<GemstoneRank> ranks = Arrays.asList(GemstoneRank.values());
    private static List<String> ranksLikeString;

    public static List<GemstoneRank> getRanks() {
        return ranks;
    }

    public static List<String> getRanksLikeString() {
        String clearData = ranks.toString().replaceAll("[^A-Za-zА-Яа-я0-9-,]", "");
        String[] arr = clearData.split(",");
        ranksLikeString = Arrays.asList(arr);
        return ranksLikeString;
    }

}
