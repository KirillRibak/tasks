package by.epam.training.necklace.model.stones.details;

import java.util.Arrays;
import java.util.List;

public enum Origin {
    NATURAL, SYNTHETIC;
    private static List<Origin> origin = Arrays.asList(Origin.values());
    private static List<String> originLikeString;

    public static List<String> getOriginLikeString() {
        String clearData = origin.toString().replaceAll("[^A-Za-zА-Яа-я0-9-,]", "");
        String[] arr = clearData.split(",");
        originLikeString = Arrays.asList(arr);
        return originLikeString;
    }
}
