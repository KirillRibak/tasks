package by.epam.training.necklace.model.stones.details;

import java.util.Arrays;
import java.util.List;

public enum RubyGroup {
    BRIGHT, MEDIUM, LIGHT;
    private static List<RubyGroup> rubyGroups = Arrays.asList(RubyGroup.values());
    private static List<String> rubyGroupLikeSring;

    public static List<RubyGroup> getRubyGroups() {
        return rubyGroups;
    }

    public static List<String> getRubyGroupsLikeString() {
        String clearData = rubyGroups.toString().replaceAll("[^A-Za-zА-Яа-я0-9-,]", "");
        String[] arr = clearData.split(",");
        rubyGroupLikeSring = Arrays.asList(arr);
        return rubyGroupLikeSring;
    }
}
