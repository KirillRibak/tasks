package by.epam.training.necklace.model.stones.details;

import java.util.Arrays;
import java.util.List;

public enum Size {
    LARGE, MEDIUM, SMALL;

    private static List<Size> sizes = Arrays.asList(Size.values());
    private static List<String> sizesLikeString;

    public static List<Size> getSizes() {
        return sizes;
    }

    public static List<String> getSizesLikeString() {
        String clearData = sizes.toString().replaceAll("[^A-Za-zА-Яа-я0-9-,]", "");
        String[] arr = clearData.split(",");
        sizesLikeString = Arrays.asList(arr);
        return sizesLikeString;
    }
}
