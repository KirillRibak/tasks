package by.epam.training.necklace.model.stones.details;

public enum StoneName {
    PEARL, DIAMOND, RUBY, AMBER
}
