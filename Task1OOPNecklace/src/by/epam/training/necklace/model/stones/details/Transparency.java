package by.epam.training.necklace.model.stones.details;

import java.util.Arrays;
import java.util.List;

public enum Transparency {
    CLEAR, SEMI, NON;
    private static List<Transparency> transparencies = Arrays.asList(Transparency.values());
    private static List<String> transparenciesLikeString;

    public static List<Transparency> getTransparencies() {
        return transparencies;
    }

    public static List<String> getTransparenciesLiskeString() {
        String clearData = transparencies.toString().replaceAll("[^A-Za-zА-Яа-я0-9-,]", "");
        String[] arr = clearData.split(",");
        transparenciesLikeString = Arrays.asList(arr);
        return transparenciesLikeString;
    }
}
