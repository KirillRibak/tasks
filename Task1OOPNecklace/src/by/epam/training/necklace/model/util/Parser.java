package by.epam.training.necklace.model.util;

import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

@Log4j2
public class Parser {
    private static List<String[]> data = new ArrayList<>();

    private static boolean validate(File file) {
        if (!file.exists()) {
            return false;
        }
        if (file.length() == 0) {
            return false;
        }
        return true;
    }

    public static Optional<List<String[]>> process(File file) {
        if (validate(file)) {
            try (Scanner scan = new Scanner(file)) {
                scan.useDelimiter("\r\n");
                while (scan.hasNext()) {
                    String[] substring = scan.next().split(" ");
                    data.add(substring);
                }
            } catch (FileNotFoundException e) {
                log.error("File is not exist.");
            }
            return Optional.of(data);
        }
        return Optional.empty();
    }


}
