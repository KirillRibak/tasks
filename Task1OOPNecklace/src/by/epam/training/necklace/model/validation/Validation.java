package by.epam.training.necklace.model.validation;

import by.epam.training.necklace.model.exception.GemstoneException;
import by.epam.training.necklace.model.stones.details.StoneName;

public class Validation {
    /**
     * Checks the name against existing names.
     *
     * @param name - the nae of stone.
     * @return - true if the name against, false if name not against.
     * @throws GemstoneException - report that the stone does not exist.
     */
    public static   boolean validate(final String name) throws GemstoneException {
        for (StoneName stone : StoneName.values()) {
            if (stone.name().compareToIgnoreCase(name) == 0) {
                return true;
            }
        }
        throw new GemstoneException(name, " is not gem");
    }
}
