package test;

import by.epam.training.necklace.model.factory.AmberFactory;
import by.epam.training.necklace.model.factory.DiamondFactory;
import by.epam.training.necklace.model.factory.PearlFactory;
import by.epam.training.necklace.model.factory.RubyFactory;
import by.epam.training.necklace.model.stones.Amber;
import by.epam.training.necklace.model.stones.Diamond;
import by.epam.training.necklace.model.stones.Pearl;
import by.epam.training.necklace.model.stones.Ruby;
import by.epam.training.necklace.model.stones.details.*;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Optional;

public class FactoryTest {

    @DataProvider(name = "correctAmber")
    public static Object[][] getCorrectAmber() {
        return new Object[][]{
                {new Object[]{"amber", 10.0, 200.0,
                        "natural", "decoration", "clear", "medium"}, new AmberFactory(),
                        Optional.of(new Amber("amber", 10.0, 200.0,
                                "natural", GemstoneRank.DECORATION, Transparency.CLEAR, Size.MEDIUM))}

        };
    }

    @DataProvider(name = "incorrectAmber")
    public static Object[][] getInvalidAmber() {
        return new Object[][]{
                {new Object[]{"amber", -10.0, 0.0,
                        "natural", "decoration", "clesadfar", "medium"}, new AmberFactory(),}
        };
    }

    @DataProvider(name = "correctRuby")
    public static Object[][] getCorrectRuby() {
        return new Object[][]{
                {new Object[]{"ruby", 10.0, 200.0,
                        "natural", "first", "clear", "bright"}, new RubyFactory(),
                        Optional.of(new Ruby("ruby", 10.0, 200.0,
                                "natural", GemstoneRank.FIRST, Transparency.CLEAR, RubyGroup.BRIGHT))}
        };
    }

    @DataProvider(name = "incorrectRuby")
    public static Object[][] getInvalidRuby() {
        return new Object[][]{
                {new Object[]{"ruby", 10.0, 200.0,
                        "natural", "first", "clear", "jkj"}, new RubyFactory()}
        };
    }

    @DataProvider(name = "correctPearl")
    public static Object[][] getCorrectPearl() {
        return new Object[][]{
                {new Object[]{"pearl", 10.0, 200.0,
                        "natural", "second", "clear", "100"}, new PearlFactory(),
                        Optional.of(new Pearl("pearl", 10.0, 200.0,
                                "natural", GemstoneRank.SECOND, Transparency.CLEAR, 100))}
        };
    }

    @DataProvider(name = "incorrectPearl")
    public static Object[][] getInvalidPearl() {
        return new Object[][]{
                {new Object[]{"pearl", 10.0, 200.0,
                        "natural", "second", "non", "medium"}, new PearlFactory()}
        };
    }


    @DataProvider(name = "correctDiamond")
    public static Object[][] getCorrectDiamond() {
        return new Object[][]{
                {new Object[]{"diamond", 10.0, 200.0,
                        "natural", "first", "clear", "blue"}, new DiamondFactory(),
                        Optional.of(new Diamond("diamond", 10.0, 200.0,
                                "natural", GemstoneRank.FIRST, Transparency.CLEAR, Colors.BLUE))}
        };
    }

    @DataProvider(name = "incorrectDiamond")
    public static Object[][] getInvalidDiamond() {
        return new Object[][]{
                {new Object[]{"diamond", 10.0, 200.0,
                        "natural", "decoration", "clear", "lmkjj"}, new DiamondFactory()}
        };
    }


    @Test(description = "The factory receives the correct data" +
            " and must create an Amber object", dataProvider = "correctAmber")
    public void AmberFactoryShouldReturnOptionalWithGem(Object[] params, AmberFactory amberFactory,
                                                        Optional<Amber> valid) {
        Assert.assertEquals(amberFactory.create((String) params[0], (Double) params[1], (Double) params[2],
                (String) params[3], (String) params[4], (String) params[5], (String) params[6]), valid);
    }

    @Test(description = "The factory receives incorrect data" +
            " and must return the optonal <empty>", dataProvider = "incorrectAmber")
    public void AmberFactoryShouldReturnOptionalEmpty(Object[] params, AmberFactory amberFactory) {
        Assert.assertEquals(amberFactory.create((String) params[0], (Double) params[1], (Double) params[2],
                (String) params[3], (String) params[4], (String) params[5], (String) params[6]), Optional.empty());
    }

    @Test(description = "The factory receives valid data" +
            " and must create an Ruby object", dataProvider = "correctRuby")
    public void RubyFactoryShouldReturnOptionalWithGem(Object[] params, RubyFactory rubyFactory,
                                                       Optional<Ruby> valid) {
        Assert.assertEquals(rubyFactory.create((String) params[0], (Double) params[1], (Double) params[2],
                (String) params[3], (String) params[4], (String) params[5], (String) params[6]), valid);
    }

    @Test(description = "The factory receives incorrect data" +
            " and must return the optonal <empty>", dataProvider = "incorrectRuby")
    public void RubyFactoryShouldReturnOptionalEmpty(Object[] params, RubyFactory rubyFactory) {
        Assert.assertEquals(rubyFactory.create((String) params[0], (Double) params[1], (Double) params[2],
                (String) params[3], (String) params[4], (String) params[5], (String) params[6]), Optional.empty());
    }

    @Test(description = "The factory receives valid data" +
            " and must create an Pearl object", dataProvider = "correctPearl")
    public void PearlFactoryShouldReturnOptionalWithGem(Object[] params, PearlFactory pearlFactory,
                                                        Optional<Pearl> valid) {
        Assert.assertEquals(pearlFactory.create((String) params[0], (Double) params[1], (Double) params[2],
                (String) params[3], (String) params[4], (String) params[5], (String) params[6]), valid);
    }

    @Test(description = "The factory receives incorrect data" +
            " and must return the optonal <empty>", dataProvider = "incorrectPearl")
    public void PearlFactoryShouldReturnOptionalEmpty(Object[] params, PearlFactory pearlFactory) {
        Assert.assertEquals(pearlFactory.create((String) params[0], (Double) params[1], (Double) params[2],
                (String) params[3], (String) params[4], (String) params[5], (String) params[6]), Optional.empty());
    }

    @Test(description = "The factory receives the correct data" +
            " and must create an Diamond object", dataProvider = "correctDiamond")
    public void DiamondFactoryShouldReturnOptionalWithGem(Object[] params, DiamondFactory diamondFactory,
                                                          Optional<Diamond> valid) {
        Assert.assertEquals(diamondFactory.create((String) params[0], (Double) params[1], (Double) params[2],
                (String) params[3], (String) params[4], (String) params[5], (String) params[6]), valid);
    }

    @Test(description = "The factory receives incorrect data" +
            " and must return the optonal <empty>", dataProvider = "incorrectDiamond")
    public void DiamondFactoryShouldReturnOptionalEmpty(Object[] params, DiamondFactory diamondFactory) {
        Assert.assertEquals(diamondFactory.create((String) params[0], (Double) params[1], (Double) params[2],
                (String) params[3], (String) params[4], (String) params[5], (String) params[6]), Optional.empty());
    }
}
