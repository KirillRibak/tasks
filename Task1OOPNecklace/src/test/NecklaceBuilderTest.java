package test;

import by.epam.training.necklace.model.builder.GemstoneNecklaceBuilder;
import by.epam.training.necklace.model.builder.NecklaceBuilder;
import by.epam.training.necklace.model.repository.Necklace;
import by.epam.training.necklace.model.util.Parser;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.util.List;
import java.util.Optional;

public class NecklaceBuilderTest {

    @DataProvider(name="correctData")
    public static Object[][] takeCorrectData(){
        return new Object[][]{
              {Parser.process(new File("data/file.txt")),
                        new GemstoneNecklaceBuilder(Necklace.getInstance())}
        };
    }
    @Test(description = "GemstoneBuilder receive not empty data" +
            " should add Stones to Necklace ", dataProvider = "correctData")
    public void createSouldCreateNecklace(Optional<List<String[]>> data,
                                          GemstoneNecklaceBuilder builder){
        builder.createNecklace(data);
        Assert.assertFalse(Necklace.getInstance().isEmpty());
    }

    @DataProvider( name="incorrectData")
    public static Object[][] takeInvalidData(){
        return new Object[][]{
                {Parser.process(new File("data/emptyFile.txt")),
                        new GemstoneNecklaceBuilder(Necklace.getInstance())}
        };
    }
    @Test(description = "Necklace must be empty",dataProvider = "incorrectData")
    public void createSouldCreateEmptyNecklace(Optional<List<String[]>> data,
                                               GemstoneNecklaceBuilder builder){
        builder.createNecklace(data);
        Assert.assertTrue(Necklace.getInstance().isEmpty());
    }
}
