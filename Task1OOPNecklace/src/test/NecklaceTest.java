package test;

import by.epam.training.necklace.model.repository.Necklace;
import by.epam.training.necklace.model.repository.Requests;
import by.epam.training.necklace.model.stones.Amber;
import by.epam.training.necklace.model.stones.Diamond;
import by.epam.training.necklace.model.stones.Ruby;
import by.epam.training.necklace.model.stones.abstraction.Gemstone;
import by.epam.training.necklace.model.stones.details.*;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class NecklaceTest {

    private Optional<Gemstone> testGem = Optional.of(new Amber("amber", 10.0,100.0, "natural",
            GemstoneRank.DECORATION, Transparency.NON, Size.MEDIUM));
    private Optional<Gemstone> testGem2 = Optional.of(new Amber("amber", 10.2,100.0, "natural",
            GemstoneRank.DECORATION, Transparency.SEMI, Size.MEDIUM));
    private Optional<Gemstone> testGem3 = Optional.of(new Amber("amber", 11.0,200.0, "natural",
            GemstoneRank.DECORATION, Transparency.SEMI, Size.MEDIUM));
    private Optional<Gemstone> testGem4 =Optional.of( new Amber("amber", 12.0,370.0, "natural",
            GemstoneRank.DECORATION, Transparency.CLEAR, Size.MEDIUM));
    private Optional<Gemstone> testGem5 =Optional.of( new Ruby("ruby", 12.0,300.0, "natural",
            GemstoneRank.FIRST, Transparency.CLEAR, RubyGroup.BRIGHT));
    private Optional<Gemstone> testGem6 =Optional.of( new Diamond("diamond", 12.0,300.0, "natural",
            GemstoneRank.DECORATION, Transparency.CLEAR, Colors.BLUE));
    private Necklace necklace = Necklace.getInstance();

    private List<Gemstone> serchByName = Arrays.asList(testGem6.get());
    private List<Gemstone> serchInRange = Arrays.asList(testGem5.get(),testGem4.get());
    private List<Gemstone> sortByWeghtOrCostOrTransp = Arrays.asList(testGem.get(),testGem2.get(),testGem3.get(),testGem4.get());


    @AfterMethod
    public void removeData(){
        necklace.removeAll();
    }

    @Test()
    public void isEmptySouldReturnFalce(){
        necklace.addStone(testGem2);
        Assert.assertFalse(necklace.isEmpty());
    }

    @Test()
    public void isEmptySouldReturnTrue(){
        Assert.assertTrue(necklace.isEmpty());
    }

    @Test
    public void addSouldAddnewGemstoneToNecklace(){
        necklace.addStone(testGem);
        Assert.assertNotNull(necklace.getStone(0).get());
    }

    @Test
    public void getStoneSouldReturnGem(){
        necklace.addStone(testGem);
        Assert.assertEquals(necklace.getStone(0).get(),testGem.get());
    }

    @Test
    public void deleteStone(){
        necklace.addStone(testGem2);
        necklace.deleteById(0);
        Assert.assertEquals(necklace.getStone(0),Optional.empty());
    }

    @Test
    public void getWeightShouldReturnSummWeightAllStone(){
        necklace.addStone(testGem);
        necklace.addStone(testGem);
        Assert.assertTrue(necklace.getWeight()==20.0);
    }

    @Test(testName = "sort")
    public void sortByWeght(){
        necklace.addStone(testGem3);
        necklace.addStone(testGem);
        necklace.addStone(testGem2);
        necklace.addStone(testGem4);
        necklace.sort(Requests.WEIGHT);
        Assert.assertEquals(necklace.getAll(),sortByWeghtOrCostOrTransp);
    }

    @Test
    public void sortByCost(){
        necklace.addStone(testGem3);
        necklace.addStone(testGem);
        necklace.addStone(testGem2);
        necklace.addStone(testGem4);
        necklace.sort(Requests.COST);
        Assert.assertEquals(necklace.getAll(),sortByWeghtOrCostOrTransp);
    }

    @Test
    public void sortByTransparency(){
        necklace.addStone(testGem3);
        necklace.addStone(testGem);
        necklace.addStone(testGem2);
        necklace.addStone(testGem4);
        necklace.sort(Requests.TRANSPARENCY);
        Assert.assertEquals(necklace.getAll(),sortByWeghtOrCostOrTransp);
    }

    @Test
    public void defaultSort(){
        necklace.addStone(testGem3);
        necklace.addStone(testGem);
        necklace.addStone(testGem2);
        necklace.addStone(testGem4);
        necklace.sort();
        Assert.assertEquals(necklace.getAll(),sortByWeghtOrCostOrTransp);
    }

    @Test
    public void serchByName(){
        necklace.addStone(testGem3);
        necklace.addStone(testGem6);
        necklace.addStone(testGem2);
        necklace.addStone(testGem4);
        List<Gemstone> founded =necklace.find(Requests.SEARCH_BY_NAME,"diamond");
        Assert.assertEquals(founded,serchByName);
    }

    @Test
    public void serchInRange(){
        necklace.addStone(testGem3);
        necklace.addStone(testGem6);
        necklace.addStone(testGem2);
        necklace.addStone(testGem4);
        List<Gemstone> founded =necklace.find(Requests.SEARCH_BY_PRICE_RANGE,280,320);
        Assert.assertEquals(founded,serchByName);
    }

    @Test
    public void sizeSouldReturnTwo(){
        necklace.addStone(testGem3);
        necklace.addStone(testGem6);
        Assert.assertEquals(necklace.size(),2);
    }

}
