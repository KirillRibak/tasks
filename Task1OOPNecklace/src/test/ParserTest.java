package test;

import by.epam.training.necklace.model.util.Parser;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.util.List;
import java.util.Optional;

public class ParserTest {

    @Test
    public void parserShouldReturnOptionalEmptyFileIsEmpty(){
        Optional<List<String[]>> optionalEmty = Parser.process(new File("data/emptyFile.txt"));
        Assert.assertEquals(optionalEmty, Optional.empty() );
    }

    @Test
    public void parserShouldReturnOptionalEmptyFileNotExist(){
        Optional<List<String[]>> optionalEmty = Parser.process(new File("data/FileIsNotExist.txt"));
        Assert.assertEquals(optionalEmty, Optional.empty() );
    }

    @Test
    public void parserShouldReturnList(){
        List<String[]> list = Parser.process(new File("data/file.txt")).get();
        Assert.assertNotNull(list);
    }

}
