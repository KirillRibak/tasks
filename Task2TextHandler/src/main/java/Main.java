import сomposite.Composite;
import сomposite.FileInitializer;
import сomposite.TypeOfTextPart;

public class Main {
    public static void main(String[] args) {
        FileInitializer fileInitializer = new FileInitializer();
        String s = fileInitializer.initizie("data/data.txt");
        Composite composite = new Composite(TypeOfTextPart.PARAGRAPH,s);
        composite.parse();
        System.out.println(composite.toString());
    }
}
