package exception;

public class NotSupportOperationException extends Throwable {
    public NotSupportOperationException(String message) {
        super(message);
    }
}
