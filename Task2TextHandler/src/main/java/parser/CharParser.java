package parser;

import java.util.LinkedList;
import java.util.List;

public class CharParser implements Parser {
    private static final String CHAR_SPLIT_REGEX = "";
    private List<String> symbols = new LinkedList<>();

    public List<String> parse(String text) {
        String[] chars = text.split(CHAR_SPLIT_REGEX);
        for (String symbol : chars) {
            symbols.add(symbol);
        }
        return symbols;
    }
}
