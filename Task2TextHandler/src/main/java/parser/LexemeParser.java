package parser;

import java.util.LinkedList;
import java.util.List;

public class LexemeParser implements Parser {

    private static final String LEXEME_SPLIT_REGEX = "(\\s)";
    private List<String> newLexemes = new LinkedList<>();

    public List<String> parse(String string) {
        String[] lexemes = string.split(LEXEME_SPLIT_REGEX);
        for (String lexeme : lexemes) {
            newLexemes.add(lexeme);
        }
        return newLexemes;
    }
}
