package parser;

import lombok.extern.log4j.Log4j2;

import java.util.LinkedList;
import java.util.List;

@Log4j2
public class ParagraphParser implements Parser {

    private static final String PARAGRAPH_SPLIT_REGEX = "(?m)(?=^\\s{4})";
    private List<String> newParagraphs = new LinkedList<>();

    public List<String> parse(String string) {
        String[] paragraphs = string.split(PARAGRAPH_SPLIT_REGEX);
        for (String paragraph : paragraphs) {
            newParagraphs.add(paragraph);
        }
        return newParagraphs;
    }
}
