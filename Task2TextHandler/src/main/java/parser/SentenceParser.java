package parser;

import java.util.LinkedList;
import java.util.List;

public class SentenceParser implements Parser {

    private static final String SENTENCE_SPLIT_REGEX = "(?<=[a-z])\\.\\s+";
    private List<String> newSentences = new LinkedList<>();

    public List<String> parse(String text) {
       String[] sentenses = text.split(SENTENCE_SPLIT_REGEX);
        for (String sentence: sentenses){
            if(!sentence.contains(".")){
               newSentences.add(sentence+".");
            }else {newSentences.add(sentence);}
        }
        return newSentences;
    }
}
