package parser;

import java.util.LinkedList;
import java.util.List;

public class WordParser implements Parser {
    private static final String WORD_SPLIT_REGEX ="((?<=\\W)|(?=\\W))";
    private List<String> newWords = new LinkedList<>();

    public List<String> parse(String text) {
        String[] words = text.split(WORD_SPLIT_REGEX);
        for (String word: words){
                newWords.add(word);
        }
        return newWords;
    }
}
