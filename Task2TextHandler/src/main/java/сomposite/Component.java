package сomposite;

import exception.NotSupportOperationException;

import java.util.List;

public interface Component {
    void addPart(Component component) throws NotSupportOperationException;
    void parse() throws NotSupportOperationException;
    void remove(Component component) throws NotSupportOperationException;
}
