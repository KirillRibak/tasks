package сomposite;

import exception.NotSupportOperationException;
import lombok.extern.log4j.Log4j2;
import parser.*;

import java.util.LinkedList;
import java.util.List;

@Log4j2
public class Composite implements Component {
    private TypeOfTextPart type;

    public Composite(TypeOfTextPart type, String text) {
        this.type = type;
        this.text = text;
    }

    private List<Component> parts = new LinkedList<>();
    private String text;

    private List<Component> getParts() {
        return parts;
    }

    public void addPart(Component component) {
        parts.add(component);
    }

    public void parse() {
        switch (type) {
            case PARAGRAPH:
                Parser parserP = new ParagraphParser();
                List<String> paragraphs = parserP.parse(text);
                for (String paragraph : paragraphs) {
                    Component p = new Composite(TypeOfTextPart.SENTENCE, paragraph);
                    try {
                        p.parse();
                    } catch (NotSupportOperationException e) {
                        log.error("Don't support operation");
                    }
                    addPart(p);
                }
                break;
            case SENTENCE:
                Parser parserS = new SentenceParser();
                List<String> sentences = parserS.parse(text);
                for (String sentence : sentences) {
                    Component p = new Composite(TypeOfTextPart.LEXEME, sentence);
                    try {
                        p.parse();
                    } catch (NotSupportOperationException e) {
                        log.error("Don't support operation");
                    }
                    addPart(p);
                }
                break;
            case LEXEME:
                Parser parserL = new LexemeParser();
                List<String> lexemes = parserL.parse(text);
                for (String lexeme : lexemes) {
                    Component p = new Composite(TypeOfTextPart.WORD, lexeme);
                    try {
                        p.parse();
                    } catch (NotSupportOperationException e) {
                        log.error("Don't support operation");
                    }
                    addPart(p);
                }
                break;
            case WORD:
                Parser parserW = new WordParser();
                List<String> words = parserW.parse(text);
                for (String word : words) {
                    Component p = new Composite(TypeOfTextPart.SYMBOL, word);
                    try {
                        p.parse();
                    } catch (NotSupportOperationException e) {
                        log.error("Don't support operation");
                    }
                    addPart(p);
                }
                break;
            case SYMBOL:
                Parser parserC = new CharParser();
                List<String> chars = parserC.parse(text);
                for (String symbol : chars) {
                    Leaf leaf = new Leaf();
                    leaf.setSymbol(symbol);
                    addPart(leaf);
                }
                break;
                default:{
                    throw new EnumConstantNotPresentException(TypeOfTextPart.class, "Undefined constant");
                }
        }
    }


    @Override
    public void remove(Component component) {
        parts.remove(component);
    }

    public TypeOfTextPart getType() {
        return type;
    }

    public String toString() {
        StringBuilder result = new StringBuilder();
        for (Component component : parts) {
            if (component instanceof Composite) {
                Composite partOfComposite = (Composite)component;
                if (partOfComposite.getType().equals(TypeOfTextPart.PARAGRAPH)) {
                    result.append(component.toString());
                }
                if (partOfComposite.getType().equals(TypeOfTextPart.SENTENCE)) {
                    result.append("\n");
                    result.append(component.toString());
                }
                if (partOfComposite.getType().equals(TypeOfTextPart.LEXEME)) {
                    result.append("\n");
                    result.append(component.toString());
                }
                if (partOfComposite.getType().equals(TypeOfTextPart.WORD)) {
                    try {
                        result.append(" ");
                        for (Component symb: component.getParts()){
                            result.append(symb.toString());
                        }
                    } catch (NotSupportOperationException e) {
                        e.printStackTrace();
                    }
                }
            }
            else {
                result.append(component.toString());
            }
        }
        return result.toString();
    }

}