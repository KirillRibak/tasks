package сomposite;

import lombok.extern.log4j.Log4j2;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

@Log4j2
public class FileInitializer {
    public String initizie(String path) {
        String text = "";
        try (FileInputStream inFile = new FileInputStream(path);) {
            byte[] str = new byte[inFile.available()];
            inFile.read(str);
            text = new String(str);
        } catch (FileNotFoundException e) {
            log.error("File not found from path" + "\t" + path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text;
    }
}
