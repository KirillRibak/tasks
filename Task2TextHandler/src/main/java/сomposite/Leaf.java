package сomposite;

import exception.NotSupportOperationException;

import java.util.List;

public class Leaf implements Component {
    private String symbol;

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @Override
    public void addPart(Component component) throws NotSupportOperationException {
        throw new NotSupportOperationException("Operation addParts() not support.");//throw
    }

    @Override
    public void parse() throws NotSupportOperationException {
        throw new NotSupportOperationException("Operation parse() not support.");
    }

    @Override
    public void remove(Component component) throws NotSupportOperationException {
        throw new NotSupportOperationException("Operation remove() not support.");
    }

    @Override
    public String toString() {
        return symbol;
    }
}
