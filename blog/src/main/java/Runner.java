import by.epam.training.blog.dao.DefaultDaoFactory;
import by.epam.training.blog.dao.pool.ConnectionPool;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.service.DefaultServiceFactory;
import by.epam.training.blog.service.api.CategoryService;
import by.epam.training.blog.service.api.PostService;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.math.NumberUtils;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ResourceBundle;

@Log4j2
public class Runner {

    public static void main(String[] args) throws NoSuchAlgorithmException, DbException, IOException {


//        String login = "sdfsdf";
//        UserService userService = new DefaultUserService();
//        User user = userService.getByLogin(login);
//        System.out.println(user.toString());
//        DefaultUserService userService = new DefaultUserService();
//        DefaultPostService postService = new DefaultPostService();
//        List<DbPost> posts  = new ArrayList<>();
//        List <User>users = new ArrayList<>();
//        for(int i = 1; i<5; i ++){
//            users.add(userService.findUser(i));
//            log.debug(users.toString());
//            log.debug(i);
//            if(i<=4){
//                posts.add(postService.findPost(i));
//                log.debug(posts.toString());
//            }
//        }

//        Scanner scanner = new Scanner(System.in);
//        String admin = "admin";
//
//        String generatedSecuredPasswordHash = "$2a$12$sKNkoG0uCyXdcE.pLp.8lOgf44ulKhxX98US6hVYeSC8EX0ouQia.";
//        boolean matched = BCrypt.checkpw(admin, generatedSecuredPasswordHash);
//        System.out.println(matched);
//
        ConnectionPool.INSTANCE.init(ResourceBundle.getBundle("datasource"));
        DefaultServiceFactory.INSTANCE.setDaoFactory(DefaultDaoFactory.INSTANCE);
//        CommentService commentService = DefaultServiceFactory.INSTANCE.getService(CommentService.class);
//        CategoryService categoryService = DefaultServiceFactory.INSTANCE.getService(CategoryService.class);
//        List<DbComment> comments = commentService.getCommentForPost(2);
//        List<DbCategory> categories = categoryService.showPostCategories(2);
//        for (DbCategory category:categories){
//            System.out.println(category.getName());
//        }
//        for(DbComment comment : comments){
//            System.out.println(comment.getComment());
//        }
//        DefaultMainLogic mainPageLogic = new DefaultMainLogic();
//        List<ApplicationPost> postList= mainPageLogic.showPosts();
//        for (ApplicationPost post : postList){
//            System.out.println(post.toString());
//        }
//        PostService postService = DefaultServiceFactory.INSTANCE.getService(PostService.class);
//        DbPost post = postService.findPost(1);
//        Converter<DbPost,ApplicationPost> converter = new PostConverter();
//        ApplicationPost applicationPost = converter.convertDbEntityToApplicationEntity(post);
        ;

//        PostService postService = DefaultServiceFactory.INSTANCE.getService(PostService.class);
//        DbPost post0 = postService.findPost(1);
//        DbPost post1 = postService.findPost(2);
//        DbPost post2 = postService.findPost(3);
//        DbPost post3 = postService.findPost(4);
//        postService.addImgToPost(post0,new FileInputStream("C:\\uploads\\red.jpg"));
//        postService.addImgToPost(post0,new FileInputStream("C:\\uploads\\red1.jpg"));
//        postService.addImgToPost(post1,new FileInputStream("C:\\uploads\\spider.jpg"));
//        postService.addImgToPost(post1,new FileInputStream("C:\\uploads\\spider2.jpg"));
//        postService.addImgToPost(post2,new FileInputStream("C:\\uploads\\cyber.jpg"));
//        postService.addImgToPost(post2,new FileInputStream("C:\\uploads\\cyber1.jpg"));
//        postService.addImgToPost(post3,new FileInputStream("C:\\uploads\\cinema.jpg"));
//        postService.addImgToPost(post3,new FileInputStream("C:\\uploads\\cinema.jpg"));

//        UserService service = DefaultServiceFactory.INSTANCE.getService(UserService.class);
//        System.out.println("TRUE: "+service.isSigned(2,1));
//        System.out.println("FALSE: "+service.isSigned(13,1));

        PostService postService = DefaultServiceFactory.INSTANCE.getService(PostService.class);
//        DbPost post = postService.findPost(100);
//        System.out.println(post);
        CategoryService commentService = DefaultServiceFactory.INSTANCE.getService(CategoryService.class);
        //  User user = commentService.showAuthor(3);
        // System.out.println(user);

//        java.util.Date uDate = new java.util.Date();
//        System.out.println("Time in java.util.Date is : " + uDate);
//        java.sql.Date sDate = convertUtilToSql(uDate);
//        System.out.println("Time in java.sql.Date is : " + sDate);
//        DateFormat df = new SimpleDateFormat("dd/MM/YYYY  hh:mm:ss");
//        System.out.println("Using a dateFormat date is : " + df.format(uDate));.

        String one = "qwerqwer";
        String two= "1.0";
        String three = null;

            System.out.println(NumberUtils.isCreatable(one)+ one);
        System.out.println(NumberUtils.isCreatable(two)+ two);
        System.out.println(NumberUtils.isCreatable(three)+ three);


    }

}



