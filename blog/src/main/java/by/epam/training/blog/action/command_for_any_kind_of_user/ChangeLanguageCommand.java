package by.epam.training.blog.action.command_for_any_kind_of_user;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ChangeLanguageCommand extends UserCommand {
    private static final String COOKIE_LANG = "locale";

    @Override
    public Forward exec(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession session = request.getSession(false);
        String lang = (String) request.getAttribute("entityId");
//        Boolean isDeleted = (Boolean) session.getAttribute("isDeleted");
//        if (isDeleted == null) {
//            //String lang = (String) request.getAttribute("entityId");
//            Cookie cookie = null;
//            Cookie[] cookies = request.getCookies();
//            for (Cookie c : cookies) {
//                if (COOKIE_LANG.equals(c.getName())) {
//                    cookie = c;
//                }
//            }
//            cookie.setValue("");
//            cookie.setPath("/");
//            cookie.setMaxAge(0);
//            response.addCookie(cookie);
//            Cookie userNameCookieRemove = new Cookie(COOKIE_LANG, "");
//            userNameCookieRemove.setMaxAge(0);
//            response.addCookie(userNameCookieRemove);
//            session.setAttribute("isDeleted", true);
//            return new Forward("/blog/changeLang/" + lang);
//        } else {
//           // String lang = (String) request.getAttribute("entityId");
//            Cookie locale = new Cookie("locale", lang);
//            locale.setMaxAge(60 * 60);
//            response.addCookie(locale);
            session.setAttribute("locale", lang);
            return new Forward("/blog/main");
        }

    @Override
    protected void initParams(HttpServletRequest request) throws IOException, ServletException {

    }
}
//}

