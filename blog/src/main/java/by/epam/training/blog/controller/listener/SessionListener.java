package by.epam.training.blog.controller.listener;

import by.epam.training.blog.domain.application_entity.ApplicationUser;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.logic.DefaultLogicFactory;
import by.epam.training.blog.logic.api.VisitLogLogic;
import lombok.extern.log4j.Log4j2;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import java.util.Date;
@Log4j2
public class SessionListener implements HttpSessionAttributeListener {
    private final static String USER = "authorizedUser";
    private DefaultLogicFactory factory = new DefaultLogicFactory();
    private VisitLogLogic logLogic = factory.getLogic(VisitLogLogic.class);

    @Override
    public void attributeAdded(HttpSessionBindingEvent event) {
        HttpSession session = event.getSession();
        String sessionId = session.getId();
        String attributeName = event.getName();
        Object attributeValue = event.getValue();
        if (USER.equals(attributeName)) {
            ApplicationUser appUser = (ApplicationUser) attributeValue;
            long millis = System.currentTimeMillis();
            Date date = new Date(millis);
            String login = appUser.getLogin();
            try {
                logLogic.registerEntry(login,sessionId,date);
            } catch (DbException e) {
                log.error(e.getMessage());
            }
        }
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent event) {
        HttpSession session = event.getSession();
        String sessionId = session.getId();
        String attributeName = event.getName();
        Object attributeValue = event.getValue();
        if (USER.equals(attributeName)) {
            ApplicationUser appUser = (ApplicationUser) attributeValue;
            long millis = System.currentTimeMillis();
            Date date = new Date(millis);
            String login = appUser.getLogin();
            try {
                logLogic.registerOut(login,sessionId,date);
            } catch (DbException e) {
                log.error(e.getMessage());
            }
        }
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent event) {

    }
}
