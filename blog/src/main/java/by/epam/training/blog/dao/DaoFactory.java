package by.epam.training.blog.dao;

import by.epam.training.blog.exception.DbException;

public interface DaoFactory  {
    <T  extends Dao> T createDao(Class<T> key) throws DbException;
}