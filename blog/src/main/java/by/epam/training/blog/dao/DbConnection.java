package by.epam.training.blog.dao;

import java.sql.Connection;

public abstract class DbConnection {
    protected Connection connection;

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
}
