package by.epam.training.blog.dao;

import by.epam.training.blog.dao.api.*;
import by.epam.training.blog.dao.implementation.*;
import by.epam.training.blog.dao.pool.ConnectionPool;
import by.epam.training.blog.exception.DbException;

import java.sql.Connection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum DefaultDaoFactory implements DaoFactory {
    INSTANCE;
    private static Map<Class<? extends Dao>,Class<? extends DbConnection>> daoClasses = new ConcurrentHashMap<>();
    static {
        daoClasses.put(UserDao.class, DefaultUserDao.class);
        daoClasses.put(PostDao.class, DefaultPostDao.class);
        daoClasses.put(CategoryDao.class, DefaultCategoryDao.class);
        daoClasses.put(CommentDao.class, DefaultCommentDao.class);
        daoClasses.put(ImageDao.class, DefaultImgDao.class);
        daoClasses.put(PostWithCategoryDao.class,DefaultPostWithCategoryDao.class);
        daoClasses.put(UserSubscriberDao.class,DefaultUserSubscriberDao.class);
        daoClasses.put(LogDao.class,DefaultLogDao.class);
    }

    public <T extends Dao> T createDao(Class<T> key) throws DbException {
        Class<? extends DbConnection> value = daoClasses.get(key);
        if(value != null) {
            try {
                DbConnection dao = value.newInstance();
                Connection connection = ConnectionPool.INSTANCE.getConnection();
                dao.setConnection(connection);
                return (T)dao;
            } catch(InstantiationException | IllegalAccessException e) {
                throw new DbException(e);
            }
        }
        return null;
    }
}
