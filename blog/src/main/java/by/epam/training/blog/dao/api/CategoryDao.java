package by.epam.training.blog.dao.api;

import by.epam.training.blog.dao.Dao;
import by.epam.training.blog.domain.db_entity.DbCategory;
import by.epam.training.blog.exception.DbException;

import java.util.List;

public interface CategoryDao extends Dao {
     Integer addNewCategory(String name) throws DbException;
     String returnCategoryName(Integer categoryId)throws DbException;
     int returnCategoryIdByName(String name) throws DbException;
     List<DbCategory> getAllCategories()  throws DbException;
     boolean isExists(String categoryName) throws DbException;
     void deleteCategory(Integer idCategory) throws DbException;
}
