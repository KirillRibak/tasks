package by.epam.training.blog.dao.api;

import by.epam.training.blog.domain.db_entity.DbComment;
import by.epam.training.blog.exception.DbException;

import java.util.List;

public interface CommentDao extends CrudDao<DbComment> {
    List<DbComment> getCommentForUser(Integer userId) throws DbException;
    List<DbComment> getCommentForPost(Integer postId) throws DbException;
    Integer showAuthor(Integer commentId) throws DbException;
    Integer showComment(Integer commentId) throws DbException;
}
