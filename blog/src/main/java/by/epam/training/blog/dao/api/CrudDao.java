package by.epam.training.blog.dao.api;

import by.epam.training.blog.dao.Dao;
import by.epam.training.blog.domain.Entity;
import by.epam.training.blog.exception.DbException;

public interface CrudDao<Type extends Entity> extends Dao {
    Integer create(Type entity) throws DbException;

    Type read(Integer id) throws DbException;

    Integer update(Type entity) throws DbException;

    Integer delete(Integer id) throws DbException;

}
