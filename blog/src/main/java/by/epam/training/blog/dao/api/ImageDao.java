package by.epam.training.blog.dao.api;

import by.epam.training.blog.dao.Dao;
import by.epam.training.blog.exception.DbException;

import java.io.InputStream;
import java.util.List;

public interface ImageDao extends Dao {
    Integer addImgToPost(Integer postId, InputStream inputStream)throws DbException;
    List<String> showPostImg(Integer postId) throws DbException;
}
