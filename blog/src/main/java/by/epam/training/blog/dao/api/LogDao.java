package by.epam.training.blog.dao.api;

import by.epam.training.blog.dao.Dao;
import by.epam.training.blog.domain.db_entity.VisitEntry;
import by.epam.training.blog.exception.DbException;

import java.util.List;

public interface LogDao extends Dao {
    Integer registerEntry(String login,String sessionId, String enterTime) throws DbException;
    Integer registerOut(String login,String sessionId, String outTime)throws DbException;
    List<VisitEntry> getVisitLog()throws DbException;
}
