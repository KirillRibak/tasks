package by.epam.training.blog.dao.api;

import by.epam.training.blog.domain.db_entity.DbPost;
import by.epam.training.blog.exception.DbException;

import java.util.List;

public interface PostDao extends CrudDao<DbPost> {
    List<DbPost> findPostByTitle(String content) throws DbException;
    List<DbPost> findPostByText(String content) throws DbException;
    List<DbPost> findUserPosts(Integer dbUser)throws DbException;
    List<DbPost> showAllPost(int start, int recordsPerPage)throws DbException;
    int getNumberOfRows() throws DbException;
}
