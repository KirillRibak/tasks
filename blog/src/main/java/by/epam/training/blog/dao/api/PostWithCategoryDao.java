package by.epam.training.blog.dao.api;

import by.epam.training.blog.dao.Dao;
import by.epam.training.blog.exception.DbException;

import java.util.List;

public interface PostWithCategoryDao extends Dao {
    List<Integer> findPostByCategory(Integer categoryId) throws DbException;
    List<Integer> findCategoryByPost(Integer postId) throws DbException;
    void linkCategoryAndPost(Integer postId,Integer categoryId );
}
