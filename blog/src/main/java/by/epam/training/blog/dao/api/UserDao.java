package by.epam.training.blog.dao.api;

import by.epam.training.blog.domain.db_entity.User;
import by.epam.training.blog.exception.DbException;

import java.io.InputStream;
import java.util.List;

public interface UserDao extends CrudDao<User>{
    void changeRole(Integer role, Integer userId) throws DbException;

    User getByLogin(String login) throws DbException;
    List<User> getAllUsers() throws DbException;
    void addPicture(Integer userId, InputStream fis)throws DbException;
}
