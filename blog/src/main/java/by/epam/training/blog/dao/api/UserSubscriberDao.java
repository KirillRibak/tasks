package by.epam.training.blog.dao.api;

import by.epam.training.blog.dao.Dao;
import by.epam.training.blog.exception.DbException;

import java.util.List;

public interface UserSubscriberDao extends Dao {
    List<Integer> findUserSubscriber(Integer id) throws DbException;
    void subscribeOnUser(Integer sub,Integer user)throws DbException;
    void unsubscribeOnUser(Integer sub,Integer user)throws DbException;
    boolean isSigned(Integer sub,Integer user) throws DbException;
    List<Integer> showUserSubscriptions(Integer id) throws DbException;
}
