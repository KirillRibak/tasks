package by.epam.training.blog.dao.implementation;

import by.epam.training.blog.dao.DbConnection;
import by.epam.training.blog.dao.api.CategoryDao;
import by.epam.training.blog.domain.db_entity.DbCategory;
import by.epam.training.blog.exception.DbException;
import lombok.extern.log4j.Log4j2;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class DefaultCategoryDao extends DbConnection implements CategoryDao {

    private static final String CREATE_CATEGORY = "INSERT INTO category(name) VALUES(?);";
    private static final String SHOW_CATEGORY_NAME = "SELECT name FROM category WHERE id = ?;";
    private static final String SHOW_CATEGORY_ID = "SELECT id FROM category WHERE name = ?;";
    private static final String GET_ALL = "SELECT id,name FROM category";
    private static final String CATEGORY_EXISTS = "SELECT EXISTS(SELECT id,name FROM category " +
            "WHERE name = ?);";
    private static final String DELETE_CATEGORY = "DELETE FROM category WHERE id = ?";

    @Override
    public Integer addNewCategory(String name) throws DbException {
        try (PreparedStatement statement = connection.prepareStatement(CREATE_CATEGORY)) {
            statement.setString(1, name);
            statement.executeUpdate();
            return statement.RETURN_GENERATED_KEYS;
        } catch (SQLException e) {
            log.error("DbPost creation operation failed ");
            throw new DbException(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public String returnCategoryName(Integer categoryId) throws DbException {
        try (PreparedStatement statement = connection.prepareStatement(SHOW_CATEGORY_NAME)) {
            statement.setInt(1, categoryId);
            try (ResultSet result = statement.executeQuery()) {
                if (result.next()) {
                    return result.getString("name");
                } else {
                    log.error("Can not find category with id-" + categoryId);
                    throw new DbException();
                }
            }
        } catch (SQLException e) {
            log.error("User search operation failed ");
            throw new DbException(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int returnCategoryIdByName(String name) throws DbException {
        try (PreparedStatement statement = connection.prepareStatement(SHOW_CATEGORY_ID)) {
            statement.setString(1, name);
            try (ResultSet result = statement.executeQuery()) {
               while (result.next()) {
                    return result.getInt("id");
                }
            }
        } catch (SQLException e) {
            log.error("User search operation failed ");
            throw new DbException(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    @Override
    public List<DbCategory> getAllCategories() throws DbException {
        List<DbCategory> categories = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet result = statement.executeQuery(GET_ALL)) {
                while (result.next()) {
                    categories.add(collectCategory(result));
                }
            }
        } catch (SQLException e) {
            throw new DbException(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return categories;
    }

    @Override
    public boolean isExists(String categoryName) throws DbException {
        int result = 0;
        try (PreparedStatement statement = connection.prepareStatement(CATEGORY_EXISTS)) {
            statement.setString(1, categoryName);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    result = resultSet.getInt(1);
                }
            }
            if (result == 1) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            throw new DbException(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                log.error(e.getMessage());
            }
        }
    }

    @Override
    public void deleteCategory(Integer idCategory) throws DbException {
        try (PreparedStatement statement = connection.prepareStatement(DELETE_CATEGORY)) {
            statement.setInt(1, idCategory);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DbException(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                log.error(e.getMessage());
            }
        }
    }

        private DbCategory collectCategory (ResultSet result) throws SQLException {
            DbCategory category = new DbCategory();
            category.setId(result.getInt("id"));
            category.setName(result.getString("name"));
            return category;
        }
    }
