package by.epam.training.blog.dao.implementation;

import by.epam.training.blog.dao.api.CommentDao;
import by.epam.training.blog.dao.DbConnection;
import by.epam.training.blog.domain.db_entity.DbComment;
import by.epam.training.blog.exception.DbException;
import lombok.extern.log4j.Log4j2;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class DefaultCommentDao extends DbConnection implements CommentDao {

    private static final String CREATE_COMMENT = "INSERT INTO comment(author,post_id,comment_text) VALUES(?,?,?);";
    private static final String DELETE_COMMENT_BY_ID = "DELETE FROM comment WHERE id = ?;";
    private static final String FIND_COMMENT_BY_ID = "SELECT " +
            "id,author,post_id,comment_text FROM comment WHERE id = ?;";
    private static final String UPDATE_COMMENT = "UPDATE comment SET comment_text = ? WHERE id = ?;";
    private static final String SHOW_USER_COMMENTS = "SELECT id,author,post_id,comment_text FROM comment WHERE author = ?;";
    private static final String SHOW_POST_COMMENTS = "SELECT id,author,post_id,comment_text FROM comment WHERE post_id = ?;";
    private static final String SHOW_AUTHOR = "SELECT author FROM comment WHERE id = ?;";
    private static final String SHOW_POST = "SELECT post_id FROM comment WHERE id = ?;";

    @Override
    public List<DbComment> getCommentForUser(Integer userId) throws DbException {
        List<DbComment> dbComments = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(SHOW_USER_COMMENTS)) {
            statement.setInt(1, userId);
            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    dbComments.add(collectComment(result));
                }
            }
        } catch (SQLException e) {
            throw new DbException(e.getMessage());
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return dbComments;
    }

    @Override
    public List<DbComment> getCommentForPost(Integer postId) throws DbException {
        List<DbComment> dbComments = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(SHOW_POST_COMMENTS)) {
            statement.setInt(1, postId);
            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    dbComments.add(collectComment(result));
                }
            }
        } catch (SQLException e) {
            throw new DbException(e.getMessage());
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return dbComments;
    }

    @Override
    public Integer showAuthor(Integer commentId) throws DbException {
        try (PreparedStatement statement = connection.prepareStatement(SHOW_AUTHOR)) {
            statement.setInt(1, commentId);
            int authorId = 0;
            try (ResultSet result = statement.executeQuery()) {
                while (result.next()){
                   authorId = result.getInt("author");
                }
                return authorId;
            }
        } catch (SQLException e) {
            throw new DbException(e.getMessage());
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Integer showComment(Integer commentId) throws DbException {
        try (PreparedStatement statement = connection.prepareStatement(SHOW_POST)) {
            statement.setInt(1, commentId);
            try (ResultSet result = statement.executeQuery()) {
                return result.getInt("post_id");
            }
        } catch (SQLException e) {
            throw new DbException(e.getMessage());
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Integer create(DbComment entity) throws DbException {
        int result;
        try(PreparedStatement statement = connection.prepareStatement(CREATE_COMMENT)){
            statement.setInt(1,entity.getAuthor());
            statement.setInt(2,entity.getIdPost());
            statement.setString(3,entity.getComment());
            statement.executeUpdate();
            result = statement.RETURN_GENERATED_KEYS;
        } catch (SQLException e) {
            log.error("DbComment creation operation failed ");
            throw new DbException(e.getMessage());
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    @Override
    public DbComment read(Integer id) throws DbException {
        try (PreparedStatement statement = connection.prepareStatement(FIND_COMMENT_BY_ID)){
            statement.setInt(1,id);
            ResultSet result = statement.executeQuery();
            return collectComment(result);
        } catch (SQLException e) {
            throw new DbException(e.getMessage());
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Integer update(DbComment entity) throws DbException {
        try(PreparedStatement statement= connection.prepareStatement(UPDATE_COMMENT)){
            statement.setString(1,entity.getComment());
            statement.setInt(2,entity.getId());
            statement.executeUpdate();
            return statement.getUpdateCount();
        } catch (SQLException e) {
            throw new DbException(e.getMessage());
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Integer delete(Integer id) throws DbException {
        try(PreparedStatement statement= connection.prepareStatement(DELETE_COMMENT_BY_ID)){
            statement.setInt(1,id);
            statement.executeUpdate();
            return statement.getUpdateCount();
        } catch (SQLException e) {
            throw new DbException(e.getMessage());
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private DbComment collectComment(ResultSet result) throws SQLException {
        DbComment dbComment = new DbComment();
        dbComment.setId(result.getInt("id"));
        dbComment.setComment(result.getString("comment_text"));
        dbComment.setIdPost(result.getInt("post_id"));
        dbComment.setAuthor(result.getInt("author"));
        return dbComment;
    }
}
