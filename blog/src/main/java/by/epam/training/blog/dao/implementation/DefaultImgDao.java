package by.epam.training.blog.dao.implementation;

import by.epam.training.blog.dao.DbConnection;
import by.epam.training.blog.dao.api.ImageDao;
import by.epam.training.blog.exception.DbException;
import lombok.extern.log4j.Log4j2;

import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Log4j2
public class DefaultImgDao extends DbConnection implements ImageDao {

    private static final String ADD_PICTURE = "INSERT INTO image (post_id, image) VALUES (?,?);";
    private static final String SHOW_POST_PICTURES = "SELECT image FROM image WHERE post_id=?;";

    @Override
    public Integer addImgToPost(Integer postId, InputStream inputStream)
            throws DbException {
        try (PreparedStatement statement = connection.prepareStatement(ADD_PICTURE)) {
            statement.setInt(1, postId);
            statement.setBinaryStream(2, inputStream);
            statement.executeUpdate();
            int result = statement.RETURN_GENERATED_KEYS;
            return result;
        } catch (SQLException e) {
            throw new DbException(e.getMessage());
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public List<String> showPostImg(Integer postId) throws DbException {
        try(PreparedStatement statement = connection.prepareStatement(SHOW_POST_PICTURES)){
            List<String> imgs = new ArrayList<>();
            statement.setInt(1,postId);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    byte[] blob = resultSet.getBytes("image");
                    if (blob != null) {
                        String imgString = Base64.getEncoder().encodeToString(blob);
                        imgs.add(imgString);
                    }
                }
            }
            return imgs;
        } catch (SQLException e) {
            throw new DbException(e.getMessage());
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
