package by.epam.training.blog.dao.implementation;

import by.epam.training.blog.dao.DbConnection;
import by.epam.training.blog.dao.api.LogDao;
import by.epam.training.blog.domain.db_entity.VisitEntry;
import by.epam.training.blog.exception.DbException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DefaultLogDao extends DbConnection implements LogDao {
    private static final String REGISTER_ENTRY = "INSERT INTO visit_log(login,session_id,enter_time) " +
            "VALUES (?,?,?);";
    private static final String REGISTER_OUT = "UPDATE visit_log SET out_time = ?" +
            " WHERE login LIKE ? AND session_id LIKE ?;";
    private static final String SHOW_VISIT_LOG = "SELECT id,login,enter_time,out_time " +
            "FROM visit_log ORDER BY id DESC;";

    @Override
    public Integer registerEntry(String login, String sessionId, String enterTime) throws DbException {
        try (PreparedStatement statement = connection.prepareStatement(REGISTER_ENTRY)) {
            statement.setString(1, login);
            statement.setString(2, sessionId);
            statement.setString(3, enterTime);
            statement.executeUpdate();
            int updateRow = statement.getUpdateCount();
            return updateRow;
        } catch (SQLException e) {
            throw new DbException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Integer registerOut(String login, String sessionId, String outTime) throws DbException {
        try (PreparedStatement statement = connection.prepareStatement(REGISTER_OUT)) {
            statement.setString(1, outTime);
            statement.setString(2, login);
            statement.setString(3, sessionId);
            statement.executeUpdate();
            int updateRow = statement.getUpdateCount();
            return updateRow;
        } catch (SQLException e) {
            throw new DbException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public List<VisitEntry> getVisitLog() throws DbException {
        List<VisitEntry> visitLog = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(SHOW_VISIT_LOG);
            while (resultSet.next()) {
                visitLog.add(collectVisitLog(resultSet));
            }
        } catch (SQLException e) {
            throw new DbException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return visitLog;
    }

    private VisitEntry collectVisitLog(ResultSet resultSet) throws SQLException {
        VisitEntry entry = new VisitEntry();
        entry.setId(resultSet.getInt("id"));
        entry.setLogin(resultSet.getString("login"));
        entry.setEnter(resultSet.getString("enter_time"));
        entry.setOut(resultSet.getString("out_time"));
        return entry;
    }
}
