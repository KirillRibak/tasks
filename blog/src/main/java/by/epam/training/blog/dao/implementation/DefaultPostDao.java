package by.epam.training.blog.dao.implementation;

import by.epam.training.blog.dao.DbConnection;
import by.epam.training.blog.dao.api.PostDao;
import by.epam.training.blog.domain.db_entity.DbPost;
import by.epam.training.blog.exception.DbException;
import lombok.extern.log4j.Log4j2;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class DefaultPostDao extends DbConnection implements PostDao {

    private static final String CREATE_POST = "INSERT INTO post(title,author,post_text) VALUES(?,?,?);";
    private static final String FIND_POST_BY_SIMILAR_TITLE = "SELECT id,title,author,post_text,creation_stamp,last_changes" +
            " FROM post  WHERE title REGEXP ?;";
    private static final String FIND_POST_BY_SIMILAR_TEXT = "SELECT id,title,author,post_text,creation_stamp," +
            "last_changes FROM post  WHERE post_text REGEXP ? ;";
    private static final String FIND_POST_BY_ID = "SELECT id,title,author,post_text,creation_stamp," +
            "last_changes FROM post WHERE id = ?;";
    private static final String UPDATE_POST = "UPDATE POST SET title = ?, post_text = ? WHERE id = ?;";
    private static final String DELETE_POST = "DELETE FROM post WHERE id = ?;";
    private static final String SHOW_AUTHOR = "SELECT author FROM post WHERE id = ?;";
    private static final String SHOW_USER_POST = "SELECT id,title,author,post_text,creation_stamp," +
            "last_changes FROM post WHERE author = ?;";
    private static final String SHOW_ALL_POST = "SELECT id,title,author,post_text,creation_stamp," +
            "last_changes FROM post ORDER BY id DESC LIMIT ?,? ; ";
    private static final String GET_ROW_COUNT = "SELECT COUNT(id) FROM post;";
    private static final String SQL_REGEX_PART1 = "([[:blank:][:punct:]]|^)";
    private static final String SQL_REGEX_PART2 = "([[:blank:][:punct:]]|$)";

    public DefaultPostDao() {
    }

    @Override
    public List<DbPost> findPostByTitle(String content) throws DbException {
        return findPostBy(content, FIND_POST_BY_SIMILAR_TITLE);
    }

    @Override
    public List<DbPost> findPostByText(String content) throws DbException {
        return findPostBy(content, FIND_POST_BY_SIMILAR_TEXT);
    }

    private List<DbPost> findPostBy(String content, String findPostBySimilarTitle) throws DbException {
        List<DbPost> dbPosts = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(findPostBySimilarTitle)) {
            String currentRequestForDb = SQL_REGEX_PART1 + content + SQL_REGEX_PART2;
            statement.setString(1, currentRequestForDb);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                dbPosts.add(collectPost(result));
            }
        } catch (SQLException e) {
            throw new DbException(e.getMessage());
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return dbPosts;
    }


    @Override
    public List<DbPost> findUserPosts(Integer userId) throws DbException {
        List<DbPost> dbPosts = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(SHOW_USER_POST)) {
            statement.setInt(1, userId);
            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    dbPosts.add(collectPost(result));
                }
            }
        } catch (SQLException e) {
            throw new DbException(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return dbPosts;
    }

    @Override
    public List<DbPost> showAllPost(int start, int recordsPerPage) throws DbException {
        List<DbPost> dbPosts = new ArrayList<>();
        try (PreparedStatement statement= connection.prepareStatement(SHOW_ALL_POST)) {
            statement.setInt(1,start);
            statement.setInt(2,recordsPerPage);
            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    dbPosts.add(collectPost(result));
                }
            }
        } catch (SQLException e) {
            throw new DbException(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return dbPosts;
    }

    @Override
    public int getNumberOfRows() throws DbException {
        int rowValue = 0;
        try (Statement statement= connection.createStatement()) {
            try (ResultSet result = statement.executeQuery(GET_ROW_COUNT)) {
                while (result.next()) {
                   rowValue = result.getInt(1);
                }
            }
        } catch (SQLException e) {
            throw new DbException(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return rowValue;
    }

    @Override
    public Integer create(DbPost entity) throws DbException {
        int result;
        try (PreparedStatement statement = connection.prepareStatement(CREATE_POST)) {
            statement.setString(1, entity.getTitle());
            statement.setInt(2, entity.getAuthor());
            statement.setString(3, entity.getText());
            statement.executeUpdate();
            result = statement.RETURN_GENERATED_KEYS;
        } catch (SQLException e) {
            log.error("DbPost creation operation failed ");
            throw new DbException(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    @Override
    public DbPost read(Integer id) throws DbException {
        try (PreparedStatement statement = connection.prepareStatement(FIND_POST_BY_ID)) {
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            DbPost post = null;
            while (result.next()) {
                post = collectPost(result);
            }
            return post;
        } catch (SQLException e) {
            throw new DbException(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Integer update(DbPost entity) throws DbException {
        try (PreparedStatement statement = connection.prepareStatement(UPDATE_POST)) {
            statement.setString(1, entity.getTitle());
            statement.setString(2, entity.getText());
            statement.setInt(3, entity.getId());
            statement.executeUpdate();
            return statement.getUpdateCount();
        } catch (SQLException e) {
            throw new DbException(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Integer delete(Integer id) throws DbException {
        try (PreparedStatement statement = connection.prepareStatement(DELETE_POST)) {
            statement.setInt(1, id);
            statement.executeUpdate();
            return statement.getUpdateCount();
        } catch (SQLException e) {
            throw new DbException(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private DbPost collectPost(ResultSet result) throws SQLException {
        DbPost dbPost = new DbPost();
        dbPost.setId(result.getInt("id"));
        dbPost.setTitle(result.getString("title"));
        dbPost.setText(result.getString("post_text"));
        dbPost.setCreationStamp(result.getDate("creation_stamp"));
        dbPost.setChangeDate(result.getTimestamp("last_changes"));
        dbPost.setAuthor(result.getInt("author"));
        return dbPost;
    }
}
