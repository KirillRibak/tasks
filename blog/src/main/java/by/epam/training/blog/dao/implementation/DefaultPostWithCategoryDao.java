package by.epam.training.blog.dao.implementation;

import by.epam.training.blog.dao.DbConnection;
import by.epam.training.blog.dao.api.PostWithCategoryDao;
import by.epam.training.blog.exception.DbException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DefaultPostWithCategoryDao extends DbConnection implements PostWithCategoryDao {

    private static final String FIND_POST_BY_CATEGORY = "SELECT post_id FROM post_category" +
            " WHERE category_id = ?;";
    private static final String FIND_CATEGORY_BY_POST = "SELECT category_id FROM post_category" +
            " WHERE post_id = ?;";
    private static final String LINK_CATEGORY_AND_POST = "INSERT INTO post_category(post_id,category_id) VALUES (?,?);";

    @Override
    public List<Integer> findPostByCategory(Integer categoryId) throws DbException {
        List<Integer> posts_id = new ArrayList<>();
        try(PreparedStatement statement = connection.prepareStatement(FIND_POST_BY_CATEGORY)) {
            statement.setInt(1,categoryId);
            ResultSet result = statement.executeQuery();
            while (result.next()){
                posts_id.add(result.getInt("post_id"));
            }
        } catch (SQLException e) {
           throw new DbException(e.getMessage());
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return posts_id;
    }

    @Override
    public List<Integer> findCategoryByPost(Integer postId) throws DbException {
        List<Integer> categories_id = new ArrayList<>();
        try(PreparedStatement statement = connection.prepareStatement(FIND_CATEGORY_BY_POST)) {
            statement.setInt(1,postId);
            ResultSet result = statement.executeQuery();
            while (result.next()){
                categories_id.add(result.getInt("category_id"));
            }
        } catch (SQLException e) {
            throw new DbException(e.getMessage());
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return categories_id;
    }

    @Override
    public void linkCategoryAndPost(Integer postId, Integer categoryId) {
        try(PreparedStatement statement = connection.prepareStatement(LINK_CATEGORY_AND_POST)){
            statement.setInt(1,postId);
            statement.setInt(2,categoryId);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
