package by.epam.training.blog.dao.implementation;

import by.epam.training.blog.dao.DbConnection;
import by.epam.training.blog.dao.api.UserDao;
import by.epam.training.blog.domain.db_entity.User;
import by.epam.training.blog.exception.DbException;
import lombok.extern.log4j.Log4j2;

import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Log4j2
public class DefaultUserDao extends DbConnection implements UserDao {

    private static final String CREATE_USER = "INSERT INTO user(login,password,email) VALUES(?,?,?);";
    private static final String FIND_USER_BY_ID = "SELECT " +
            "id,login,password,email,creation_stamp,last_changes,img,role,about_me" +
            " FROM user WHERE id = ?;";
    private static final String ADD_USER_PICTURE = "UPDATE user SET img = ? WHERE id = ?;";
    private static final String FIND_USER_BY_LOGIN = "SELECT " +
            "id,login,password,email,creation_stamp,last_changes,img,role,about_me" +
            " FROM user WHERE login = ?;";
    private static final String SELECT_ALL_USERS = "SELECT " +
            "id,login,password,email,creation_stamp,last_changes,img,role,about_me FROM user " +
            "ORDER BY login;";
    private static final String DELETE_USER_BY_ID = "DELETE FROM user WHERE id = ?;";
    private static final String UPDATE_USER = "UPDATE user SET password = ?, email = ?,about_me = ? WHERE id = ?;";
    private static final String CHANGE_ROLE ="UPDATE user SET role = ? WHERE id =?;";


    public DefaultUserDao() {
    }

    @Override
    public void changeRole(Integer role,Integer userId) throws DbException {
        try(PreparedStatement statement = connection.prepareStatement(CHANGE_ROLE)){
            statement.setInt(1,role);
            statement.setInt(2,userId);
            statement.executeUpdate();
        }  catch (SQLException e) {
            throw new DbException(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public Integer create(User dbUser) throws DbException {
        int result;
        try (PreparedStatement statement = connection.prepareStatement(CREATE_USER)) {
            statement.setString(1, dbUser.getLogin());
            statement.setString(2, dbUser.getPassword());
            statement.setString(3, dbUser.getEmail());
            statement.executeUpdate();
            result = statement.RETURN_GENERATED_KEYS;
        } catch (SQLException e) {
            log.error("User creation operation failed ");
            throw new DbException(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }


    @Override
    public User read(Integer id) throws DbException {

        try (PreparedStatement statement = connection.prepareStatement(FIND_USER_BY_ID)) {
            statement.setInt(1, id);
            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    return collectUserEntity(result);
                }
            }
        } catch (SQLException e) {
            log.error("User search operation failed ");
            throw new DbException(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        throw new DbException("can't find user with id " + id);
    }

    @Override
    public Integer update(User entity) throws DbException {
        try (PreparedStatement statement = connection.prepareStatement(UPDATE_USER)) {
            statement.setString(1, entity.getPassword());
            statement.setString(2, entity.getEmail());
            statement.setString(3,entity.getAboutMe());
            statement.setInt(4, entity.getId());
            statement.executeUpdate();
            return statement.getUpdateCount();
        } catch (SQLException e) {
            log.error("User update operation failed ");
            throw new DbException(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Integer delete(Integer id) throws DbException {
        try (PreparedStatement statement = connection.prepareStatement(DELETE_USER_BY_ID)) {
            statement.setInt(1, id);
            statement.executeUpdate();
            return statement.getUpdateCount();
        } catch (SQLException e) {
            log.error("User delete operation failed ");
            throw new DbException(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public User getByLogin(String login) throws DbException {
        try (PreparedStatement statement = connection.prepareStatement(FIND_USER_BY_LOGIN)) {
            statement.setString(1, login);
            try (ResultSet result = statement.executeQuery()) {
                if (result.next()) {
                    return collectUserEntity(result);
                } else {
                    log.error("Can not find user with login-" + login);
                    throw new DbException();
                }
            }
        } catch (SQLException e) {
            log.error("User search operation failed ");
            throw new DbException(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public List<User> getAllUsers() throws DbException {
        try (PreparedStatement statement = connection.prepareStatement(SELECT_ALL_USERS)) {
            List<User> dbUsers = new ArrayList<>();
            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    dbUsers.add(collectUserEntity(result));
                }
            }
            return dbUsers;
        } catch (SQLException e) {
            throw new DbException(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void addPicture(Integer userId, InputStream fis) throws DbException {
        try (PreparedStatement statement = connection.prepareStatement(ADD_USER_PICTURE)) {
            statement.setBinaryStream(1, fis);
            statement.setInt(2,userId);
            statement.executeUpdate();
        } catch (SQLException e) {
            log.error("User picture update operation failed ");
            throw new DbException(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private User collectUserEntity(ResultSet result) throws SQLException {
        User dbUser = new User();
        dbUser.setId(result.getInt("id"));
        dbUser.setLogin(result.getString("login"));
        dbUser.setPassword(result.getString("password"));
        dbUser.setEmail(result.getString("email"));
        dbUser.setCreationStamp(result.getDate("creation_stamp"));
        dbUser.setChangeTime(result.getDate("last_changes"));
        dbUser.setAboutMe(result.getString("about_me"));
        byte[] blob = result.getBytes("img");
        if (blob != null) {
            String imgString = Base64.getEncoder().encodeToString(blob);
            dbUser.setImg(imgString);
        }
        dbUser.setRole(result.getInt("role"));
        return dbUser;
    }
}
