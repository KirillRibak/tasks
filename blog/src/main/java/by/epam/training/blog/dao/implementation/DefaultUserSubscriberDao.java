package by.epam.training.blog.dao.implementation;

import by.epam.training.blog.dao.DbConnection;
import by.epam.training.blog.dao.api.UserSubscriberDao;
import by.epam.training.blog.exception.DbException;
import lombok.extern.log4j.Log4j2;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class DefaultUserSubscriberDao extends DbConnection implements UserSubscriberDao {

    private static final String FIND_USER_SUBS = "SELECT subscriber_id FROM user_subscription" +
            " WHERE user_id = ?";
    private static final String SUBSCRIBE_ON_USER = "INSERT INTO user_subscription(subscriber_id,user_id)" +
            "VALUES (?,?);";
    private static final String UNSUBSCRIBE_ON_USER = "DELETE FROM user_subscription WHERE subscriber_id = ? " +
            "AND user_id = ?;";
    private static final String IS_SIGNED = "SELECT EXISTS(SELECT user_id, subscriber_id FROM user_subscription " +
            "WHERE subscriber_id = ? AND user_id = ?);";
    private static final String SHOW_USER_SUBSCRIPTIONS = "SELECT user_id FROM " +
            "user_subscription WHERE subscriber_id = ?;";

    @Override
    public List<Integer> findUserSubscriber(Integer id) throws DbException {
        List<Integer> sub_id = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(FIND_USER_SUBS)) {
            statement.setInt(1, id);
            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    sub_id.add(result.getInt("subscriber_id"));
                }
            }
        } catch (SQLException e) {
            throw new DbException(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                log.error(e.getMessage());
            }
        }
        return sub_id;
    }

    @Override
    public void subscribeOnUser(Integer sub, Integer user) throws DbException {
        try (PreparedStatement statement = connection.prepareStatement(SUBSCRIBE_ON_USER)) {
            statement.setInt(1, sub);
            statement.setInt(2, user);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DbException(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                log.error(e.getMessage());
            }
        }
    }

    @Override
    public void unsubscribeOnUser(Integer sub, Integer user) throws DbException {
        try (PreparedStatement statement = connection.prepareStatement(UNSUBSCRIBE_ON_USER)) {
            statement.setInt(1, sub);
            statement.setInt(2, user);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DbException(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                log.error(e.getMessage());
            }
        }
    }

    @Override
    public boolean isSigned(Integer sub, Integer user) throws DbException {
        int result = 0;
        try(PreparedStatement statement =connection.prepareStatement(IS_SIGNED)){
            statement.setInt(1,sub);
            statement.setInt(2,user);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    result = resultSet.getInt(1);
                }
            }
            if (result==1){
                 return true;
             }else {
                 return false;
             }
        }catch (SQLException e) {
            throw new DbException(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                log.error(e.getMessage());
            }
        }
    }

    @Override
    public List<Integer> showUserSubscriptions(Integer id) throws DbException {
        try (PreparedStatement statement = connection.prepareStatement(SHOW_USER_SUBSCRIPTIONS)) {
            List<Integer> userIds = new ArrayList<>();
            statement.setInt(1, id);
            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    userIds.add(result.getInt("user_id"));
                }
            }
            return userIds;
        } catch (SQLException e) {
            throw new DbException(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
