package by.epam.training.blog.dao.pool;

import by.epam.training.blog.exception.DbException;
import lombok.extern.log4j.Log4j2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Log4j2
public  enum  ConnectionPool {
    INSTANCE;
    private Lock lock = new ReentrantLock();
    private String driver;
    private String url;
    private String user;
    private String password;
    private int maxSize;
    private int minSize;
    private int checkConnectionTimeout;

    private BlockingQueue<PooledConnection> freeConnections = new LinkedBlockingQueue<>();
    private Set<PooledConnection> usedConnections = new ConcurrentSkipListSet<>();


    public Connection getConnection() throws DbException {
        lock.lock();
        PooledConnection connection = null;
        while(connection == null) {
            try {
                if(!freeConnections.isEmpty()) {
                    connection = freeConnections.take();
                    if(!connection.isValid(checkConnectionTimeout)) {
                        try {
                            connection.getConnection().close();
                        } catch(SQLException e) {}
                        connection = null;
                    }
                } else if(usedConnections.size() < maxSize) {
                    connection = createConnection();
                } else {
                    log.error("The limit of number of database connections is exceeded");
                    throw new DbException();
                }
            } catch(InterruptedException | SQLException e) {
                log.error("It is impossible to connect to a database", e);
                throw new DbException(e);
            }
        }
        usedConnections.add(connection);
        log.debug(String.format("Connection was received from pool. Current pool size: %d used connections; %d free connection", usedConnections.size(), freeConnections.size()));
        lock.unlock();
        return connection;
    }

    void freeConnection(PooledConnection connection) {
        lock.lock();
        try {
            if(connection.isValid(checkConnectionTimeout)) {
                connection.clearWarnings();
                connection.setAutoCommit(true);
                usedConnections.remove(connection);
                freeConnections.put(connection);
                log.debug(String.format("Connection was returned into pool. Current pool size: %d used connections; %d free connection", usedConnections.size(), freeConnections.size()));
            }
        } catch(SQLException | InterruptedException e1) {
            log.warn("It is impossible to return database connection into pool", e1);
            try {
                connection.getConnection().close();
            } catch(SQLException e2) {}
        }
        lock.unlock();
    }

    public  void init(ResourceBundle resourceBundle) throws DbException {
        lock.lock();
        try {
            destroy();
            this.url = resourceBundle.getString("jdbcUrl");
            this.user = resourceBundle.getString("dataSource.user");
            this.password = resourceBundle.getString("dataSource.password");
            this.driver = resourceBundle.getString("driver");
            this.maxSize =Integer.valueOf(resourceBundle.getString("maxpoolsize"));
            this.minSize = Integer.valueOf(resourceBundle.getString("minpoolsize"));
            this.checkConnectionTimeout = Integer.valueOf(resourceBundle.getString("connectiontimeout"));
            Class.forName(driver);
            for(int counter = 0; counter < minSize; counter++) {
                freeConnections.put(createConnection());
            }
        } catch(ClassNotFoundException | SQLException | InterruptedException e) {
            log.fatal("It is impossible to initialize connection pool", e);
            throw new DbException(e);
        }

        lock.unlock();
    }


    private PooledConnection createConnection() throws SQLException {
        return new PooledConnection(DriverManager.getConnection(url, user, password));
    }

    public  void destroy() {
        lock.lock();
        usedConnections.addAll(freeConnections);
        freeConnections.clear();
        for(PooledConnection connection : usedConnections) {
            try {
                connection.getConnection().close();
            } catch(SQLException e) {}
        }
        usedConnections.clear();
        lock.unlock();
    }

    public Lock getLock() {
        return lock;
    }
}