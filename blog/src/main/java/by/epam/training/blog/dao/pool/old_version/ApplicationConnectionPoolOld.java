package by.epam.training.blog.dao.pool.old_version;

import lombok.extern.log4j.Log4j2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

@Log4j2
public class ApplicationConnectionPoolOld implements ConnectionPoolOld {
    private static int MAX_CONNECTION_POOL_SIZE = 20;
    private static int MIN_CONNECTION_POOL_SIZE = 10;

    private ReentrantLock lock = new ReentrantLock();

    private String url;
    private String user;
    private String password;
    private String driverName;
    private BlockingQueue<Connection> connectionPool;
    private Set<Connection> usedConnections;

    public static ApplicationConnectionPoolOld getInstance() {
        return LazySomethingHolder.singletonInstance;
    }

    private static class LazySomethingHolder {
        public static ApplicationConnectionPoolOld singletonInstance =
                new ApplicationConnectionPoolOld(ResourceBundle.getBundle("datasource"));
    }

    private ApplicationConnectionPoolOld(ResourceBundle resourceBundle) {
        this.url = resourceBundle.getString("jdbcUrl");
        this.user = resourceBundle.getString("dataSource.user");
        this.password = resourceBundle.getString("dataSource.password");
        this.driverName = resourceBundle.getString("driver");
        this.connectionPool = new LinkedBlockingQueue<>();
        this.usedConnections = new CopyOnWriteArraySet<>();
        log.debug("init fields");
        changePoolSize(resourceBundle);
        log.debug("out init method enter load driver method");
        loadDriver();
        log.debug("out load driver method");
        log.debug("enter init method");
        init();
        log.debug("out init method");
    }

    private void init() {
        log.debug("start init method");
        for (int i = 0; i < MIN_CONNECTION_POOL_SIZE; i++) {
            try {
                connectionPool.add(createConnection());
            } catch (SQLException e) {
                log.debug("The error in init method");
                log.error(e.getMessage());
            }
        }
        log.debug("finish init method");
    }

    private Connection createConnection() throws SQLException {
        return DriverManager.getConnection(url, user, password);
    }

    private void loadDriver() {
        log.debug("start load driver method");
        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            System.out.println("Unable to load class.");
            e.printStackTrace();
        }
        log.debug("finifsh load driver method");
    }

    public Connection getConnection() {
        lock.lock();
        try {
            if (connectionPool.isEmpty()) {
                if (usedConnections.size() < MAX_CONNECTION_POOL_SIZE) {
                    try {
                        connectionPool.add(createConnection());
                    } catch (SQLException e) {
                        log.error(e.getMessage());
                    }
                } else {
                    throw new RuntimeException(
                            "Maximum pool size reached, no available connections!");
                }
            }
            Connection connection = null;
            try {
                connection = connectionPool.take();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            usedConnections.add(connection);
            return connection;
        } finally {
            lock.unlock();
        }
    }

    public boolean releaseConnection(Connection connection) {
        lock.lock();
        log.debug("Before unused: " + connectionPool.size());
        log.debug("Before used: " + usedConnections.size());
        connectionPool.add(connection);
        boolean flag = usedConnections.remove(connection);
        log.debug("After unused: " + connectionPool.size());
        log.debug("After used: " + usedConnections.size());
        log.debug("Connection was returned : " + flag);
        lock.unlock();
        return flag;
    }

    public int getSize() {
        lock.lock();
        try {
            return connectionPool.size() + usedConnections.size();
        } finally {
            lock.unlock();
        }
    }

    public void shutdown()  {
        lock.lock();
        try {
            usedConnections.forEach(this::releaseConnection);
            for (Connection c : connectionPool) {
                c.close();
            }
            connectionPool.clear();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    private void changePoolSize(ResourceBundle resourceBundle) {
        if (resourceBundle.containsKey("maxpoolsize")) {
            try {
                int newSize = Integer.parseInt(resourceBundle.getString("maxpoolsize"));
                if (newSize > MIN_CONNECTION_POOL_SIZE) {
                    MAX_CONNECTION_POOL_SIZE = newSize;
                }
            } catch (ClassCastException e) {
                log.error("Invalid value maxpoolsize");
            }
        }
        if (resourceBundle.containsKey("minpoolsize")) {
            try {
                int newSize = Integer.parseInt(resourceBundle.getString("minpoolsize"));
                if (newSize < MAX_CONNECTION_POOL_SIZE) {
                    MIN_CONNECTION_POOL_SIZE = newSize;
                }
            } catch (ClassCastException e) {
                log.error("Invalid value minpoolsize");
            }
        }
    }
}
