package by.epam.training.blog.dao.pool.old_version;

import lombok.extern.log4j.Log4j2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

@Log4j2
public class BasicConnectionPoolOld implements ConnectionPoolOld {

    private String url;
    private String user;
    private String password;
    private String driverName;
    private final List<Connection> usedConnections = new ArrayList<>();
    private static final int INITIAL_POOL_SIZE = 10;
    private final int MAX_POOL_SIZE = 20;
    List<Connection> pool = new ArrayList<>(INITIAL_POOL_SIZE);

    public static BasicConnectionPoolOld getInstance() {
        return BasicConnectionPoolOld.LazySomethingHolder.singletonInstance;
    }

    private static class LazySomethingHolder {
        public static BasicConnectionPoolOld singletonInstance =
                new BasicConnectionPoolOld(ResourceBundle.getBundle("datasource"));
    }

    private void init() {
        for (int i = 0; i < INITIAL_POOL_SIZE; i++) {
            pool.add(createConnection(url, user, password));
        }
    }

    private BasicConnectionPoolOld(ResourceBundle resourceBundle) {
        this.url = resourceBundle.getString("jdbcUrl");
        this.user = resourceBundle.getString("dataSource.user");
        this.password = resourceBundle.getString("dataSource.password");
        this.driverName = resourceBundle.getString("driver");
        init();
    }

    @Override
    public Connection getConnection() {
        if (pool.isEmpty()) {
            if (usedConnections.size() < MAX_POOL_SIZE) {
                pool.add(createConnection(url, user, password));
            } else {
                throw new RuntimeException("Maximum pool size reached, no available connections!");
            }
        }

        Connection connection = pool.remove(pool.size() - 1);
        usedConnections.add(connection);
        return connection;
    }

    @Override
    public boolean releaseConnection(Connection connection) {
        pool.add(connection);
        return usedConnections.remove(connection);
    }

    private static Connection createConnection(String url, String user, String password) {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            throw new RuntimeException();
        }
        return connection;
    }

    @Override
    public int getSize() {
        return pool.size() + usedConnections.size();
    }

    @Override
    public void shutdown() {
        usedConnections.forEach(this::releaseConnection);
        for (Connection c : pool) {
            try {
                c.close();
            } catch (SQLException e) {
                throw new RuntimeException();
            }
        }
        pool.clear();
    }
}