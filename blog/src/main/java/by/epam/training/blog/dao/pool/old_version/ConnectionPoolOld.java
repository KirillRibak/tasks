package by.epam.training.blog.dao.pool.old_version;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface ConnectionPoolOld {
    Connection getConnection() ;

    boolean releaseConnection(Connection connection);

    int getSize();

    void shutdown();
}

