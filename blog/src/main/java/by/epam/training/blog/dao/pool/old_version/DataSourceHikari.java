package by.epam.training.blog.dao.pool.old_version;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class DataSourceHikari {

    private static HikariConfig config = new HikariConfig("datasource.properties");
    private static com.zaxxer.hikari.HikariDataSource ds;

    static {
        ds = new HikariDataSource( config );
    }

    private DataSourceHikari() {}

    public static Connection getConnection() throws SQLException {
        return ds.getConnection();
    }

}
