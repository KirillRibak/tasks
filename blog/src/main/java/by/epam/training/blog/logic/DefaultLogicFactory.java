package by.epam.training.blog.logic;

import by.epam.training.blog.logic.api.*;
import by.epam.training.blog.logic.implementation.*;
import lombok.extern.log4j.Log4j2;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
@Log4j2
public class DefaultLogicFactory implements LogicFactory{
    private Map<Class<? extends Logic>, Class<? extends LogicWithServices>> logic = new ConcurrentHashMap<>();
    {
        logic.put(CategoryLogic.class, DefaultCategoryLogic.class);
        logic.put(SearchLogic.class, DefaultSearchLogic.class);
        logic.put(LoginLogic.class, DefaultLoginLogic.class);
        logic.put(MainLogic.class, DefaultMainLogic.class);
        logic.put(PostLogic.class, DefaultPostLogic.class);
        logic.put(ProfileLogic.class, DefaultProfileLogic.class);
        logic.put(RegistrationLogic.class,DefaultRegistrationLogic.class);
        logic.put(ShowUserLogic.class,DefaultShowUserLogic.class);
        logic.put(UsersLogic.class,DefaultUsersLogic.class);
        logic.put(VisitLogLogic.class,DefaultVisitLogLogic.class);
    }

    @Override
    public <T extends Logic> T getLogic(Class<T> key) {

        Class<? extends LogicWithServices> value = logic.get(key);
        if(value != null) {
            try {
                LogicWithServices logic = value.newInstance();
                return (T) logic;
            } catch(InstantiationException | IllegalAccessException e) {
               log.error(e.getMessage());
            }
        }
        return null;
    }
}
