package by.epam.training.blog.logic;

import by.epam.training.blog.domain.application_entity.ApplicationUser;
import by.epam.training.blog.exception.DbException;

public interface Logic {
     ApplicationUser getUser(Integer id) throws DbException;
}
