package by.epam.training.blog.logic;

public interface LogicFactory {
    <T extends Logic> T getLogic(Class<T>key);
}
