package by.epam.training.blog.logic;

import by.epam.training.blog.dao.DefaultDaoFactory;
import by.epam.training.blog.domain.application_entity.ApplicationCategory;
import by.epam.training.blog.domain.application_entity.ApplicationComment;
import by.epam.training.blog.domain.application_entity.ApplicationPost;
import by.epam.training.blog.domain.application_entity.ApplicationUser;
import by.epam.training.blog.domain.db_entity.DbCategory;
import by.epam.training.blog.domain.db_entity.DbComment;
import by.epam.training.blog.domain.db_entity.DbPost;
import by.epam.training.blog.domain.db_entity.User;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.logic.entity_converter.Converter;
import by.epam.training.blog.logic.entity_converter.ConverterEnum;
import by.epam.training.blog.logic.entity_converter.ConverterFactory;
import by.epam.training.blog.service.DefaultServiceFactory;
import by.epam.training.blog.service.ServiceFactory;
import by.epam.training.blog.service.api.*;

public abstract class LogicWithServices implements Logic{
    protected Converter<DbPost, ApplicationPost> postConverter = ConverterFactory.INSTANCE.getConverter(ConverterEnum.POST);
    protected Converter<User, ApplicationUser> userConverter = ConverterFactory.INSTANCE.getConverter(ConverterEnum.USER);
    protected Converter<DbCategory, ApplicationCategory> categoryConverter = ConverterFactory.INSTANCE.getConverter(ConverterEnum.CATEGORY);
    protected Converter<DbComment, ApplicationComment> commentConverter = ConverterFactory.INSTANCE.getConverter(ConverterEnum.COMMENT);


    private ServiceFactory getFactory() throws DbException {
        DefaultServiceFactory.INSTANCE.setDaoFactory(DefaultDaoFactory.INSTANCE);
        return  DefaultServiceFactory.INSTANCE;
    }

    protected VisitLogService getVisitLogService() throws DbException {
        return getFactory().getService(VisitLogService.class);
    }

    protected CategoryService getCategoryService() throws DbException {
        return getFactory().getService(CategoryService.class);
    }

    protected UserService getUserService() throws DbException {
        return getFactory().getService(UserService.class);
    }

    protected PostService getPostService() throws DbException {
        return getFactory().getService(PostService.class);
    }

    protected CommentService getCommentService() throws DbException {
        return getFactory().getService(CommentService.class);
    }

    public ApplicationUser getUser(Integer id) throws DbException {
        User user = getUserService().findUser(id);
        return userConverter.convertDbEntityToApplicationEntity(user);
    }
}
