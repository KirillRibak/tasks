package by.epam.training.blog.logic.api;

import by.epam.training.blog.domain.application_entity.ApplicationUser;
import by.epam.training.blog.logic.Logic;

public interface LoginLogic extends Logic {
    boolean checkAuthorizationData(String login, String password);

    ApplicationUser getUser(String login, String password);
}
