package by.epam.training.blog.logic.api;

import by.epam.training.blog.domain.application_entity.ApplicationPost;
import by.epam.training.blog.logic.Logic;

import java.util.Set;

public interface SearchLogic extends Logic {
     Set<ApplicationPost> showFoundPosts(String request);
}
