package by.epam.training.blog.logic.api;

import by.epam.training.blog.domain.application_entity.ApplicationUser;
import by.epam.training.blog.logic.Logic;

public interface ShowUserLogic extends Logic {
    ApplicationUser showUser(String login);

    void subscribeOnUser(Integer subscriberId, Integer userId);

    void unsubscribeOnUser(Integer subscriberId, Integer userId);

    boolean isSigned(Integer sub, Integer user);
}
