package by.epam.training.blog.logic.api;

import by.epam.training.blog.domain.db_entity.VisitEntry;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.logic.Logic;

import java.util.Date;
import java.util.List;

public interface VisitLogLogic extends Logic {
    Integer registerEntry(String login,String sessionId, Date enterTime) throws DbException;
    Integer registerOut(String login,String sessionId, Date outTime)throws DbException;
    List<VisitEntry> getVisitLog()throws DbException;
}
