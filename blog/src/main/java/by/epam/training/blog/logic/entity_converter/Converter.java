package by.epam.training.blog.logic.entity_converter;

import by.epam.training.blog.dao.DefaultDaoFactory;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.service.DefaultServiceFactory;
import by.epam.training.blog.service.ServiceFactory;
import by.epam.training.blog.service.api.CategoryService;
import by.epam.training.blog.service.api.CommentService;
import by.epam.training.blog.service.api.PostService;
import by.epam.training.blog.service.api.UserService;

public interface Converter<DbEntity,ApplicationEntity> {
    public  ApplicationEntity convertDbEntityToApplicationEntity(DbEntity dbEntity);

    default    ServiceFactory getFactory() throws DbException {
        DefaultServiceFactory.INSTANCE.setDaoFactory(DefaultDaoFactory.INSTANCE);
        return  DefaultServiceFactory.INSTANCE;
    }

    default CategoryService getCategoryService() throws DbException {
        return getFactory().getService(CategoryService.class);
    }

    default UserService getUserService() throws DbException {
        return getFactory().getService(UserService.class);
    }

    default PostService getPostService() throws DbException {
        return getFactory().getService(PostService.class);
    }

    default CommentService getCommentService() throws DbException {
        return getFactory().getService(CommentService.class);
    }

}
