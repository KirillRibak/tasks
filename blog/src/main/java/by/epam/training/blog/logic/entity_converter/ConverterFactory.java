package by.epam.training.blog.logic.entity_converter;

import by.epam.training.blog.logic.entity_converter.implementation.CategoryConverter;
import by.epam.training.blog.logic.entity_converter.implementation.CommentConverter;
import by.epam.training.blog.logic.entity_converter.implementation.PostConverter;
import by.epam.training.blog.logic.entity_converter.implementation.UserConverter;
import lombok.extern.log4j.Log4j2;

@Log4j2
public enum  ConverterFactory {
    INSTANCE;
    public  Converter getConverter(ConverterEnum value) {
        switch (value) {
            case POST: {
                return new PostConverter();
            }
            case USER: {
                return new UserConverter();
            }
            case COMMENT: {
                return new CommentConverter();
            }
            case CATEGORY: {
                return new CategoryConverter();
            }
            default:{
                log.error("Converter "+value+" is not exists");
                throw new IllegalArgumentException("Converter "+value+" is not exists");
            }
        }
    }
}
