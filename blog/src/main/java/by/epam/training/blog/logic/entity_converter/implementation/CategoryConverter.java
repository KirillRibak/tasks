package by.epam.training.blog.logic.entity_converter.implementation;

import by.epam.training.blog.domain.application_entity.ApplicationCategory;
import by.epam.training.blog.domain.db_entity.DbCategory;
import by.epam.training.blog.domain.db_entity.DbPost;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.logic.entity_converter.Converter;

import java.util.ArrayList;
import java.util.List;

public class CategoryConverter implements Converter<DbCategory, ApplicationCategory> {

    @Override
    public ApplicationCategory convertDbEntityToApplicationEntity(DbCategory dbCategory) {
        ApplicationCategory appCategory = new ApplicationCategory();
        try{
            appCategory.setId(dbCategory.getId());
            appCategory.setName(dbCategory.getName());
            List<Integer> postIdCollection = getCategoryService().
                    findPostByCategory(dbCategory.getId());
            List<DbPost> dbPosts = new ArrayList<>();
            for (Integer postId:postIdCollection){
                DbPost dbPost = getPostService().findPost(postId);
                dbPosts.add(dbPost);
            }
            return appCategory;
        }catch (DbException e){

        }
        return appCategory;
    }
}
