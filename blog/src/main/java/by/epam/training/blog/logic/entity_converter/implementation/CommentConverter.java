package by.epam.training.blog.logic.entity_converter.implementation;

import by.epam.training.blog.domain.application_entity.ApplicationComment;
import by.epam.training.blog.domain.db_entity.DbComment;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.logic.entity_converter.Converter;

public class CommentConverter implements Converter<DbComment, ApplicationComment> {

    @Override
    public ApplicationComment convertDbEntityToApplicationEntity(DbComment dbComment) {
        ApplicationComment appComment = new ApplicationComment();
        try{
            appComment.setId(dbComment.getId());
            appComment.setDbPost(getPostService().findPost(dbComment.getIdPost()));
            appComment.setAuthor(getUserService().findUser(dbComment.getAuthor()));
            appComment.setComment(dbComment.getComment());
            return appComment;
        }catch (DbException e){

        }
        return appComment;
    }
}
