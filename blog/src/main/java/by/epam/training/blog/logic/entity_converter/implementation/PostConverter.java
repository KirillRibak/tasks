package by.epam.training.blog.logic.entity_converter.implementation;

import by.epam.training.blog.domain.application_entity.ApplicationComment;
import by.epam.training.blog.domain.application_entity.ApplicationPost;
import by.epam.training.blog.domain.db_entity.DbCategory;
import by.epam.training.blog.domain.db_entity.DbComment;
import by.epam.training.blog.domain.db_entity.DbPost;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.logic.entity_converter.Converter;
import by.epam.training.blog.service.api.PostService;
import by.epam.training.blog.service.api.UserService;

import java.util.List;

public class PostConverter implements Converter<DbPost, ApplicationPost> {

    private CommentConverter commentConverter = new CommentConverter();

    @Override
    public ApplicationPost convertDbEntityToApplicationEntity(DbPost dbPost) {
        ApplicationPost appPost = new ApplicationPost();
        try{
            UserService usrService = getUserService();
            PostService postService = getPostService();
            appPost.setId(dbPost.getId());
            appPost.setTitle(dbPost.getTitle());
            appPost.setText(dbPost.getText());
            appPost.setCreationStamp(dbPost.getCreationStamp());
            appPost.setAuthor(usrService.findUser(dbPost.getAuthor()));
            appPost.setChangeDate(dbPost.getChangeDate());
            List<String> imgs = postService.showPostImg(dbPost.getId());
            for(String img: imgs){
                appPost.addImage(img);
            }
            List<DbComment> dbComments =
                    getCommentService().getCommentForPost(dbPost.getId());
            for(DbComment dbComment : dbComments){
                ApplicationComment appComment = commentConverter.
                        convertDbEntityToApplicationEntity(dbComment);
                appPost.addComment(appComment);
            }
            List<DbCategory> dbCategories = getCategoryService().
                    showPostCategories(dbPost.getId());
            for(DbCategory dbCategory:dbCategories){
                appPost.addCategory(dbCategory);
            }
            return appPost;
        }catch (DbException e){

        }
        return appPost;
    }
}
