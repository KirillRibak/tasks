package by.epam.training.blog.logic.entity_converter.implementation;

import by.epam.training.blog.domain.Role;
import by.epam.training.blog.domain.application_entity.ApplicationComment;
import by.epam.training.blog.domain.application_entity.ApplicationPost;
import by.epam.training.blog.domain.application_entity.ApplicationUser;
import by.epam.training.blog.domain.db_entity.DbComment;
import by.epam.training.blog.domain.db_entity.DbPost;
import by.epam.training.blog.domain.db_entity.User;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.logic.entity_converter.Converter;

import java.util.List;

public class UserConverter implements Converter<User,ApplicationUser> {
    private PostConverter postConverter = new PostConverter();
    private CommentConverter commentConverter = new CommentConverter();

    @Override
    public ApplicationUser convertDbEntityToApplicationEntity(User dbUser) {
        ApplicationUser appUser = new ApplicationUser();
        try {
            appUser.setId(dbUser.getId());
            appUser.setLogin(dbUser.getLogin());
            appUser.setEmail(dbUser.getEmail());
            appUser.setPassword(dbUser.getPassword());
            appUser.setCreationStamp(dbUser.getCreationStamp());
            appUser.setChangeTime(dbUser.getChangeTime());
            appUser.setImg(dbUser.getImg());
            appUser.setAboutMe(dbUser.getAboutMe());
            appUser.setRole(Role.getByIdentity(dbUser.getRole()));
            List<DbPost> dbPosts = getPostService().findUserPosts(dbUser.getId());
            for (DbPost dbPost: dbPosts) {
                ApplicationPost appPost =
                        postConverter.convertDbEntityToApplicationEntity(dbPost);
                appUser.addAppPost(appPost);
            }
            List<DbComment> dbComments = getCommentService().getCommentForUser(dbUser.getId());
            for (DbComment dbComment: dbComments) {
                ApplicationComment appComment =
                        commentConverter.convertDbEntityToApplicationEntity(dbComment);
                appUser.addAppComment(appComment);
            }
            List<User> dbUsers = getUserService().showUserSubscribers(dbUser.getId());
            for (User sub : dbUsers){
                appUser.addSubscriber(sub);

            }
            return appUser;
        } catch (DbException e) {
            e.printStackTrace();
        }
        return appUser;
    }
}
