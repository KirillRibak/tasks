package by.epam.training.blog.logic.implementation;

import by.epam.training.blog.domain.application_entity.ApplicationPost;
import by.epam.training.blog.domain.db_entity.DbPost;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.logic.LogicWithServices;
import by.epam.training.blog.logic.api.CategoryLogic;
import by.epam.training.blog.service.api.CategoryService;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.List;

@Log4j2
public class DefaultCategoryLogic extends LogicWithServices implements CategoryLogic {
    public List<ApplicationPost> showPosts(Integer categoryId) {
        List<ApplicationPost> appPosts = new ArrayList<>();
        List<DbPost> dbPosts = new ArrayList<>();
        try {
            dbPosts = getPostService().findPostsByCategory(categoryId);
            for (DbPost dbPost : dbPosts) {
                appPosts.add(postConverter.convertDbEntityToApplicationEntity(dbPost));
            }
            return appPosts;
        } catch (DbException e) {
            log.error(e.getMessage());
        }
        return appPosts;
    }

    public String getCategoryName(int id) {
        try {
            CategoryService categoryService = getCategoryService();
            String name = categoryService.returnCategoryName(id);
            return name;
        } catch (DbException e) {
            log.error(e.getMessage());
        }
        return "Category";
    }

    @Override
    public boolean isExists(String name) {
        boolean isExists = false;
        try {
           isExists = getCategoryService().isExists(name);
        } catch (DbException e) {
            log.error(e.getMessage());
        }
        return isExists;
    }

    @Override
    public boolean deleteCategory(Integer categoryId) {
        boolean result = false;
        try {
           result = getCategoryService().deleteCategory(categoryId);
        } catch (DbException e) {
            log.error(e.getMessage());
        }
        return result;
    }

    @Override
    public void createCategory(String category) {
        try {
            getCategoryService().addNewCategory(category);
        } catch (DbException e) {
            log.error(e.getMessage());
        }
    }
}
