package by.epam.training.blog.logic.implementation;

import by.epam.training.blog.domain.application_entity.ApplicationUser;
import by.epam.training.blog.domain.db_entity.User;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.logic.LogicWithServices;
import by.epam.training.blog.logic.api.LoginLogic;
import by.epam.training.blog.logic.bcrypt.PasswordChecker;

public class DefaultLoginLogic extends LogicWithServices implements LoginLogic {

    @Override
    public boolean checkAuthorizationData(String login, String password) {
        User dbUser = null;
        try {
            dbUser = getUserService().getByLogin(login);
        } catch (DbException e) {
        }
        if (dbUser != null) {
            String hash = dbUser.getPassword();
            boolean isValid = PasswordChecker.checkPassword(password, hash);
            return isValid;
        }
        return false;
    }

    @Override
    public ApplicationUser getUser(String login, String password) {
        User dbUser = null;
        try {
            dbUser = getUserService().getByLogin(login);
        } catch (DbException e) {
        }
        if (dbUser != null) {
            String hash = dbUser.getPassword();
            ApplicationUser applicationUser = userConverter.
                    convertDbEntityToApplicationEntity(dbUser);
            return applicationUser;
        }
        return null;
    }
}
