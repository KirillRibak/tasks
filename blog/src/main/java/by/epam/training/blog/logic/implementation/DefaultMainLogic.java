package by.epam.training.blog.logic.implementation;

import by.epam.training.blog.domain.Role;
import by.epam.training.blog.domain.application_entity.ApplicationPost;
import by.epam.training.blog.domain.db_entity.DbCategory;
import by.epam.training.blog.domain.db_entity.DbPost;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.logic.LogicWithServices;
import by.epam.training.blog.logic.api.MainLogic;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.List;

@Log4j2
public class DefaultMainLogic extends LogicWithServices implements MainLogic {
    private static final int NUMBER_OF_RECORDS_PER_PAGE = 6;

    @Override
    public List<ApplicationPost> showPosts(int currentPage) {
        List<ApplicationPost> appPosts = new ArrayList<>();
        List<DbPost> dbPosts;
        try {
            dbPosts = getPostService().showAllPost(currentPage, NUMBER_OF_RECORDS_PER_PAGE);
            for (DbPost dbPost : dbPosts) {
                appPosts.add(postConverter.convertDbEntityToApplicationEntity(dbPost));
            }
            return appPosts;
        } catch (DbException e) {
            log.error(e.getMessage());
        }
        return appPosts;
    }

    @Override
    public List<DbCategory> getCategory() {
        List<DbCategory> categories = null;
        try {
            categories = getCategoryService().getAllCategory();
        } catch (DbException e) {
            log.error(e.getMessage());
        }
        return categories;
    }

    @Override
    public int getNumberOfPages() {
        int rows = 0;
        int nOfPages = 0;
        try {
            rows = getPostService().getNumberOfRows();
            nOfPages = rows / NUMBER_OF_RECORDS_PER_PAGE;

            if (nOfPages % NUMBER_OF_RECORDS_PER_PAGE > 0) {
                nOfPages++;
            }
        } catch (DbException e) {
            log.error(e.getMessage());
        }
        return nOfPages;
    }

    @Override
    public Role getUserRole(String login) {
        return Role.USER;
    }
}

