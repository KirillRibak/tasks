package by.epam.training.blog.logic.implementation;

import by.epam.training.blog.domain.db_entity.DbComment;
import by.epam.training.blog.domain.db_entity.DbPost;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.logic.LogicWithServices;
import by.epam.training.blog.logic.api.PostLogic;
import lombok.extern.log4j.Log4j2;

import java.io.InputStream;
import java.util.List;

@Log4j2
public class DefaultPostLogic extends LogicWithServices implements PostLogic {
    private int counter = 0;
    private final int MAX_IMG_VALUE = 3;

    @Override
    public void deletePost(Integer postId) {
        try {
            getPostService().deletePost(postId);
        } catch (DbException e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void createPost(String title, Integer authorId, String text,
                           String[] categoryName, List<InputStream> imgStream) {
        DbPost newPost = new DbPost();
        int newPostId = 0;
        newPost.setTitle(title);
        newPost.setAuthor(authorId);
        newPost.setText(text);
        try {
            getPostService().save(newPost);
            newPostId = getNewPostId(authorId, text, title);
            for (InputStream img : imgStream) {
                if (counter < MAX_IMG_VALUE) {
                    addPicture(newPostId, img);
                }
            }
            for (int i = 0; i < categoryName.length; i++) {
                linkPostAndCatery(i, categoryName, newPostId);
            }
        } catch (DbException e) {
            log.error(e.getMessage());
        }

    }

    @Override
    public DbPost getPost(Integer postId) {
        try {
            return getPostService().findPost(postId);
        } catch (DbException e) {
            log.error(e.getMessage());
        }
        return null;
    }

    @Override
    public void updatePost(Integer postId, String title, String text) {
        try {
            DbPost post = getPostService().findPost(postId);
            post.setTitle(title);
            post.setText(text);
            getPostService().updatePost(post);
        } catch (DbException e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void addComment(Integer author, Integer postId, String commentText) {
        try {
            DbComment comment = new DbComment();
            comment.setAuthor(author);
            comment.setIdPost(postId);
            comment.setComment(commentText);
            getCommentService().createComment(comment);
        } catch (DbException e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void deleteComment(Integer commentId) {
        try {
            getCommentService().deleteComment(commentId);
        } catch (DbException e) {
            log.error(e.getMessage());
        }
    }

    private void linkPostAndCatery(int pointer, String[] categories, int newPostId) throws DbException {
        int categoryId = getCategoryService().returnCategoryIdByName(categories[pointer]);
        getPostService().linkPostWithcategory(newPostId, categoryId);
    }

    private void addPicture(int postId, InputStream inputStream) {
        try {
            getPostService().addImgToPost(postId, inputStream);
        } catch (DbException e) {
            log.error(e.getMessage());
        }
    }

    private Integer getNewPostId(Integer authorId, String text, String title) throws DbException {
        int postId = 0;
        List<DbPost> dbPosts = getPostService().findUserPosts(authorId);
        for (DbPost post : dbPosts) {
            if (post.getText().equals(text) && post.getTitle().equals(title)) {
                postId = post.getId();
            }
        }
        return postId;
    }


}
