package by.epam.training.blog.logic.implementation;

import by.epam.training.blog.domain.application_entity.ApplicationUser;
import by.epam.training.blog.domain.db_entity.User;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.logic.LogicWithServices;
import by.epam.training.blog.logic.api.ProfileLogic;
import by.epam.training.blog.logic.bcrypt.Cryptographer;
import lombok.extern.log4j.Log4j2;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class DefaultProfileLogic extends LogicWithServices implements ProfileLogic {
    private final String EMPTY_STRING = "";

    @Override
    public User collectUpdatedUser
            (String email, String password, String confirm, String aboutMe, ApplicationUser appUser) {
        User user = new User();
        if (password.equals(EMPTY_STRING) || password == null) {
            user.setPassword(appUser.getPassword());
        } else {
            if (password.equals(confirm)) {
                String encryptPassword = Cryptographer.encrypt(password);
                user.setPassword(encryptPassword);
            }
        }
        user.setId(appUser.getId());
        if (email.equals(EMPTY_STRING) || email == null) {
            user.setEmail(appUser.getEmail());
        } else {
            user.setEmail(email);
        }
        if (aboutMe.equals(EMPTY_STRING) || aboutMe == null) {
        } else {
            user.setAboutMe(aboutMe);
        }
        return user;
    }

    @Override
    public void updateUser(User user) {
        try {
            getUserService().updateUser(user);
        } catch (DbException e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public ApplicationUser updateAuthorizedUser(User user, ApplicationUser appUser) {
        ApplicationUser newAppUser = new ApplicationUser(appUser.getLogin(), user.getPassword(),
                user.getEmail(), appUser.getCreationStamp(), appUser.getChangeTime(),
                appUser.getImg(), appUser.getAppPosts(), appUser.getSubscribers(),
                appUser.getAppComments(), appUser.getRole(), user.getAboutMe());
        newAppUser.setId(user.getId());
        return newAppUser;
    }

    @Override
    public ApplicationUser uploadImg(Integer userId, InputStream inputStream) {
        ApplicationUser appUser = null;
        try {
            getUserService().addPicture(userId, inputStream);
            appUser = getUser(userId);
        } catch (DbException e) {
            log.error(e.getMessage());
        }
        return appUser;
    }

    @Override
    public List<ApplicationUser> getMySubscriptions(Integer userId) {
        List<ApplicationUser> mySubscriptions =new ArrayList<>();
        try {
            List<User> dbUsers = getUserService().showUserSubscriptions(userId);
            for (User user: dbUsers){
                ApplicationUser appUser = userConverter.convertDbEntityToApplicationEntity(user);
                mySubscriptions.add(appUser);
            }
        } catch (DbException e) {
            log.error(e.getMessage());
        }
        return mySubscriptions;
    }
}
