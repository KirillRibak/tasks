package by.epam.training.blog.logic.implementation;

import by.epam.training.blog.domain.db_entity.User;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.logic.LogicWithServices;
import by.epam.training.blog.logic.api.RegistrationLogic;
import by.epam.training.blog.logic.bcrypt.Cryptographer;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class DefaultRegistrationLogic extends LogicWithServices implements RegistrationLogic {

    @Override
    public boolean checkForLogin(String login){
        User dbUser = null;
        try {
            dbUser = getUserService().getByLogin(login);
        } catch (DbException e) {
            return true;
        }
        return false;
    }

    @Override
    public boolean checkPasswordValidation(String pass){
        if(pass.length()>=4){
            return true;
        }
        return false;
    }
    @Override
    public boolean comparePasswords(String password, String confimPassword){
        boolean isValid = password.equals(confimPassword);
        return isValid;
    }

    private String encodePassword(String password){
        String hash = Cryptographer.encrypt(password);
        return hash;
    }
    @Override
    public boolean registerUser(String login,String password,String email,String contextPath) {
        User dbUser = new User();
        String hash = encodePassword(password);
        dbUser.setPassword(hash);
        dbUser.setLogin(login);
        dbUser.setEmail(email);
        try {
            getUserService().save(dbUser);
            return true;
        } catch (DbException e) {
            return false;
        }
    }
}
