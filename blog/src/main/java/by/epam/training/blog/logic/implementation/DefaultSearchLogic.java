package by.epam.training.blog.logic.implementation;

import by.epam.training.blog.domain.application_entity.ApplicationPost;
import by.epam.training.blog.domain.db_entity.DbPost;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.logic.LogicWithServices;
import by.epam.training.blog.logic.api.SearchLogic;
import lombok.extern.log4j.Log4j2;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Log4j2
public class DefaultSearchLogic extends LogicWithServices implements SearchLogic {
    public Set<ApplicationPost> showFoundPosts(String request){
        try {
            List<DbPost> posts = getPostService().findPostsByContent(request);
            Set<ApplicationPost> appPost = new HashSet<>();
            for (DbPost post:posts){
                appPost.add(postConverter.convertDbEntityToApplicationEntity(post));
            }
            return appPost;
        } catch (DbException e) {
           log.error(e.getMessage());
        }
        return null;
    }
}
