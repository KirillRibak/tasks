package by.epam.training.blog.logic.implementation;

import by.epam.training.blog.domain.application_entity.ApplicationUser;
import by.epam.training.blog.domain.db_entity.User;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.logic.LogicWithServices;
import by.epam.training.blog.logic.api.ShowUserLogic;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class DefaultShowUserLogic extends LogicWithServices implements ShowUserLogic {

    @Override
    public ApplicationUser showUser(String login) {
        ApplicationUser appUser = null;
        try {
            User user = getUserService().getByLogin(login);
            appUser = userConverter.convertDbEntityToApplicationEntity(user);
        } catch (DbException e) {
            log.error(e.getMessage());
        }
        return appUser;
    }

    @Override
    public  void subscribeOnUser(Integer subscriberId, Integer userId){
        try {
            getUserService().subscribe(subscriberId,userId);
        } catch (DbException e) {
            log.error(e.getMessage());
        }
    }
    @Override
    public void unsubscribeOnUser(Integer subscriberId, Integer userId) {
        try {
            getUserService().unsubscribe(subscriberId,userId);
        } catch (DbException e) {
            log.error(e.getMessage());
        }
    }
    @Override
    public boolean isSigned(Integer sub,Integer user) {
        boolean result = false;
        try {
            result=getUserService().isSigned(sub,user);
        } catch (DbException e) {
            log.error(e.getMessage());
        }
        return result;
    }
}
