package by.epam.training.blog.logic.implementation;

import by.epam.training.blog.domain.application_entity.ApplicationUser;
import by.epam.training.blog.domain.db_entity.User;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.logic.LogicWithServices;
import by.epam.training.blog.logic.api.UsersLogic;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.List;
@Log4j2
public class DefaultUsersLogic extends LogicWithServices implements UsersLogic {
    @Override
    public List<ApplicationUser> getAllUsers(){
        List<User> users = null;
        List<ApplicationUser> appUsers = new ArrayList<>();
        try {
            users = getUserService().findAllUsers();
            for(User user: users){
                appUsers.add(getUser(user.getId()));
            }
        } catch (DbException e) {
            e.printStackTrace();
        }
        return appUsers;
    }

    @Override
    public void deleteUser(Integer userId){
        try {
            getUserService().deleteUser(userId);
        } catch (DbException e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void changeRole(Integer role, Integer userId){
        try {
            getUserService().changeRole(role,userId);
        } catch (DbException e) {
            log.error(e.getMessage());
        }
    }


}
