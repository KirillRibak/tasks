package by.epam.training.blog.logic.implementation;

import by.epam.training.blog.domain.db_entity.VisitEntry;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.logic.LogicWithServices;
import by.epam.training.blog.logic.api.VisitLogLogic;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class DefaultVisitLogLogic extends LogicWithServices implements VisitLogLogic {

    private String convertDate(Date date) {
        DateFormat df = new SimpleDateFormat("dd/MM/YYYY  hh:mm:ss");
        String time = df.format(date);
        return  time;
    }

    @Override
    public Integer registerEntry(String login, String sessionId, Date enterTime) throws DbException {
        String time = convertDate(enterTime);
        getVisitLogService().registerEntry(login,sessionId,time);
        return 0;
    }

    @Override
    public Integer registerOut(String login, String sessionId, Date outTime) throws DbException {
        String time = convertDate(outTime);
        getVisitLogService().registerOut(login,sessionId,time);
        return 0;
    }

    @Override
    public List<VisitEntry> getVisitLog() throws DbException {
        List<VisitEntry> entries = getVisitLogService().getVisitLog();
        return entries;
    }
}
