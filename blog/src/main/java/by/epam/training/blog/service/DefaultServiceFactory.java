package by.epam.training.blog.service;

import by.epam.training.blog.dao.DaoFactory;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.service.api.*;
import by.epam.training.blog.service.implementation.*;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum  DefaultServiceFactory implements ServiceFactory {
    INSTANCE;
    private static Map<Class<? extends Service>,Class<? extends ServiceWithConnection>> servises =
            new ConcurrentHashMap<>();
    static {
        servises.put(CategoryService.class, DefaultCategoryService.class);
        servises.put(CommentService.class, DefaultCommentService.class);
        servises.put(PostService.class, DefaultPostService.class);
        servises.put(UserService.class, DefaultUserService.class);
        servises.put(VisitLogService.class, DefaultVisitLogService.class);
    }

    private DaoFactory daoFactory ;
    public void setDaoFactory(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public <T extends Service> T getService(Class<T> key) throws DbException {

        Class<? extends ServiceWithConnection> value = servises.get(key);
        if(value != null) {
            try {
                ServiceWithConnection service = value.newInstance();
                service.SetDaoFactory(daoFactory);
                return (T) service;
            } catch(InstantiationException | IllegalAccessException e) {
                throw new DbException();
            }
        }
        return null;
    }
}
