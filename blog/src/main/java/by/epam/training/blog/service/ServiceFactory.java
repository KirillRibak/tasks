package by.epam.training.blog.service;

import by.epam.training.blog.exception.DbException;

public interface ServiceFactory {
    <T extends Service> T getService(Class<T> key) throws DbException;
}

