package by.epam.training.blog.service;

import by.epam.training.blog.dao.DaoFactory;

public abstract class ServiceWithConnection {
   protected DaoFactory daoFactory;

   public void SetDaoFactory(DaoFactory daoFactory) {
      this.daoFactory = daoFactory;
   }
}
