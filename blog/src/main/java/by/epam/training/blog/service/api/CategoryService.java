package by.epam.training.blog.service.api;

import by.epam.training.blog.domain.db_entity.DbCategory;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.service.Service;

import java.util.List;

public interface CategoryService extends Service {
    public List<DbCategory> showPostCategories (Integer postId) throws DbException;
    DbCategory addNewCategory(String name) throws DbException;
    String returnCategoryName(Integer categoryId)throws DbException;
    int returnCategoryIdByName(String name) throws DbException;
    List<Integer> findPostByCategory(Integer categoryId) throws DbException;
    List<Integer> findCategoryByPost(Integer postId) throws DbException;
    List<DbCategory> getAllCategory() throws DbException;
    boolean isExists(String name)throws DbException;
    boolean deleteCategory(Integer id) throws DbException;
}
