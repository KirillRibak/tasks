package by.epam.training.blog.service.api;

import by.epam.training.blog.domain.db_entity.DbComment;
import by.epam.training.blog.domain.db_entity.User;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.service.Service;

import java.util.List;

public interface CommentService extends Service {
    Integer createComment(DbComment comment)throws DbException;
    Integer deleteComment(Integer commentId)throws DbException;
    List<DbComment> getCommentForUser(Integer userId) throws DbException;
    List<DbComment> getCommentForPost(Integer postId) throws DbException;
    User showAuthor(Integer commentId) throws DbException;
    DbComment showComment(Integer commentId) throws DbException;
}
