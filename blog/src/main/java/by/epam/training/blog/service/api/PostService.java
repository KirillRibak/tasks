package by.epam.training.blog.service.api;

import by.epam.training.blog.domain.db_entity.DbPost;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.service.Service;

import java.io.InputStream;
import java.util.List;

public interface PostService extends Service {
    List<DbPost> showAllPost(int currentPage, int recordsPerPage)throws DbException;
    int getNumberOfRows() throws DbException;
    List<DbPost> findPostsByContent(String content) throws DbException;
    List<DbPost> findUserPosts(Integer dbUser)throws DbException;
    List<DbPost> findPostsByCategory(Integer categoryId) throws DbException;
    Integer addImgToPost(Integer postId, InputStream fis) throws DbException;
    List<String> showPostImg(Integer postId) throws DbException;
    Integer deletePost(Integer postId) throws DbException;
    Integer updatePost(DbPost dbPost) throws DbException;
    DbPost findPost(Integer id) throws DbException;
    Integer save(DbPost dbPost) throws DbException;
    void linkPostWithcategory(Integer postId,Integer categoryId) throws DbException;
}
