package by.epam.training.blog.service.api;

import by.epam.training.blog.domain.db_entity.User;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.service.Service;

import java.io.InputStream;
import java.util.List;

public interface UserService extends Service {
    List<User> findAllUsers()throws DbException;
    List<User> showUserSubscribers(Integer userId)throws DbException;
    List<User> showUserSubscriptions(Integer userId)throws DbException;
    void addPicture(Integer userId, InputStream fis) throws DbException;
    User getByLogin(String login) throws DbException;
    Integer deleteUser(Integer id) throws DbException;
    Integer updateUser(User dbUser) throws DbException;
    User findUser(Integer id) throws DbException;
    Integer save(User dbUser) throws DbException;
    void subscribe(Integer sub,Integer user) throws DbException;
    void unsubscribe(Integer sub,Integer user) throws DbException;
    boolean isSigned(Integer sub,Integer user)throws DbException;
    void changeRole(Integer role, Integer userId) throws DbException;
}
