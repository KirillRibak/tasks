package by.epam.training.blog.service.api;

import by.epam.training.blog.domain.db_entity.VisitEntry;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.service.Service;

import java.util.List;

public interface VisitLogService extends Service {
    Integer registerEntry(String login,String sessionId, String enterTime) throws DbException;
    Integer registerOut(String login,String sessionId, String outTime)throws DbException;
    List<VisitEntry> getVisitLog()throws DbException;

}
