package by.epam.training.blog.service.implementation;

import by.epam.training.blog.dao.api.CategoryDao;
import by.epam.training.blog.dao.api.PostWithCategoryDao;
import by.epam.training.blog.domain.db_entity.DbCategory;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.service.ServiceWithConnection;
import by.epam.training.blog.service.api.CategoryService;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.List;

@Log4j2
public class DefaultCategoryService extends ServiceWithConnection implements CategoryService {


    @Override
    public List<DbCategory> showPostCategories(Integer postId) throws DbException {
        PostWithCategoryDao postWithCategoryDao = daoFactory.createDao(PostWithCategoryDao.class);
        List<Integer> ids = postWithCategoryDao.findCategoryByPost(postId);
        List<DbCategory> categories = new ArrayList<>();
        for (Integer id : ids) {
            categories.add(collectCategory(id));
        }
        return categories;
    }

    @Override
    public DbCategory addNewCategory(String name) throws DbException {
        CategoryDao categoryDao = daoFactory.createDao(CategoryDao.class);
            int categoryId = categoryDao.addNewCategory(name);
            return collectCategory(categoryId);
    }

    @Override
    public String returnCategoryName(Integer categoryId) throws DbException {
        CategoryDao categoryDao = daoFactory.createDao(CategoryDao.class);
        String name = categoryDao.returnCategoryName(categoryId);
        return name;
    }

    @Override
    public int returnCategoryIdByName(String name) throws DbException {
        CategoryDao categoryDao = daoFactory.createDao(CategoryDao.class);
        Integer categoryId = categoryDao.returnCategoryIdByName(name);
        return categoryId;
    }

    @Override
    public List<Integer> findPostByCategory(Integer categoryId) throws DbException {
        PostWithCategoryDao postWithCategoryDao = daoFactory.createDao(PostWithCategoryDao.class);
        List<Integer> postIdCollection = postWithCategoryDao.findPostByCategory(categoryId);
        return postIdCollection;
    }

    @Override
    public List<Integer> findCategoryByPost(Integer postId) throws DbException {
        PostWithCategoryDao postWithCategoryDao = daoFactory.createDao(PostWithCategoryDao.class);
        List<Integer> categoryIdCollection = postWithCategoryDao.findCategoryByPost(postId);
        return categoryIdCollection;
    }

    @Override
    public List<DbCategory> getAllCategory() throws DbException {
        CategoryDao categoryDao = daoFactory.createDao(CategoryDao.class);
        List<DbCategory> categories = categoryDao.getAllCategories();
        return categories;
    }

    @Override
    public boolean isExists(String name) throws DbException {
        CategoryDao categoryDao = daoFactory.createDao(CategoryDao.class);
        boolean result = categoryDao.isExists(name);
        return result;
    }

    @Override
    public boolean deleteCategory(Integer id) throws DbException {
        CategoryDao categoryDao = daoFactory.createDao(CategoryDao.class);
        String name = returnCategoryName(id);
        categoryDao.deleteCategory(id);
        if (isExists(name)){
            return false;
        }else {
            return true;
        }
    }

    private DbCategory collectCategory(int id) throws DbException {

        PostWithCategoryDao postWithCategoryDao = daoFactory.createDao(PostWithCategoryDao.class);
        CategoryDao categoryDao = daoFactory.createDao(CategoryDao.class);
        DbCategory dbCategory = new DbCategory();
        try {
            dbCategory.setName(categoryDao.returnCategoryName(id));
            List<Integer> posts = postWithCategoryDao.findPostByCategory(id);
            for (Integer postId : posts) {
                dbCategory.addPost(postId);
            }
        } catch (DbException e) {
            log.error("Service level error: method collectCategory " + "\n" + e.getMessage());
        }
        return dbCategory;
    }

    private DbCategory collectCategory(String name, int id) throws DbException {
        PostWithCategoryDao postWithCategoryDao = daoFactory.createDao(PostWithCategoryDao.class);
        CategoryDao categoryDao = daoFactory.createDao(CategoryDao.class);
        DbCategory dbCategory = new DbCategory();
        dbCategory.setName(name);
        List<Integer> posts = null;
        try {
            posts = postWithCategoryDao.findPostByCategory(id);
            for (Integer postId : posts) {
                dbCategory.addPost(postId);
            }
        } catch (DbException e) {
            log.error("Service level error: method collectCategory " + "\n" + e.getMessage());
        }

        return dbCategory;
    }
}
