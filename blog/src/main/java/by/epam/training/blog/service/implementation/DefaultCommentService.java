package by.epam.training.blog.service.implementation;

import by.epam.training.blog.dao.api.CommentDao;
import by.epam.training.blog.dao.api.UserDao;
import by.epam.training.blog.domain.db_entity.DbComment;
import by.epam.training.blog.domain.db_entity.User;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.service.ServiceWithConnection;
import by.epam.training.blog.service.api.CommentService;
import lombok.extern.log4j.Log4j2;

import java.util.List;

@Log4j2
public class DefaultCommentService extends ServiceWithConnection implements CommentService {


    @Override
    public Integer createComment(DbComment comment) throws DbException {
        CommentDao commentDao = daoFactory.createDao(CommentDao.class);
        return commentDao.create(comment);
    }

    @Override
    public Integer deleteComment(Integer commentId) throws DbException {
        CommentDao commentDao = daoFactory.createDao(CommentDao.class);
        return commentDao.delete(commentId);
    }

    @Override
    public List<DbComment> getCommentForUser(Integer userId) throws DbException {
        CommentDao commentDao = daoFactory.createDao(CommentDao.class);
        List<DbComment> dbComments = commentDao.getCommentForUser(userId);
        return dbComments;
    }

    @Override
    public List<DbComment> getCommentForPost(Integer postId) throws DbException {
        CommentDao commentDao = daoFactory.createDao(CommentDao.class);
        List<DbComment> dbComments = commentDao.getCommentForPost(postId);
        return dbComments;
    }

    @Override
    public User showAuthor(Integer commentId) throws DbException {
        UserDao userDao = daoFactory.createDao(UserDao.class);
        CommentDao commentDao = daoFactory.createDao(CommentDao.class);
        User dbUser = userDao.read(commentDao.showAuthor(commentId));
        return dbUser;
    }

    @Override
    public DbComment showComment(Integer commentId) throws DbException {
        CommentDao commentDao = daoFactory.createDao(CommentDao.class);
        DbComment comment = commentDao.read(commentId);
        return comment;
    }
}
