package by.epam.training.blog.service.implementation;

import by.epam.training.blog.dao.api.ImageDao;
import by.epam.training.blog.dao.api.PostDao;
import by.epam.training.blog.dao.api.PostWithCategoryDao;
import by.epam.training.blog.domain.db_entity.DbPost;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.service.ServiceWithConnection;
import by.epam.training.blog.service.api.PostService;
import lombok.extern.log4j.Log4j2;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class DefaultPostService extends ServiceWithConnection implements PostService {

    @Override
    public List<DbPost> showAllPost(int currentPage, int recordsPerPage) throws DbException {
        int start = currentPage * recordsPerPage - recordsPerPage;
        PostDao postDao = daoFactory.createDao(PostDao.class);
        List<DbPost> posts = postDao.showAllPost(start,recordsPerPage);
        return posts;
    }

    @Override
    public int getNumberOfRows() throws DbException {
        PostDao postDao = daoFactory.createDao(PostDao.class);
        return postDao.getNumberOfRows();
    }

    @Override
    public List<DbPost> findPostsByContent(String content) throws DbException {
        PostDao postDao = daoFactory.createDao(PostDao.class);
        List<DbPost> postsMatchByText = postDao.findPostByText(content);
        List<DbPost> postsMatchByTitle = postDao.findPostByTitle(content);
        postsMatchByText.addAll(postsMatchByTitle);
        return postsMatchByText;
    }

    @Override
    public List<DbPost> findUserPosts(Integer dbUser) throws DbException {
        PostDao postDao = daoFactory.createDao(PostDao.class);
        List<DbPost> dbPosts = postDao.findUserPosts(dbUser);
        return dbPosts;
    }

    @Override
    public List<DbPost> findPostsByCategory(Integer categoryId) throws DbException {
        PostDao postDao = daoFactory.createDao(PostDao.class);
        PostWithCategoryDao  postWithCategoryDao = daoFactory.createDao(PostWithCategoryDao.class);
        List<Integer> ids = postWithCategoryDao.findPostByCategory(categoryId);
        List<DbPost> dbPosts = new ArrayList<>();
        for (Integer id : ids) {
            dbPosts.add(postDao.read(id));
        }
        return dbPosts;
    }

    @Override
    public Integer addImgToPost(Integer postId, InputStream fis) throws DbException {
        ImageDao imgDao = daoFactory.createDao(ImageDao.class);
        return imgDao.addImgToPost(postId,fis);
    }

    @Override
    public List<String> showPostImg(Integer postId) throws DbException {
        ImageDao imgDao = daoFactory.createDao(ImageDao.class);
        return imgDao.showPostImg(postId);
    }

    @Override
    public Integer deletePost(Integer id) throws DbException {
        PostDao postDao = daoFactory.createDao(PostDao.class);
        return postDao.delete(id);
    }

    @Override
    public Integer updatePost(DbPost dbPost) throws DbException {
        PostDao postDao = daoFactory.createDao(PostDao.class);
        return postDao.update(dbPost);
    }

    @Override
    public DbPost findPost(Integer id) throws DbException {
        PostDao postDao = daoFactory.createDao(PostDao.class);
        DbPost post = postDao.read(id);
        return post;
    }

    @Override
    public Integer save(DbPost dbPost) throws DbException {
        PostDao postDao = daoFactory.createDao(PostDao.class);
        Integer id = postDao.create(dbPost);
        return id;
    }

    @Override
    public void linkPostWithcategory(Integer postId, Integer categoryId) throws DbException {
        PostWithCategoryDao postWithCategoryDao = daoFactory.createDao(PostWithCategoryDao.class);
        postWithCategoryDao.linkCategoryAndPost(postId,categoryId);
    }
}
