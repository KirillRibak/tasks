package by.epam.training.blog.service.implementation;

import by.epam.training.blog.dao.api.UserDao;
import by.epam.training.blog.dao.api.UserSubscriberDao;
import by.epam.training.blog.domain.db_entity.User;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.service.ServiceWithConnection;
import by.epam.training.blog.service.api.UserService;
import lombok.extern.log4j.Log4j2;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class DefaultUserService extends ServiceWithConnection implements UserService {

    @Override
    public List<User> findAllUsers() throws DbException {
        UserDao  userDao = daoFactory.createDao(UserDao.class);
        List<User> dbUsers = userDao.getAllUsers();
        return dbUsers;
    }

    @Override
    public List<User> showUserSubscribers(Integer userId) throws DbException {
        List<User> dbUsers = new ArrayList<>();
        UserSubscriberDao userSubDao = daoFactory.createDao(UserSubscriberDao.class);
        UserDao  userDao = daoFactory.createDao(UserDao.class);
        List<Integer> ids = userSubDao.findUserSubscriber(userId);
        for (Integer id : ids) {
            dbUsers.add(userDao.read(id));
        }
        return dbUsers;
    }

    @Override
    public List<User> showUserSubscriptions(Integer userId) throws DbException {
        List<User> dbUsers = new ArrayList<>();
        UserSubscriberDao userSubDao = daoFactory.createDao(UserSubscriberDao.class);
        UserDao  userDao = daoFactory.createDao(UserDao.class);
        List<Integer> ids = userSubDao.showUserSubscriptions(userId);
        for (Integer id : ids) {
            dbUsers.add(userDao.read(id));
        }
        return dbUsers;
    }

    @Override
    public void addPicture(Integer userId, InputStream fis) throws DbException {
        UserDao  userDao = daoFactory.createDao(UserDao.class);
        userDao.addPicture(userId,fis);
    }

    @Override
    public User getByLogin(String login) throws DbException {
        UserDao  userDao = daoFactory.createDao(UserDao.class);
        User dbUser = userDao.getByLogin(login);
        return dbUser;
    }

    @Override
    public Integer deleteUser(Integer id) throws DbException {
        UserDao  userDao = daoFactory.createDao(UserDao.class);
        return userDao.delete(id);
    }


    @Override
    public Integer updateUser(User dbUser) throws DbException {
        UserDao  userDao = daoFactory.createDao(UserDao.class);
        return userDao.update(dbUser);
    }

    @Override
    public User findUser(Integer id) throws DbException {
        UserDao  userDao = daoFactory.createDao(UserDao.class);
        User dbUser = null;
        dbUser = userDao.read(id);
        return dbUser;
    }

    @Override
    public Integer save(User dbUser) throws DbException {
        UserDao  userDao = daoFactory.createDao(UserDao.class);
        return userDao.create(dbUser);
    }

    @Override
    public void subscribe(Integer sub, Integer user) throws DbException {
        UserSubscriberDao userSubscriberDao = daoFactory.createDao(UserSubscriberDao.class);
        userSubscriberDao.subscribeOnUser(sub,user);
    }

    @Override
    public void unsubscribe(Integer sub, Integer user) throws DbException {
        UserSubscriberDao userSubscriberDao = daoFactory.createDao(UserSubscriberDao.class);
        userSubscriberDao.unsubscribeOnUser(sub,user);
    }

    @Override
    public boolean isSigned(Integer sub, Integer user) throws DbException {
        UserSubscriberDao userSubscriberDao = daoFactory.createDao(UserSubscriberDao.class);
        return userSubscriberDao.isSigned(sub,user);
    }

    @Override
    public void changeRole(Integer role, Integer userId) throws DbException {
        UserDao userDao = daoFactory.createDao(UserDao.class);
        userDao.changeRole(role,userId);
    }
}
