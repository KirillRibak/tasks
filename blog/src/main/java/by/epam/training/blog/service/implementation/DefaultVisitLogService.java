package by.epam.training.blog.service.implementation;

import by.epam.training.blog.dao.DefaultDaoFactory;
import by.epam.training.blog.dao.api.LogDao;
import by.epam.training.blog.domain.db_entity.VisitEntry;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.service.ServiceWithConnection;
import by.epam.training.blog.service.api.VisitLogService;

import java.util.List;

public class DefaultVisitLogService extends ServiceWithConnection implements VisitLogService {

    @Override
    public Integer registerEntry(String login, String sessionId, String enterTime) throws DbException {
        LogDao logDao = DefaultDaoFactory.INSTANCE.createDao(LogDao.class);
        int row = logDao.registerEntry(login,sessionId,enterTime);
        return row;
    }

    @Override
    public Integer registerOut(String login, String sessionId, String outTime) throws DbException {
        LogDao logDao = DefaultDaoFactory.INSTANCE.createDao(LogDao.class);
        int row = logDao.registerOut(login,sessionId,outTime);
        return row;
    }

    @Override
    public List<VisitEntry> getVisitLog() throws DbException {
        LogDao logDao = DefaultDaoFactory.INSTANCE.createDao(LogDao.class);
        List<VisitEntry> visitLog = logDao.getVisitLog();
        return visitLog;
    }
}
