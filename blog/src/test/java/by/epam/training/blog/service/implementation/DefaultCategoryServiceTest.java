package by.epam.training.blog.service.implementation;

import by.epam.training.blog.dao.DefaultDaoFactory;
import by.epam.training.blog.dao.pool.ConnectionPool;
import by.epam.training.blog.domain.db_entity.DbCategory;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.service.DefaultServiceFactory;
import by.epam.training.blog.service.api.CategoryService;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.util.List;
import java.util.ResourceBundle;

public class DefaultCategoryServiceTest {

    private CategoryService categoryService;
    private DbCategory category;

    public void init(){
    }

    @BeforeSuite
    public void profileSetup() throws DbException {
        ConnectionPool.INSTANCE.init(ResourceBundle.getBundle("datasource"));
        DefaultServiceFactory.INSTANCE.setDaoFactory(DefaultDaoFactory.INSTANCE);
         categoryService = DefaultServiceFactory.INSTANCE.getService(CategoryService.class);
         category = new DbCategory();
         category.setName("игры");

    }

    @Test(priority = 1)
    public void showPostCategories() throws DbException {
        List<DbCategory> categoryFromDb = categoryService.showPostCategories(1);
        DbCategory dbCategory = categoryFromDb.get(0);
        Assert.assertEquals(dbCategory.getName(),category.getName());
    }

    @Test(priority = 2)
    public void returnCategoryName()throws DbException  {
        String name = categoryService.returnCategoryName(40);
        String expectedName = "игры";
        Assert.assertEquals(name,expectedName);
    }
    @Test(priority = 3)
    public void returnCategoryIdByName() throws DbException {
        int expectedId = 40;
        int id = categoryService.returnCategoryIdByName("игры");
        Assert.assertTrue(expectedId == id);
    }

    @Test(priority = 4)
    public void addNewCategory()throws DbException  {
        categoryService.addNewCategory("новая");
        int id = categoryService.returnCategoryIdByName("новая");
        Assert.assertTrue(id>0);
    }

    @Test(priority = 5)
    public void findPostByCategory()throws DbException  {
        List<Integer> foundPosts = categoryService.findPostByCategory(40);
        Assert.assertTrue(!foundPosts.isEmpty());

    }

    @Test(priority = 6)
    public void findCategoryByPost()throws DbException  {
        List<Integer> dbCategories = categoryService.findCategoryByPost(1);
        Assert.assertTrue(!dbCategories.isEmpty());
    }

    @Test(priority = 7)
    public void getAllCategory()throws DbException  {
        List<DbCategory> dbCategories = categoryService.getAllCategory();
        Assert.assertTrue(!dbCategories.isEmpty());
    }

    @Test(priority = 8)
    public void isExists() throws DbException {
        boolean flag = categoryService.isExists("игры");
        Assert.assertTrue(flag);
    }

    @AfterClass
    public void deleteCategory()throws DbException  {
        int id = categoryService.returnCategoryIdByName("новая");
        categoryService.deleteCategory(id);
        Assert.assertFalse(categoryService.isExists("новая"));
    }

}
