package by.epam.training.blog.service.implementation;

import by.epam.training.blog.dao.DefaultDaoFactory;
import by.epam.training.blog.dao.pool.ConnectionPool;
import by.epam.training.blog.domain.db_entity.DbComment;
import by.epam.training.blog.domain.db_entity.User;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.service.DefaultServiceFactory;
import by.epam.training.blog.service.api.CommentService;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.ResourceBundle;

public class DefaultCommentServiceTest {
    CommentService commentService;

    DbComment comment ;

    @BeforeMethod
    public void setUp() throws DbException {
        ConnectionPool.INSTANCE.init(ResourceBundle.getBundle("datasource"));
        DefaultServiceFactory.INSTANCE.setDaoFactory(DefaultDaoFactory.INSTANCE);
        commentService = DefaultServiceFactory.INSTANCE.getService(CommentService.class);

        comment = new DbComment();

        comment.setComment("comment");
        comment.setIdPost(1);
        comment.setAuthor(1);
    }

    @Test(priority = 1)
    public void testGetCommentForUser() throws DbException {
        List<DbComment> userComments = commentService.getCommentForUser(1);
        Assert.assertFalse(userComments.isEmpty());
    }

    @Test(priority = 2)
    public void testGetCommentForPost()throws DbException {
        List<DbComment> postComments = commentService.getCommentForPost(1);
        Assert.assertFalse(postComments.isEmpty());
    }

    @Test(priority = 3)
    public void testShowAuthor()throws DbException {
        User userFromDb = commentService.showAuthor(14);
        int expecteduserId = 15;
        Assert.assertTrue(userFromDb.getId()==expecteduserId);
    }

    @Test(priority = 4)
    public void testCreateComment()throws DbException {
        int row  = commentService.createComment(comment);
        int expectedValue = 1;
        Assert.assertEquals(row,expectedValue);
    }

    @AfterClass
    public void testDeleteComment()throws DbException {
        List<DbComment> userComments = commentService.getCommentForUser(1);
        DbComment userComment = userComments.get(0);
        int row= commentService.deleteComment(userComment.getId());
        Assert.assertEquals(row,1);
    }


}