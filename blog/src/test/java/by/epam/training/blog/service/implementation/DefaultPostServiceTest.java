package by.epam.training.blog.service.implementation;

import by.epam.training.blog.dao.DefaultDaoFactory;
import by.epam.training.blog.dao.pool.ConnectionPool;
import by.epam.training.blog.domain.db_entity.DbPost;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.service.DefaultServiceFactory;
import by.epam.training.blog.service.api.PostService;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.ResourceBundle;

public class DefaultPostServiceTest {

    private DbPost post;
    private DbPost newPost;
    private DbPost updatablePost;
    private PostService postService;
    private String content = "Новости от CD Project Red";

    @BeforeMethod
    public void setUp() throws DbException {
        ConnectionPool.INSTANCE.init(ResourceBundle.getBundle("datasource"));
        DefaultServiceFactory.INSTANCE.setDaoFactory(DefaultDaoFactory.INSTANCE);
        postService = DefaultServiceFactory.INSTANCE.getService(PostService.class);

        post = new DbPost();
        newPost = new DbPost();
        updatablePost = new DbPost();

        post.setId(1);
        post.setTitle(content);
        post.setAuthor(1);
        post.setText("Польский портал Bankier опубликовал ряд высказываний руководителя CD Projekt Адама Кичиньского с некой конференции, где разработчик рассказал массу интересного касательно будущего студии.\n" +
                "Во-первых, как сообщает СМИ, Кичиньский подтвердил, что CD Projekt RED работает сразу над тремя играми по вселенной Cyberpunk 2077, включая ?основную? и ещё две неанонсированные. Всего в компании при этом трудится пять команд.\n" +
                "Одна из команд ?Киберпанка? находится во Вроцлаве и насчитывает приблизительно четыре десятка специалистов - они заняты неким многопользовательским проектом.\n" +
                "Более того, следующим крупным релизом CD Projekt (видимо, той самой крупнобюджетной ролевой игрой, что должна выйти до конца 2021 года) тоже станет проект по мотивам Cyberpunk 2077. По словам студии, он инновационный и рискованный.\n" +
                "Ещё Кичиньский сообщил, что студия ?очень довольна? предзаказами Cyberpunk 2077, особенно на китайском рынке, а доля продаж на РС, скорее всего, будет ниже, чем у третьего ?Ведьмака?. На это есть сразу несколько причин:\n" +
                "?Ведьмак? долгое время был компьютерным эксклюзивом и ассоциировался в первую очередь с этой платформой;\n" +
                "С 2015 года аудитория консолей текущего поколения значительно выросла.\n" +
                "Отметим, что в первый год количество проданных копий ?Дикой Охоты? на РС составило 31% от общего тиража, тогда как большая часть пришлась на версию для PS4 - 48%.\n" +
                "Cyberpunk 2077 готовится к релизу 16 апреля на PS4, Xbox One и РС.");

        newPost.setTitle("Title");
        newPost.setText("text");
        newPost.setAuthor(1);

        updatablePost.setTitle("new title");
        updatablePost.setText("new text");
        updatablePost.setAuthor(1);
    }

    @Test(priority = 1)
    public void testShowAllPost() throws DbException {

        List<DbPost> posts = postService.showAllPost(1,2);
        Assert.assertTrue(!posts.isEmpty());
    }

    @Test(priority = 2)
    public void testFindPost() throws DbException{
        DbPost postFromDb = postService.findPost(1);
        Assert.assertTrue(post.equals(postFromDb));
    }

    @Test(priority = 3)
    public void testFindPostsByContent() throws DbException{
        List<DbPost> posts = postService.findPostsByContent(content);
        DbPost foundpost = posts.get(0);
        Assert.assertTrue(post.equals(foundpost));
    }

    @Test(priority = 4)
    public void testSave() throws DbException{
        int row = postService.save(newPost);
        Assert.assertEquals(1,row);
    }

    @Test(priority = 10)
    public void testGetColumValue() throws DbException {
        int rowValue = postService.getNumberOfRows();
        Assert.assertTrue(rowValue>0);
    }

    @Test(priority = 5)
    public void testUpdatePost() throws DbException{
        List<DbPost> foundPosts = postService.findPostsByContent("Title");
        DbPost postFromDb = foundPosts.get(0);
        updatablePost.setId(postFromDb.getId());
       int row = postService.updatePost(updatablePost);
        Assert.assertEquals(1,row);

    }

    @Test(priority = 6)
    public void testFindUserPosts()throws DbException {
        List<DbPost> posts = postService.findUserPosts(1);
        Assert.assertFalse(posts.isEmpty());
    }

    @Test(priority = 7)
    public void testFindPostsByCategory()throws DbException {
        List<DbPost> posts = postService.findPostsByCategory(40);
        DbPost postFromDb = posts.get(0);
        DbPost expectedPost = postService.findPost(1);
        Assert.assertTrue(expectedPost.equals(postFromDb));
    }

    @Test(priority = 8)
    public void testShowPostImg() throws DbException{
        List<String> postImgs = postService.showPostImg(1);
        Assert.assertNotNull(postImgs);
    }

    @Test(priority = 9)
    public void testAddImgToPost() throws DbException, FileNotFoundException {
        List<DbPost> foundPosts = postService.findPostsByContent("new title");
        DbPost postFromDb = foundPosts.get(0);
        postService.addImgToPost(postFromDb.getId(),new FileInputStream("./web/img/empty.jpg"));
        List<String> postImgs = postService.showPostImg(postFromDb.getId());
        Assert.assertNotNull(postImgs);
    }

    @Test(priority = 10)
    public void testLinkPostWithCategory() throws DbException{
        List<DbPost> foundPosts = postService.findPostsByContent("new title");
        DbPost postFromDb = foundPosts.get(0);
        int postId = postFromDb.getId();
        postService.linkPostWithcategory(postId,28);
        List<DbPost> posts = postService.findPostsByCategory(28);
        DbPost postFromDbWithCategory = posts.get(0);
        Assert.assertTrue(postFromDb.equals(postFromDbWithCategory));
    }

    @AfterClass
    public void testDeletePost() throws DbException{
        List<DbPost> foundPosts = postService.findPostsByContent("new title");
        DbPost postFromDb = foundPosts.get(0);
        int row = postService.deletePost(postFromDb.getId());
        Assert.assertEquals(1,row);
    }
}