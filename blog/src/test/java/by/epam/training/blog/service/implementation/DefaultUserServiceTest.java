package by.epam.training.blog.service.implementation;

import by.epam.training.blog.dao.DefaultDaoFactory;
import by.epam.training.blog.dao.pool.ConnectionPool;
import by.epam.training.blog.domain.db_entity.User;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.service.DefaultServiceFactory;
import by.epam.training.blog.service.api.UserService;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.ResourceBundle;

public class DefaultUserServiceTest {
    private User user;
    private User newUser;
    private User newUserWithUpdate;
    UserService userService;
    String emptyString = "";

    @BeforeSuite
    public void setUp() throws DbException {
        ConnectionPool.INSTANCE.init(ResourceBundle.getBundle("datasource"));
        DefaultServiceFactory.INSTANCE.setDaoFactory(DefaultDaoFactory.INSTANCE);
        userService = DefaultServiceFactory.INSTANCE.getService(UserService.class);

        user = new User();
        newUser = new User();
        newUserWithUpdate = new User();

        user.setId(1);
        user.setLogin("KIRILL");
        user.setRole(2);
        user.setPassword("$2a$12$kSHengNcBnwZBjFrnNMpp.U6OJDcSmEEjZ7b9vQO.uzyQbElh.bdu");
        user.setEmail("krybak99@gmail.com");


        newUser.setLogin("NewUser");
        newUser.setPassword("pass");
        newUser.setEmail("email@email.com");

        newUserWithUpdate.setLogin("NewUser");
        newUserWithUpdate.setPassword("pass1");
        newUserWithUpdate.setEmail("mail@mail.com");
    }

    @Test(priority = 1)
    public void testFindUser() throws DbException {
        User userFromDb = userService.findUser(1);
        Assert.assertTrue(userFromDb.equals(user));
    }

    @Test(priority = 2)
    public void testFindAllUsers() throws DbException {
        List<User> users = userService.findAllUsers();
        Assert.assertTrue(!users.isEmpty());
    }

    @Test(priority = 3)
    public void testShowUserSubscribers() throws DbException {
        List<User> users = userService.showUserSubscribers(1);
        Assert.assertTrue(!users.isEmpty());
    }

    @Test(priority = 4)
    public void testShowUserSubscriptions() throws DbException {
        List<User> users = userService.showUserSubscriptions(1);
        Assert.assertFalse(users.isEmpty());
    }

    @Test(priority = 5)
    public void testGetByLogin() throws DbException {
        User userFromDb = userService.getByLogin("KIRILL");
        Assert.assertTrue(userFromDb.equals(user));
    }

    @Test(priority = 6, dependsOnMethods = {"testGetByLogin"})
    public void testSave() throws DbException {
        int row = userService.save(newUser);
        Assert.assertEquals(1,row);
    }

    @Test(priority = 7,dependsOnMethods = {"testSave"})
    public void testUpdateUser() throws DbException {
        User userFromDb = userService.getByLogin("NewUser");
        newUserWithUpdate.setId(userFromDb.getId());
        int row = userService.updateUser(newUserWithUpdate);
        Assert.assertEquals(row ,1);
    }

    @Test( dependsOnMethods = {"testSave"})
    public void testAddPicture() throws DbException, FileNotFoundException {
        User userFromDb = userService.getByLogin("NewUser");
        FileInputStream image = new FileInputStream("./web/img/empty.jpg");
        userService.addPicture(userFromDb.getId(),image);
        User userWithImage = userService.getByLogin("NewUser");
        String userImg = userWithImage.getImg();
        Assert.assertTrue(!emptyString.equals(userImg)&& userImg!=null);
    }


    @Test( dependsOnMethods = {"testSave"})
    public void testChangeRole() throws DbException {
        User userFromDb = userService.getByLogin("NewUser");
        userService.changeRole(0,userFromDb.getId());
        User userFromDbWithNewRole = userService.getByLogin("NewUser");
        Assert.assertNotEquals(userFromDb.getRole(),userFromDbWithNewRole.getRole());
    }

    @Test( dependsOnMethods = {"testSave"})
    public void testSubscribe() throws DbException {
        User userFromDb = userService.getByLogin("NewUser");
        userService.subscribe(userFromDb.getId(),1);
        List<User> mySubscriptionsAfter = userService.showUserSubscriptions(userFromDb.getId());
        int afterSubscribe = mySubscriptionsAfter.size();
        Assert.assertNotEquals(0,afterSubscribe);
    }

    @Test(dependsOnMethods = {"testSubscribe"})
    public void testIsSignedShouldReturnTrue() throws DbException {
        User userFromDb = userService.getByLogin("NewUser");
        boolean isSigned = userService.isSigned(userFromDb.getId(),1);
        Assert.assertTrue(isSigned);
    }

    @Test(dependsOnMethods = {"testSubscribe"})
    public void testIsSignedShouldReturnFalse() throws DbException {
        User userFromDb = userService.getByLogin("NewUser");
        boolean isSigned = userService.isSigned(15,userFromDb.getId());
        Assert.assertFalse(isSigned);
    }

    @Test(dependsOnMethods = {"testIsSignedShouldReturnFalse"})
    public void testUnsubscribe() throws DbException  {
        User userFromDb = userService.getByLogin("NewUser");
        List<User> mySubscriptions = userService.showUserSubscriptions(userFromDb.getId());
        int beforeSubscribe = mySubscriptions.size();
        userService.unsubscribe(userFromDb.getId(),1);
        List<User> mySubscriptionsAfter = userService.showUserSubscriptions(userFromDb.getId());
        int afterSubscribe = mySubscriptionsAfter.size();
        Assert.assertNotEquals(beforeSubscribe,afterSubscribe);
    }

    @AfterClass
    public void testDeleteUser() throws DbException {
        User userFromDb = userService.getByLogin("NewUser");
        int row = userService.deleteUser(userFromDb.getId());
        Assert.assertEquals(row,1);
    }

}