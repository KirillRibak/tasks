package by.epam.training.blog.service.implementation;

import by.epam.training.blog.dao.DefaultDaoFactory;
import by.epam.training.blog.dao.pool.ConnectionPool;
import by.epam.training.blog.domain.db_entity.VisitEntry;
import by.epam.training.blog.exception.DbException;
import by.epam.training.blog.service.DefaultServiceFactory;
import by.epam.training.blog.service.api.VisitLogService;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.sql.Date;
import java.util.List;
import java.util.ResourceBundle;

public class DefaultVisitLogServiceTest {
    VisitLogService service;
    private VisitEntry visitEntry;
    private int id;
    private String login;
    private String in;
    private String out;

    @BeforeMethod
    public void setUp() throws DbException {
        ConnectionPool.INSTANCE.init(ResourceBundle.getBundle("datasource"));
        DefaultServiceFactory.INSTANCE.setDaoFactory(DefaultDaoFactory.INSTANCE);
        service = DefaultServiceFactory.INSTANCE.getService(VisitLogService.class);
        id = 1;
        login = "test";
        in = new Date(System.currentTimeMillis()).toString();
        out = new Date(System.currentTimeMillis()).toString();
        visitEntry = new VisitEntry();
        visitEntry.setId(id);
        visitEntry.setLogin(login);
        visitEntry.setEnter(in);
        visitEntry.setOut(out);
    }

    @Test(priority = 1)
    public void testGetVisitLog() throws DbException {
        int row = service.registerEntry(login, login, in);
        Assert.assertEquals(1, row);
    }

    @Test(priority = 2)
    public void testRegisterEntry() throws DbException {
        int row = service.registerOut(login, login, out);
        Assert.assertEquals(1, row);
    }

    @Test(priority = 3)
    public void testRegisterOut() throws DbException {
        List<VisitEntry> entries = service.getVisitLog();
        Assert.assertFalse(entries.isEmpty());
    }


}