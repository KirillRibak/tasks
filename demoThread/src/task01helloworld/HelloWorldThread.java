package task01helloworld;

public class HelloWorldThread extends Thread{
    public void run(){
        System.out.println("Hello World");
    }

    public static void main(String[] args) {
        HelloWorldThread hello = new HelloWorldThread();
        hello.start();
    }
}
