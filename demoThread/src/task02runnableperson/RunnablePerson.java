package task02runnableperson;

public class RunnablePerson extends Person implements Runnable{
    public RunnablePerson(String name) {
        super(name);
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            Thread t = Thread.currentThread();

            System.out.println("Hello,I am " + getName());
        }
    }



    public static void main(String[] args) throws InterruptedException {
        Thread t = Thread.currentThread();
        t.setName(t.getName());
        System.out.println("Thread Main started");
        RunnablePerson pers1 = new RunnablePerson("Alise");
        Thread thread1 = new Thread(pers1);
        thread1.start();
        RunnablePerson pers2 = new RunnablePerson("Kirill");
        Thread thread2 = new Thread(pers2);
        thread2.start();
       thread1.join();
       thread2.join();
        System.out.println("Thread Main finish");
    }
}
