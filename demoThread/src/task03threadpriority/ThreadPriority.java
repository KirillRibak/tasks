package task03threadpriority;

import java.util.concurrent.TimeUnit;

public class ThreadPriority extends Thread {
    public ThreadPriority(String name) {
        super(name);
    }
    public void run() {
        for (int i = 0; i < 50; i++) {
            System.out.println(getName() + " " + i);
            try {
                //Thread.sleep(0);
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                System.err.print(e);
            }
        }
    }
        public static void main(String[ ] args) {
            ThreadPriority min = new ThreadPriority("Min");
            ThreadPriority max = new ThreadPriority("Max");
            ThreadPriority norm = new ThreadPriority("Normal");
            min.setPriority(Thread.MIN_PRIORITY); // 1
            max.setPriority(Thread.MAX_PRIORITY); // 10
            norm.setPriority(Thread.NORM_PRIORITY); // 5
            min.start();
            norm.start();
            max.start();
        }
}