package task05threadtreadtodisable;

public class ThreadToDisable implements Runnable {

    private boolean isActive;

    public ThreadToDisable() {
        this.isActive = true;
    }

    void disable(){
        isActive=false;
    }
    @Override
    public void run() {
        System.out.printf("Поток  начал работу... ");
        Thread.currentThread().getName();
        int counter=1;
        while(isActive){
            System.out.println("Цикл " + counter++);
            try{
                Thread.sleep(500);
            }
            catch(InterruptedException e){
                System.out.println("Поток прерван");
            }
        }
        System.out.printf("Поток %s завершил работу... \n",
        Thread.currentThread().getName());
    }

    public static void main(String[] args) {
        System.out.println("Главный поток начал работу...");
        ThreadToDisable myThread = new ThreadToDisable();
        Thread myT = new Thread(myThread, "name of ThreadToDisable");
        myT.start();
        try{
            Thread.sleep(1100);
            myThread.disable();
            Thread.sleep(1000);
        }
        catch(InterruptedException e){
            System.out.println("Поток прерван");
        }
        System.out.println("Главный поток завершил работу...");
    }
}
