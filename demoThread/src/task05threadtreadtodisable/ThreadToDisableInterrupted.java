package task05threadtreadtodisable;

public class ThreadToDisableInterrupted extends Thread {

    @Override
    public void run() {
        System.out.printf("Поток  начал работу... ");
        Thread.currentThread().getName();
        int counter = 1;

        while (!Thread.interrupted()) {
            System.out.println("Цикл " + counter++);
        }
        System.out.printf("Поток %s завершил работу... \n",
                Thread.currentThread().getName());
    }

    public static void main(String[] args) {
        System.out.println("Главный поток начал работу...");
        ThreadToDisableInterrupted myThread = new ThreadToDisableInterrupted();
        myThread.start();
        try {
            Thread.sleep(1100);
            myThread.interrupt();
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("Поток прерван");
        }
        System.out.println("Главный поток завершил работу...");
    }
}

