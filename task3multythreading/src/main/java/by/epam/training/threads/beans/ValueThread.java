package by.epam.training.threads.beans;

import lombok.extern.log4j.Log4j2;
import by.epam.training.threads.services.DiagonalChanger;

import java.util.concurrent.Semaphore;

/**
 * Class are present the main essence,
 * each should change the matrix.
 */
@Log4j2
public class ValueThread extends Thread {
    /**
     * The value that is inserted into the matrix.
     */
    private Integer value;
    /**
     *The value of repetition of cycle.
     */
    private Integer repetitions;
    /**
     * The essence, each change the matrix.
     */
    private DiagonalChanger changer;
    /**
     * Thread synchronizer.
     */
    private Semaphore semaphore;

    /**
     * Constructor
     * @param changer - The essence, each change the matrix.
     * @param semaphore - Thread synchronizer.
     * @param value - The value that is inserted into the matrix.
     * @param repetitions - The value of repetition of cycle.
     */
    public ValueThread(DiagonalChanger changer, Semaphore semaphore,Integer value, Integer repetitions) {
        this.changer = changer;
        this.semaphore = semaphore;
        this.value = value;
        this.repetitions = repetitions;
    }

    /**
     * Flow detection method.
     */
    public void run() {
        try {
            for(int i = 0; i<repetitions ;i++) {
                semaphore.acquire();
                log.debug(Thread.currentThread().getName() + " " + value);
                changer.changeMainDiagonal(value);
                semaphore.release();
            }
        } catch (InterruptedException e) {
            log.error(e.getMessage());
        }
    }
}