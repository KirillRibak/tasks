package by.epam.training.threads.controller;

import by.epam.training.threads.beans.ValueThread;
import by.epam.training.threads.dao.Matrix;
import lombok.extern.log4j.Log4j2;
import by.epam.training.threads.services.DiagonalChanger;
import by.epam.training.threads.util.implementation.MatrixParser;
import by.epam.training.threads.util.implementation.ThreadParser;
import by.epam.training.threads.view.MatrixPrinter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Semaphore;
import by.epam.training.threads.services.validation.*;
import by.epam.training.threads.services.initialization.*;


@Log4j2
public class Runner {
    public static void main(String[] args) throws InterruptedException {
        File file = new File("data/Matrix.txt");
        File fileThread = new File("data/Thread.txt");
        File fileToWrite = new File("data/ModifiedMatrix");

        MatrixParser parser = new MatrixParser();
        ThreadParser threadParser = new ThreadParser();

        Matrix matrix = Matrix.getInstance();

        List<Integer[]> mtr = parser.process(file);
        List<Integer> trd = threadParser.process(fileThread);

        Validator validator = new CurrentTaskMatrixValidator();
        Validator validator1 = new CurrentTaskThreadValidator();
        MatrixPrinter printer = new MatrixPrinter();

        DiagonalChanger changer = new DiagonalChanger(matrix);
        Semaphore semaphore = new Semaphore(1, true);

        MatrixInitializer initializer = new MatrixInitializer(matrix, validator);
        ThreadInitializer threadInitializer = new ThreadInitializer(validator1, new ArrayList<>(), changer, semaphore);

        initializer.init(mtr);
        threadInitializer.init(trd);

        Random rnd = new Random(System.currentTimeMillis());
        for (int i = 0; i < threadInitializer.getThreads().size(); i++) {
            ValueThread thread = threadInitializer.getThreads().get(i);
            thread.start();
            if (i == threadInitializer.getThreads().size() - 1) {
                threadInitializer.getThreads().get(i).join();
            }
        }
        printer.wirteInFile(fileToWrite, matrix.toString());
        log.debug("main had been finished");
    }
}


