package by.epam.training.threads.dao;

import lombok.extern.log4j.Log4j2;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Container matrix essence.
 */
@Log4j2
public class Matrix {
    /**
     * Container.
     */
    private List<List<Integer>> matrix;

    /**
     * Constructor.
     */
    private Matrix() {
        this.matrix = new CopyOnWriteArrayList<>();
    }

    /**
     * Nested class, each create the object of outer class.
     */
    private static class LazySomethingHolder {
        public static Matrix singletonInstance = new Matrix();
    }

    /**
     * Return matrix.
     *
     * @return - Matrix object.
     */
    public static Matrix getInstance() {
        return LazySomethingHolder.singletonInstance;
    }

    /**
     * Add new item to matrix.
     *
     * @param row - item,each should be save.
     */
    public void add(List<Integer> row) {
        matrix.add(row);
    }

    /**
     *
     * @param pointer - index of element.
     * @return
     */
    public List<Integer> getDiagonal(int pointer) {
        return matrix.get(pointer);
    }

    public int size() {
        return matrix.size();
    }

    public void set(int index, List<Integer> row) {
        matrix.set(index, row);
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        for (List<Integer> row : matrix) {
            buffer.append(row.toString());
            buffer.append("\n");
        }
        return buffer.toString();

    }
}




