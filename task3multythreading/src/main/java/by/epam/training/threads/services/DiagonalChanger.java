package by.epam.training.threads.services;

import by.epam.training.threads.dao.Matrix;

import java.util.List;

public class DiagonalChanger {
    private int counter ;

    private Matrix matrix;

    public DiagonalChanger( Matrix  matrix) {
        this.counter = 0;
        this.matrix = matrix;
    }

    public void changeMainDiagonal(Integer newValue){
        if (counter<matrix.size()) {
            List<Integer> row = matrix.getDiagonal(counter);
            row.set(counter, newValue);
            matrix.set(counter, row);
            counter++;
        }
    }


    public Matrix getMatrix(){
        return matrix;
    }
}
