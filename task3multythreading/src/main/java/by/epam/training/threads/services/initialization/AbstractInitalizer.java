package by.epam.training.threads.services.initialization;
import by.epam.training.threads.services.validation.*;

public abstract class AbstractInitalizer<T> {
    protected Validator validator;
    abstract void init( T value);
}
