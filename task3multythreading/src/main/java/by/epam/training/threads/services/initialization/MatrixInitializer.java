package by.epam.training.threads.services.initialization;

import lombok.extern.log4j.Log4j2;
import by.epam.training.threads.dao.Matrix;
import by.epam.training.threads.services.validation.*;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Log4j2
public class MatrixInitializer extends AbstractInitalizer <List<Integer[]>>{

    private Matrix matrix;
    private Validator validator;

    public MatrixInitializer(Matrix matrix, Validator validator) {
        this.matrix = matrix;
        this.validator = validator;
    }

    public void init(List<Integer[]> matr) {
        if (validator.isValid(matr)) {
            for (Integer[] row : matr) {
                CopyOnWriteArrayList<Integer> threadSafeRow = new CopyOnWriteArrayList<>(row);
                matrix.add(threadSafeRow);
            }
            log.info("Matrix had been initialized");
        } else {
            log.warn("Matrix was not initialized ");
        }
    }

}
