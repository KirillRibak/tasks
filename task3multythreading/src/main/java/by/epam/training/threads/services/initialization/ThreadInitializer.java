package by.epam.training.threads.services.initialization;

import lombok.extern.log4j.Log4j2;
import by.epam.training.threads.services.DiagonalChanger;
import by.epam.training.threads.beans.ValueThread;
import by.epam.training.threads.services.validation.*;

import java.util.List;
import java.util.concurrent.Semaphore;

@Log4j2
public class ThreadInitializer extends AbstractInitalizer <List<Integer>>{

    private Validator validator;
    private List<ValueThread> threads;
    private DiagonalChanger changer;
    private Semaphore semaphore;

    public ThreadInitializer(Validator validator, List<ValueThread> threads,
                             DiagonalChanger changer, Semaphore semaphore) {
        this.validator = validator;
        this.threads = threads;
        this.changer = changer;
        this.semaphore = semaphore;
    }

    public void init(List<Integer> values) {
        if (validator.isValid(values)) {
            for(Integer value: values){
                ValueThread valueThread = new ValueThread(changer,semaphore,value,values.size());
                threads.add(valueThread);
            }
            log.info("Threads had been initialized");
        } else {
            log.warn("Threads was not initialized ");
        }
    }

    public List<ValueThread> getThreads(){
        return threads;
    }
}
