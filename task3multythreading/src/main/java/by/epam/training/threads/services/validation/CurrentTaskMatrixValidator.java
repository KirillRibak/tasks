package by.epam.training.threads.services.validation;

import lombok.extern.log4j.Log4j2;

import java.util.List;
@Log4j2
public class CurrentTaskMatrixValidator implements Validator<List<Integer[]>>{
    public boolean isValid(List<Integer[]> matr) {
        int leftBorder = 8;
        int rightBorder = 12;
        int rowValue = matr.size();
        if (rowValue < leftBorder || rowValue > rightBorder) {
            log.error("Incorrect matrix size");
            return false;
        }
        for (Integer[] values : matr) {
            if (values.length != rowValue) {
                log.error("Incorrect matrix size, matrix should be square");
                return false;
            }
        }
        for (int i = 0; i < matr.size(); i++) {
            Integer[] values = matr.get(i);
            for (int j = 0; j<values.length;j++){
                if (j==i){
                    if (values[j]!=0){
                        log.warn("main diagonal elements must be 0");
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
