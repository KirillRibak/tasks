package by.epam.training.threads.services.validation;

import lombok.extern.log4j.Log4j2;

import java.util.List;

@Log4j2
public class CurrentTaskThreadValidator implements Validator<List<Integer>> {
    @Override
    public boolean isValid(List<Integer> values) {
        if (values.size()>6 || values.size()<4){
            log.error("Invalid file size");
            return false;
        }
        return true;
    }
}
