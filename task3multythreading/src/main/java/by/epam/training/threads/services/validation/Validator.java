package by.epam.training.threads.services.validation;

public interface Validator <T> {
    boolean isValid(T values);
}
