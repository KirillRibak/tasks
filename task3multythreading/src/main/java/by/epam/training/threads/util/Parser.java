package by.epam.training.threads.util;

import java.io.File;

public interface Parser<T> {
    T process(File file);
}

