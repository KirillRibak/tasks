package by.epam.training.threads.util.implementation;

import lombok.extern.log4j.Log4j2;
import by.epam.training.threads.util.Parser;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Log4j2
public class MatrixParser implements Parser<List<Integer[]>> {

    private  List<Integer[]> data = new ArrayList<>();

    private  boolean validate(File file) {
        if (!file.exists()) {
            return false;
        }
        if (file.length() == 0) {
            return false;
        }
        return true;
    }

    public List<Integer[]> process(File file) {
        if (validate(file)) {
            try (Scanner scan = new Scanner(file)) {
                scan.useDelimiter("\r\n");
                while (scan.hasNext()) {
                    String[] substring = scan.next().split("");
                    Integer[] values = processLine(substring);
                    data.add(values);
                }
            } catch (FileNotFoundException e) {
                log.error("File is not exist.");
            }
        }
        return data ;
    }

    private Integer[] processLine(String[] strings) {
        Integer[] intArray=new Integer[strings.length];
       try {
           int i=0;
           for(String str:strings){
               intArray[i]=Integer.parseInt(str);
               i++;
           }
       }catch (NumberFormatException e){
           log.error("file contains incorrectly entered data");
       }
       return intArray;
    }

}
