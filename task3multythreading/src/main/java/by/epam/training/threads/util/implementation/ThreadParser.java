package by.epam.training.threads.util.implementation;

import lombok.extern.log4j.Log4j2;
import by.epam.training.threads.util.Parser;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Log4j2
public class ThreadParser implements Parser<List<Integer>> {

    public ThreadParser() {
        this.data = new ArrayList<>();
    }

    private List<Integer> data ;

    private  boolean validate(File file) {
        if (!file.exists()) {
            return false;
        }
        if (file.length() == 0) {
            return false;
        }
        return true;
    }

    public List<Integer> process(File file) {
        if (validate(file)) {
            try (Scanner scan = new Scanner(file)) {
                scan.useDelimiter("\r\n");
                while (scan.hasNext()) {
                    String someValue = scan.next();
                    try {
                        data.add(Integer.parseInt(someValue));
                    }catch (NumberFormatException e){
                        log.error("The file contains invalid values");
                    }
                }
            } catch (FileNotFoundException e) {
                log.error("File is not exist.");
            }
        }
        return  data;
    }
}
