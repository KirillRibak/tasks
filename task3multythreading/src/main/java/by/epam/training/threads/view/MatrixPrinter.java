package by.epam.training.threads.view;

import lombok.extern.log4j.Log4j2;

import java.io.*;

@Log4j2
public class MatrixPrinter {

    public  void wirteInFile(File file, String data){
        try(FileOutputStream out = new FileOutputStream(file)){
            byte[] buffer = data.getBytes();
            out.write(buffer,0,buffer.length);
        }catch (FileNotFoundException e){
            log.error("File is not exists");
        }catch (IOException e){
            log.error("Can't write file");
        }
    }
}
