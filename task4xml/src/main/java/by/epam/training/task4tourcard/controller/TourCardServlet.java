package by.epam.training.task4tourcard.controller;

import by.epam.training.task4tourcard.model.Card;
import by.epam.training.task4tourcard.service.FileService;
import by.epam.training.task4tourcard.service.ParserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.util.Set;

@MultipartConfig
public class TourCardServlet extends HttpServlet {
    private final static String URL = "/table.jsp";
    private ParserService parserService = new ParserService();
    private FileService fileService = new FileService();

    public TourCardServlet() {
        super();
    }

    @Override
    public void init() throws ServletException {
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String parser = req.getParameter("parser");
        fileService.updateFile(req);
        Set<Card> cards = parserService.getCards(FileService.UPLOAD_PATH,parser);
        req.setAttribute("cards",cards);
        req.getRequestDispatcher(URL).forward(req, resp);
    }

}