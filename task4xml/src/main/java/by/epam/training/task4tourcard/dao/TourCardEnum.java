package by.epam.training.task4tourcard.dao;

public enum TourCardEnum {
    PRICE("price"),
    TOURCARDS("tourcards"),
    TOURCARD("tourcard"),
    NAME("name"),
    LOGIN("login"),
    INSURANCE("insurance"),
    TOURISTS("tourists"),
    PLACE("place"),
    REGION("region"),
    CITY("city"),
    TIME("time"),
    START("start"),
    FINISH("finish"),
    CATEGORY("category"),
    EXCURSION("excursion"),
    EXCURSIONNAME("excursionname"),
    EXCURSIONPRICE("excursionprice"),
    TRIP("trip"),
    TRIPNAME("tripname"),
    TRIPPLACE("tripplace"),
    TRIPPRICE("tripprice"),
    TRANSPORT("transport");
    private String value;
    private TourCardEnum(String value){
       this.value=value;
    }

    public String getValue() {
        return value;
    }
}
