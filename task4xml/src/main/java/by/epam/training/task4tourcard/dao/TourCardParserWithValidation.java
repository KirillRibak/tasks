package by.epam.training.task4tourcard.dao;

import by.epam.training.task4tourcard.model.Card;
import by.epam.training.task4tourcard.dao.validator.TourCardValidatorXsd;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

public abstract class TourCardParserWithValidation {
    protected TourCardValidatorXsd validatorXsd;
    protected String fileXML;
    private String xsdPath;
    protected Set<Card> cards;

    public TourCardParserWithValidation(String fileXML, String xsdPath) {
        cards = new HashSet<>();
        this.fileXML= fileXML;
        this.xsdPath = xsdPath;
        validatorXsd = new TourCardValidatorXsd(fileXML,xsdPath);
    }
    protected boolean isValid(){
        return validatorXsd.isValid();
    }
    public Set<Card> getCards() {
        return cards;
    }
    public abstract void buildTourCards();
}
