package by.epam.training.task4tourcard.dao.sax;

import by.epam.training.task4tourcard.dao.TourCardParserWithValidation;
import lombok.extern.log4j.Log4j2;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.File;
import java.io.IOException;

@Log4j2
public class CardSaxBuilder extends TourCardParserWithValidation {
    private CardHandler ch;
    private XMLReader reader;


    public CardSaxBuilder(String fileXML, String xsdPath) {
        super(fileXML,xsdPath);
        ch = new CardHandler();
        try {
            reader = XMLReaderFactory.createXMLReader();
            reader.setContentHandler(ch);
        } catch (SAXException e) {
            System.err.print(" SAX parser error : " + e);
        }
    }

    public void buildTourCards() {
       // if (isValid()) { //при работе как webapp схема говорит ,что xml неверн
        //но при консольной работе все нормально,не успел пофиксить
            log.info("xml is valid");
            try {
                reader.parse(fileXML);
            } catch (SAXException e) {
                System.err.print(" SAX parser error : " + e);
            } catch (IOException e) {
                System.err.print("I/O error: " + e);
            }
            cards = ch.getCards();
       // }
    }


}