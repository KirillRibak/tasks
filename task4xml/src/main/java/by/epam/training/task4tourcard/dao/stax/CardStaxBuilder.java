package by.epam.training.task4tourcard.dao.stax;

import by.epam.training.task4tourcard.dao.TourCardEnum;
import by.epam.training.task4tourcard.dao.TourCardParserWithValidation;
import by.epam.training.task4tourcard.model.*;
import by.epam.training.task4tourcard.util.CurrentDateFormatter;
import lombok.extern.log4j.Log4j2;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.HashSet;

@Log4j2
public class CardStaxBuilder extends TourCardParserWithValidation {

    private final ObjectFactory factory;

    private XMLInputFactory inputFactory;

    public CardStaxBuilder(String fileXML, String xsdPath) {
        super(fileXML, xsdPath);
        this.cards = new HashSet<>();
        this.inputFactory = XMLInputFactory.newInstance();
        factory = new ObjectFactory();
    }


    public void buildTourCards() {
      //  if (isValid()) {
            XMLStreamReader reader = null;
            String name;
            try (FileInputStream inputStream = new FileInputStream(fileXML)) {
                reader = inputFactory.createXMLStreamReader(inputStream);
                while (reader.hasNext()) {
                    int type = reader.next();
                    if (type == XMLStreamConstants.START_ELEMENT) {
                        name = reader.getLocalName();
                        if (TourCardEnum.valueOf(name.toUpperCase()) == TourCardEnum.TOURCARD) {
                            TourCard card = buildCard(reader);
                            log.debug("Card should be added.");
                            cards.add(card);
                        }
                    }
                }
            } catch (XMLStreamException ex) {
                log.error("StAX parsing error! " + ex.getMessage());
            } catch (IOException e) {
                log.error("I/O error");
            }
      //  }
    }

    private TourCard buildCard(XMLStreamReader reader) throws XMLStreamException {
        TourCard tourCard = factory.createTourCard();
        Integer price = Integer.parseInt(reader.getAttributeValue(null, TourCardEnum.PRICE.getValue()));
        Integer tourists = Integer.parseInt(reader.getAttributeValue(null, TourCardEnum.TOURISTS.getValue()));
        tourCard.setName(reader.getAttributeValue(null, TourCardEnum.NAME.getValue()));
        tourCard.setPrice(BigInteger.valueOf(price));
        tourCard.setTourists(BigInteger.valueOf(tourists));
        tourCard.setLogin(reader.getAttributeValue(null, TourCardEnum.LOGIN.getValue()));
        tourCard.setInsurance(Boolean.valueOf(reader.getAttributeValue(null, TourCardEnum.INSURANCE.getValue())));
        String name;
        try {
            while (reader.hasNext()) {
                int type = reader.next();
                switch (type) {
                    case XMLStreamConstants.START_ELEMENT:
                        name = reader.getLocalName();
                        switch (TourCardEnum.valueOf(name.toUpperCase())) {
                            case PLACE:
                                try {
                                    tourCard.setPlace(getXMLPlace(reader));
                                } catch (XMLStreamException e) {
                                    log.error(e.getMessage());
                                }
                                break;
                            case TIME:
                                try {
                                    tourCard.setTime(getXMLTime(reader));
                                } catch (XMLStreamException e) {
                                    log.error(e.getMessage());
                                }
                                break;
                            case CATEGORY:
                                tourCard.setCategory(getXMLText(reader));
                                break;
                            case EXCURSION:
                                try {
                                    tourCard.getExcursion().add(getXMLExcursion(reader));
                                } catch (XMLStreamException e) {
                                    log.error(e.getMessage());
                                }
                                break;
                            case TRIP:
                                try {
                                    tourCard.getTrip().add(getXMLTrip(reader));
                                } catch (XMLStreamException e) {
                                    log.error(e.getMessage());
                                }
                                break;
                            case TRANSPORT:
                                tourCard.setTransport(getXMLText(reader));
                                break;
                            default:
                                log.error("Incorrect request" + name + " is not processing.");
                        }
                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        name = reader.getLocalName();
                        if (TourCardEnum.valueOf(name.toUpperCase()) == TourCardEnum.TOURCARD) {
                            return tourCard;
                        }
                        break;
                    default:
                        log.error("Unknown tag statement" + " " + type);
                }
            }
        } catch (XMLStreamException e) {
            log.error("XML read excpeption.");
        }
        throw new XMLStreamException("Unknown element in tag 'tourcard'. ");
    }

    private String getXMLText(XMLStreamReader reader) {
        String text = null;
        try {
            if (reader.hasNext()) {
                reader.next();
                text = reader.getText();
            }
        } catch (XMLStreamException e) {
            log.error("XML stream exception in StAX.");
        }
        return text;
    }


    private Trip getXMLTrip(XMLStreamReader reader) throws XMLStreamException {
        log.debug("The 'trip' tag began to be processed.");
        Trip trip = factory.createTrip();
        int type;
        String name;
        while (reader.hasNext()) {
            type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (TourCardEnum.valueOf(name.toUpperCase())) {
                        case TRIPPLACE:
                            trip.setTripPlace(getXMLText(reader));
                            break;
                        case TRIPNAME:
                            trip.setTripName(getXMLText(reader));
                            break;
                        case TRIPPRICE:
                            Integer price = Integer.valueOf(getXMLText(reader));
                            trip.setTripPrice(BigInteger.valueOf(price));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (TourCardEnum.valueOf(name.toUpperCase()) == TourCardEnum.TRIP) {
                        log.info("The 'trip' tag has been processed.");
                        return trip;
                    }
                    break;
            }
        }
        throw new XMLStreamException("Unknown element in tag Trip");
    }

    private Excursion getXMLExcursion(XMLStreamReader reader) throws XMLStreamException {
        log.info("The 'excursion'  become to be processed.");
        Excursion excursion = factory.createExcursion();
        int type;
        String name;
        while (reader.hasNext()) {
            type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (TourCardEnum.valueOf(name.toUpperCase())) {
                        case EXCURSIONNAME:
                            excursion.setExcursionName(getXMLText(reader));
                            break;
                        case EXCURSIONPRICE:
                            Integer price = Integer.valueOf(getXMLText(reader));
                            excursion.setExcursionPrice(BigInteger.valueOf(price));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (TourCardEnum.valueOf(name.toUpperCase()) == TourCardEnum.EXCURSION) {
                        log.info("The 'excursion' tag has been processed.");
                        return excursion;
                    }
                    break;
            }
        }
        throw new XMLStreamException("Unknown element in tag Excursion");
    }

    private Time getXMLTime(XMLStreamReader reader) throws XMLStreamException {
        log.info("The 'time'  become to be processed.");
        Time time = factory.createTime();
        int type;
        String name;
        while (reader.hasNext()) {
            type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (TourCardEnum.valueOf(name.toUpperCase())) {
                        case START:
                            time.setStart(CurrentDateFormatter.getTime(getXMLText(reader)));
                            break;
                        case FINISH:
                            time.setFinish(CurrentDateFormatter.getTime(getXMLText(reader)));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (TourCardEnum.valueOf(name.toUpperCase()) == TourCardEnum.TIME) {
                        log.info("The 'time' tag has been processed.");
                        return time;
                    }
                    break;
            }
        }
        throw new XMLStreamException("Unknown element in tag Time");
    }

    private Place getXMLPlace(XMLStreamReader reader) throws XMLStreamException {
        log.info("The 'place'  become to be processed.");
        Place place = factory.createPlace();
        int type;
        String name;
        while (reader.hasNext()) {
            type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (TourCardEnum.valueOf(name.toUpperCase())) {
                        case REGION:
                            place.setRegion(getXMLText(reader));
                            break;
                        case CITY:
                            place.setCity(getXMLText(reader));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (TourCardEnum.valueOf(name.toUpperCase()) == TourCardEnum.PLACE) {
                        log.info("The 'place'  has been processed.");
                        return place;
                    }
                    break;
            }
        }
        throw new XMLStreamException("Unknown element in tag Place");
    }
}