package by.epam.training.task4tourcard.dao.validator;

import jdk.internal.org.xml.sax.SAXParseException;
import jdk.internal.org.xml.sax.helpers.DefaultHandler;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;

@Log4j2
public class TourCardErrorHander extends DefaultHandler {
    public TourCardErrorHander() throws IOException {

    }
    public void warning(SAXParseException e) {
        log.warn(getLineAddress(e) + "-" + e.getMessage());
    }
    public void error(SAXParseException e) {
        log.error(getLineAddress(e) + " - " + e.getMessage());
    }
    public void fatalError(SAXParseException e) {
        log.fatal(getLineAddress(e) + " - " + e.getMessage());
    }
    private String getLineAddress(SAXParseException e) {
        return e.getLineNumber() + " : " + e.getColumnNumber();
    }
}