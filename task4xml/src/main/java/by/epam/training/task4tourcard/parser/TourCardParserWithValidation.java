package by.epam.training.task4tourcard.parser;

import by.epam.training.task4tourcard.card.Card;
import by.epam.training.task4tourcard.parser.validator.TourCardValidatorXsd;

import java.util.HashSet;
import java.util.Set;

public abstract class TourCardParserWithValidation {
    protected TourCardValidatorXsd validatorXsd;
    protected String filePath;
    private String xsdPath;
    protected Set<Card> cards;

    public TourCardParserWithValidation(String filePath, String xsdPath) {
        cards = new HashSet<>();
        this.filePath= filePath;
        this.xsdPath = xsdPath;
        validatorXsd = new TourCardValidatorXsd(filePath,xsdPath);
    }
    protected boolean isValid(){
        return validatorXsd.isValid();
    }
    public Set<Card> getCards() {
        return cards;
    }
    public abstract void buildTourCards();
}
