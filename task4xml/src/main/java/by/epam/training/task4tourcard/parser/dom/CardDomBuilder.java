package by.epam.training.task4tourcard.parser.dom;

import by.epam.training.task4tourcard.card.*;
import by.epam.training.task4tourcard.parser.TourCardEnum;
import by.epam.training.task4tourcard.parser.TourCardParserWithValidation;
import by.epam.training.task4tourcard.parser.validator.TourCardValidatorXsd;
import by.epam.training.task4tourcard.util.CurrentDateFormatter;
import lombok.extern.log4j.Log4j2;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Log4j2
public class CardDomBuilder extends TourCardParserWithValidation {
    public static final String DEFAULT_START = "2019-01-01";
    public static final String DEFAULT_FINISH = "2019-01-10";
    public static final String TOURCARDTAG = "tourcard";

    private DocumentBuilder documentBuilder;

    public CardDomBuilder(String filePath,String xsdPath)
    {
        super(filePath,xsdPath);
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        try {
            documentBuilder = docFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            log.error("Parser configuration exception");
        }
    }

    public void buildTourCards() {
        if (isValid()) {
            try {
                Document doc = documentBuilder.parse(filePath);

                Element root = doc.getDocumentElement();
                NodeList simpleTourCards = root.getElementsByTagName(TOURCARDTAG);
                for (int i = 0; i < simpleTourCards.getLength(); i++) {
                    Element cardElement = (Element) simpleTourCards.item(i);
                    TourCard tourCard = buildTourCard(cardElement);
                    cards.add(tourCard);
                }
            } catch (SAXException e) {
                log.error("Parsing failure");
            } catch (IOException e) {
                log.error("File error or I/O error");
            }
        }
    }

    private TourCard buildTourCard(Element cardElement) {
        ObjectFactory factory = new ObjectFactory();
        TourCard tourCard = factory.createTourCard();
        Time cardTime = factory.createTime();
        Place relaxPlace = factory.createPlace();
        Integer price=0;
        Integer tourists=1;
        try {
             price = Integer.parseInt(cardElement.getAttribute(TourCardEnum.PRICE.getValue()));
        } catch (NumberFormatException e) {
            log.error("Incorrect price format" + ""+cardElement.getAttribute(TourCardEnum.PRICE.getValue()));
        }
        try {
             tourists = Integer.parseInt(cardElement.getAttribute(TourCardEnum.TOURISTS.getValue()));
        }catch (NumberFormatException e){
            log.error("Incorrect tourists format" + ""+cardElement.getAttribute(TourCardEnum.TOURISTS.getValue()));
        }

        Element timeElement = (Element) cardElement.getElementsByTagName(TourCardEnum.TIME.getValue()).item(0);
        Element placeElement = (Element) cardElement.getElementsByTagName(TourCardEnum.PLACE.getValue()).item(0);
        Date start = CurrentDateFormatter.getTime(getElementTextContent(timeElement, TourCardEnum.START.getValue()));
        Date finish =  CurrentDateFormatter.getTime(getElementTextContent(timeElement, TourCardEnum.FINISH.getValue()));
        if (start == null || finish == null) {
            cardTime.setStart( CurrentDateFormatter.getTime(DEFAULT_START));
            cardTime.setFinish( CurrentDateFormatter.getTime(DEFAULT_FINISH));
        } else {
            cardTime.setStart(start);
            cardTime.setFinish(finish);
        }
        if (cardElement.hasAttribute(TourCardEnum.EXCURSION.getValue())) {
            NodeList excursions = cardElement.getElementsByTagName(TourCardEnum.EXCURSION.getValue());
            for (int i = 0; i < excursions.getLength(); i++) {
                Element excursionElement = (Element) cardElement.getElementsByTagName(TourCardEnum.EXCURSION.getValue())
                        .item(0);
                Excursion excursion = factory.createExcursion();
                Integer excursionPrice = Integer.parseInt(getElementTextContent(excursionElement,
                        TourCardEnum.EXCURSIONPRICE.getValue()));
                excursion.setExcursionName(getElementTextContent(excursionElement,
                        TourCardEnum.EXCURSIONNAME.getValue()));
                excursion.setExcursionPrice(BigInteger.valueOf(excursionPrice));
                tourCard.getExcursion().add(excursion);
            }
        } else {
            NodeList trips = cardElement.getElementsByTagName(TourCardEnum.TRIP.getValue());
            for (int i = 0; i < trips.getLength(); i++) {
                Element tripElement = (Element) cardElement.getElementsByTagName(TourCardEnum.TRIP.getValue()).item(0);
                Trip trip = factory.createTrip();
                Integer tripPrice = Integer.parseInt(getElementTextContent(tripElement, TourCardEnum.TRIPPRICE.getValue()));
                trip.setTripName(getElementTextContent(tripElement, TourCardEnum.TRIPNAME.getValue()));
                trip.setTripPlace(getElementTextContent(tripElement, TourCardEnum.TRIPPLACE.getValue()));
                trip.setTripPrice(BigInteger.valueOf(tripPrice));
                tourCard.getTrip().add(trip);
            }
        }

        relaxPlace.setCity(getElementTextContent(placeElement, TourCardEnum.CITY.getValue()));
        relaxPlace.setRegion(getElementTextContent(placeElement, TourCardEnum.REGION.getValue()));

        tourCard.setName(cardElement.getAttribute(TourCardEnum.NAME.getValue()));
        tourCard.setLogin(cardElement.getAttribute(TourCardEnum.LOGIN.getValue()));
        tourCard.setInsurance(Boolean.getBoolean(cardElement.getAttribute(TourCardEnum.INSURANCE.getValue())));
        tourCard.setPrice(BigInteger.valueOf(price));
        tourCard.setTourists(BigInteger.valueOf(tourists));
        tourCard.setTime(cardTime);
        tourCard.setPlace(relaxPlace);
        tourCard.setCategory(getElementTextContent(cardElement, TourCardEnum.CATEGORY.getValue()));
        tourCard.setTransport(getElementTextContent(cardElement, TourCardEnum.TRANSPORT.getValue()));
        return tourCard;
    }

    private String getElementTextContent(Element element, String elementName) {
        NodeList nList = element.getElementsByTagName(elementName);
        Node node = nList.item(0);
        String text = node.getTextContent();
        return text;
    }

    private class Set<T> {
    }
}
