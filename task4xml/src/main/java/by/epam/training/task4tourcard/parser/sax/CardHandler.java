package by.epam.training.task4tourcard.parser.sax;

import by.epam.training.task4tourcard.card.*;
import by.epam.training.task4tourcard.parser.TourCardEnum;
import by.epam.training.task4tourcard.util.CurrentDateFormatter;
import lombok.extern.log4j.Log4j2;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.math.BigInteger;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

@Log4j2
public class CardHandler extends DefaultHandler {

    private static final int NUMBER_OF_ATTRIBUTES = 5;
    private static final int PRICE = 1;
    private static final int LOGIN = 2;
    private static final int TOURISTS = 3;
    private static final int INSURANCE = 4;
    private final ObjectFactory factory;

    private Place place;
    private Time time;
    private Excursion excursion;
    private Trip trip;

    private Set<Card> cards;
    private TourCard current = null;
    private TourCardEnum currentEnum = null;
    private EnumSet<TourCardEnum> withText;

    public CardHandler() {
        factory = new ObjectFactory();
        cards = new HashSet<>();
        withText = EnumSet.range(TourCardEnum.PLACE, TourCardEnum.TRANSPORT);
    }

    public Set<Card> getCards() {
        return cards;
    }

    @Override
    public void startDocument() {
        log.debug("Start handling xml.");
    }

    @Override
    public void endDocument() {
        log.debug("Finish handling xml.");
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        if ("tourcard".equals(localName)) {
            current = new TourCard();
            current.setName(attributes.getValue(0));
            if (attributes.getLength() == NUMBER_OF_ATTRIBUTES) {
                Integer price = Integer.parseInt(attributes.getValue(PRICE));
                Integer tourists = Integer.parseInt(attributes.getValue(TOURISTS));

                current.setPrice(BigInteger.valueOf(price));
                current.setLogin(attributes.getValue(LOGIN));
                current.setTourists(BigInteger.valueOf(tourists));
                current.setInsurance(Boolean.valueOf(attributes.getValue(INSURANCE)));
            }
        } else {
            TourCardEnum temp = TourCardEnum.valueOf(localName.toUpperCase());
            if (withText.contains(temp)) {
                currentEnum = temp;
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        if ("tourcard".equals(localName)) {
                cards.add(current);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        String s = new String(ch, start, length).trim();
        if (currentEnum != null) {
            switch (currentEnum) {
                case TOURCARDS:
                    break;
                case PLACE:
                    place = factory.createPlace();
                    break;
                case REGION:
                    place.setRegion(s);
                    break;
                case CITY:
                    place.setCity(s);
                    current.setPlace(place);
                    break;
                case TIME:
                    time = factory.createTime();
                    break;
                case START:
                    time.setStart(CurrentDateFormatter.getTime(s));
                    break;
                case FINISH:
                    time.setFinish(CurrentDateFormatter.getTime(s));
                    current.setTime(time);
                    break;
                case CATEGORY:
                    current.setCategory(s);
                    break;
                case EXCURSION:
                    excursion = factory.createExcursion();
                    break;
                case EXCURSIONNAME:
                    excursion.setExcursionName(s);
                    break;
                case EXCURSIONPRICE:
                    Integer excPrice = Integer.parseInt(s);
                    excursion.setExcursionPrice(BigInteger.valueOf(excPrice));
                    current.getExcursion().add(excursion);
                case TRIP:
                    trip = factory.createTrip();
                    break;
                case TRIPNAME:
                    trip.setTripName(s);
                    break;
                case TRIPPLACE:
                    trip.setTripPlace(s);
                case TRIPPRICE:
                    Integer tripPrice = Integer.parseInt(s);
                    excursion.setExcursionPrice(BigInteger.valueOf(tripPrice));
                    current.getTrip().add(trip);
                    break;
                case TRANSPORT:
                    current.setTransport(s);
                    break;
                default:
                    log.error("Incorrect request"+ currentEnum + " is not processing.");
            }
        }
        currentEnum = null;
    }
}


