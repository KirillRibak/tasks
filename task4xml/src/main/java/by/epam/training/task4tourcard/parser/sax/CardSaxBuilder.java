package by.epam.training.task4tourcard.parser.sax;

import by.epam.training.task4tourcard.card.Card;
import by.epam.training.task4tourcard.parser.TourCardParserWithValidation;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;
import java.util.Set;
public class CardSaxBuilder extends TourCardParserWithValidation {
    private CardHandler ch;
    private XMLReader reader;


    public CardSaxBuilder(String filePath,String xsdPath) {
        super(filePath,xsdPath);
        ch = new CardHandler();
        try {
            reader = XMLReaderFactory.createXMLReader();
            reader.setContentHandler(ch);
        } catch (SAXException e) {
            System.err.print(" SAX parser error : " + e);
        }
    }

    public void buildTourCards() {
        if (isValid()) {
            try {
                reader.parse(filePath);
            } catch (SAXException e) {
                System.err.print(" SAX parser error : " + e);
            } catch (IOException e) {
                System.err.print("I/O error: " + e);
            }
            cards = ch.getCards();
        }
    }
}