package by.epam.training.task4tourcard.parser.validator;

import java.io.*;
import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import lombok.extern.log4j.Log4j2;
import org.xml.sax.SAXException;

@Log4j2
public class TourCardValidatorXsd {
    private final static String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
    private String fileName;
    private String schemaName;
    private SchemaFactory factory;
    private File schemaLocation;

    public TourCardValidatorXsd(String fileName, String schemaName) {
        this.fileName = fileName;
        this.schemaName = schemaName;
        this.factory = SchemaFactory.newInstance(language);
        this.schemaLocation = new File(schemaName);
    }

    public boolean isValid() {
        try {
            Schema schema = factory.newSchema(schemaLocation);
            Validator validator = schema.newValidator();
            Source source = new StreamSource(fileName);
            validator.validate(source);
            log.info(fileName + " is valid.");
            return true;
        } catch (SAXException e) {
            log.error("validation " + fileName + " is not valid because "
                    + e.getMessage());
        } catch (IOException e) {
            log.error(fileName + " is not valid because "
                    + e.getMessage());
        }
        return false;
    }
}

