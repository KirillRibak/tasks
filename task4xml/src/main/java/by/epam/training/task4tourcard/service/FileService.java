package by.epam.training.task4tourcard.service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileService {
    public static final String UPLOAD_PATH  = "data/temp.xml";
    private static final String FILE_ATTRIBUTE = "fileXML";
    public void updateFile( HttpServletRequest request) throws IOException, ServletException {
        Part filePart = request.getPart(FILE_ATTRIBUTE);
        File file = new File(UPLOAD_PATH);
        try(InputStream filecontent = filePart.getInputStream();
            OutputStream out = new FileOutputStream(file)) {
            int read;
            final byte[] bytes = new byte[1024];
            while ((read = filecontent.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
        }
    }


//    private File file = new File(TEMP);
//    public File processFile(HttpServletRequest req) {
//        try {
//            Part filePart = req.getPart("fileXML");
//            try(InputStream fileContent = filePart.getInputStream();
//                OutputStream out = new FileOutputStream(file)) {
//                int read;
//                final byte[] bytes = new byte[1024];
//                while ((read = fileContent.read(bytes)) != -1) {
//                    out.write(bytes, 0, read);
//                }
//            }
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (ServletException e) {
//            e.printStackTrace();
//        }
//
//        return file;
//    }
}
