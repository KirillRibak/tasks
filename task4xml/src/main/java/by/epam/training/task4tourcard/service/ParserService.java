package by.epam.training.task4tourcard.service;

import by.epam.training.task4tourcard.dao.dom.CardDomBuilder;
import by.epam.training.task4tourcard.dao.sax.CardSaxBuilder;
import by.epam.training.task4tourcard.dao.stax.CardStaxBuilder;
import by.epam.training.task4tourcard.model.Card;

import java.io.File;
import java.util.Set;

public class ParserService {
    private static final String XSD = "data/TourCard.xsd";
    private Set<Card> cards;

    public Set<Card> getCards(String file, String parserType) {
        switch (parserType) {
            case "SAX":
                CardSaxBuilder sax = new CardSaxBuilder(file, XSD);
                sax.buildTourCards();
                cards = sax.getCards();
                break;
            case "DOM":
                CardDomBuilder dom = new CardDomBuilder(file, XSD);
                dom.buildTourCards();
                cards = dom.getCards();
                break;
            case "StAX":
                CardStaxBuilder stax = new CardStaxBuilder(file, XSD);
                stax.buildTourCards();
                cards = stax.getCards();
        }
        return cards;
    }
}
