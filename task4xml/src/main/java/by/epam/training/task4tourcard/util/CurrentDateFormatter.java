package by.epam.training.task4tourcard.util;

import lombok.extern.log4j.Log4j2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
@Log4j2
public class CurrentDateFormatter {
    public static Date getTime(String date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd");
        Date docDate = null;
        try {
            docDate = format.parse(date);
        } catch (ParseException e) {
            log.error("Incorrect date format");
        }
        return docDate;
    }
}
