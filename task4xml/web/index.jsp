<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Parses</title>
  </head>
  <body>
  <form action="table" method="post"enctype="multipart/form-data">
      <br/>
      <div>
      <select name="parser" >
          <option value="SAX"> <c:out value="SAX"/></option>
          <option value="DOM"> <c:out value="DOM"/></option>
          <option value="StAX"> <c:out value="StAX"/></option>
      </select>
      </div>
      <br/>
      <div>
      <input type="file" name="fileXML" accept=".xml"/>
      </div>
      <br/>
    <input type="submit" value="Show table">
  </form>
  </body>
</html>
