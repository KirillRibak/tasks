<%--
  Created by IntelliJ IDEA.
  User: Кирилл
  Date: 26.06.2019
  Time: 20:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>table</title>
</head>
<body>
<br>
<button type="button" name="back" onclick="history.back()">back</button>
<br>
<table border="2">
    <tr>
        <th>Имя путевки</th>
        <th>Цена</th>
        <th>Начало отдыха</th>
        <th>Конец отдыхв</th>
        <th>Город</th>
        <th>Категория отдыха</th>
        <th>Экскурсии</th>
        <th>Поездки</th>
        <th>Тип транспорта</th>
        <th>Логин</th>
        <th>Туристы</th>
        <th>Охрана</th>
    </tr>
<c:forEach var="card" items="${cards}">  <td>
    <tr>
        <td><c:out value="${card.name}"/></td>
        <td><c:out value="${card.price}"/></td>
        <td><c:out value="${card.time.start}"/></td>
        <td><c:out value="${card.time.finish}"/></td>
        <td><c:out value="${card.place.city}"/></td>
        <td><c:out value="${card.category}"/></td>
        <c:forEach var="exc" items="${card.excursion}">
            <td><c:out value="${exc.excursionName}"/></td>
        </c:forEach>
        <c:forEach var="tr" items="${card.trip}">
            <td><c:out value="${tr.tripName}"/></td>
        </c:forEach>
        <td><c:out value="${card.transport}"/></td>
        <td><c:out value="${card.login}"/></td>
        <td><c:out value="${card.tourists}"/></td>
        <td><c:out value="${card.insurance}"/></td>
        </td>
    </tr>
</c:forEach>
</table>
</body>
</html>
